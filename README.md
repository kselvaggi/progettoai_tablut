# ProgettoAI_Tablut
Progetto di Intelligenza Artificiale realizzato per la competizione di Tablut dell'Università di Bologna.

# Come avviare il client
Il client è avviato mediante il seguente comando

	java -jar TeamCAMST.jar (white|black) [maxTimeInMillis]

NOTA: Il comando tra parentesi quadre è facoltativo, di default il client richiede 60 secondi per scegliere una mossa.

NOTA(2): Il tempo deve essere specificato in millisecondi

# Membri del Team CAMST
```
Andrea Bonantini
Federico Ceccoli
Kevin Selvaggi
```
