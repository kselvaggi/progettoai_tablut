Il client � avviato mediante il seguente comando

	java -jar TeamCAMST.jar (white|black) [maxTimeInMillis]

NOTA   : Il comando tra parentesi quadre � facoltativo, di default il client richiede 60 secondi per scegliere una mossa.
NOTA(2): Il tempo deve essere specificato in millisecondi