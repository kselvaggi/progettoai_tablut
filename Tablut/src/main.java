import java.io.IOException;
import java.io.ObjectInputStream.GetField;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import it.unibo.ai.didattica.competition.tablut.bitboard.*;
import it.unibo.ai.didattica.competition.tablut.domain.*;
import it.unibo.ai.didattica.competition.tablut.search.*;

public class main {

	
	public static void main(String[] args) {
		
		BitboardState state = new BitboardState();
		BitboardState cloneState = new BitboardState();
				
		/** TESTING SYMMETRIES **/
//		state.move(BitboardState.C5, BitboardState.C9);//
//		state.move(BitboardState.B5, BitboardState.C5);
//		state.move(BitboardState.D5, BitboardState.D5);//
//		state.move(BitboardState.A4, BitboardState.C4);
//		state.move(BitboardState.E4, BitboardState.D4);//
//		state.move(BitboardState.F1, BitboardState.G3);
//		state.move(BitboardState.G5, BitboardState.B4);//
//		state.move(BitboardState.E1, BitboardState.E1);
//		state.move(BitboardState.E7, BitboardState.G4);//
//		state.move(BitboardState.G4, BitboardState.G4);
//		state.move(BitboardState.E6, BitboardState.G2);//
//		state.move(BitboardState.E9, BitboardState.F2);
//		state.move(BitboardState.C5, BitboardState.D4);//
//		System.out.println("NORMAL: \n" + BitboardUtility.boardToString(state.getBoard()[0]));
		
		// TESTING VERTICAL FLIP
//		int[] flipped_board = BitboardUtility.mirrorVertical(state.getBoard()[1]);
//		System.out.println("FLIPPED: \n" + BitboardUtility.boardToString(flipped_board));
		
		// TESTING HORIZONTAL FLIP
//		int[] flipped_board = BitboardUtility.mirrorHorizontal(state.getBoard()[1]);
//		System.out.println("FLIPPED: \n" + BitboardUtility.boardToString(flipped_board));
		
		// TESTING DIAGONAL FLIP
		// Test #1
//		int [] test_board = new int[3];
		// Test board 0
//		test_board[0] = 0x21108E8;
//		test_board[1] = 0x3800088;
//		test_board[2] = 0x21110A0;
		
		// Test board 1
//		test_board[0] = 0x21108FC;
//		test_board[1] = 0x3C13088;
//		test_board[2] = 0x21130A0;
//		System.out.println("NORMAL: \n" + BitboardUtility.boardToString(test_board));
//		
//		int[] flipped_board = BitboardUtility.mirrorDiagonal(test_board);
//		System.out.println("FLIPPED: \n" + BitboardUtility.boardToString(flipped_board) + " " + BitboardUtility.getBoardIndex(flipped_board[2]));
		
//		// Test #2 (CON STATE)
//		int[] flipped_board = BitboardUtility.mirrorDiagonal(state.getBoard()[0]);
//		System.out.println("FLIPPED: \n" + BitboardUtility.boardToString(flipped_board));
		
		// TESTING 90� ROTATION
//		int[] flipped_board = BitboardUtility.rotationClockwise90(test_board);
//		System.out.println("FLIPPED: \n" + BitboardUtility.boardToString(flipped_board));
					  
		/***********************/
		
//		BitboardHistoryTable histTable = new BitboardHistoryTable();
//		BitboardAction action;
//		try {
//			action = new BitboardAction(BitboardState.A9, BitboardState.I1, Turn.BLACK);
//			histTable.incrementValue(action, 2);
//			long value = histTable.getValue(action);
//			action = new BitboardAction(BitboardState.A9, BitboardState.A2, Turn.BLACK);
//			histTable.incrementValue(action, 2);
//			value = histTable.getValue(action);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		
//		long temp = state.getBoard()[0][0];
//		long hash1 = BitboardUtility.getHash(state);
//		long hash2 = BitboardUtility.getHash(cloneState);
//		
//		if (hash1 == hash2) {
//			System.out.println("yo");
//		}
		
		/** TEST SEVERAL MOVES **/
		// Test mangia re dentro il castello
//		state.move(BitboardState.D5, BitboardState.A1);//
//		state.move(BitboardState.B5, BitboardState.D5);
//		state.move(BitboardState.E4, BitboardState.B1);//
//		state.move(BitboardState.A4, BitboardState.E4);
//		state.move(BitboardState.E6, BitboardState.C1);//
//		state.move(BitboardState.A5, BitboardState.E6);
//		state.move(BitboardState.F5, BitboardState.A2);//
//		state.move(BitboardState.A6, BitboardState.F5);
		
		// Test mangia re a sinistra del castello
//		state.move(BitboardState.D5, BitboardState.A1);//
//		state.move(BitboardState.B5, BitboardState.D4);
//		state.move(BitboardState.E5, BitboardState.D5);//
//		state.move(BitboardState.A4, BitboardState.D6);
//		state.move(BitboardState.C5, BitboardState.B1);//
//		state.move(BitboardState.A5, BitboardState.C5);
		
		// Test mangia re sotto al castello
//		state.move(BitboardState.D5, BitboardState.A1);//
//		state.move(BitboardState.A4, BitboardState.D6);
//		state.move(BitboardState.E5, BitboardState.D5);//
//		state.move(BitboardState.A5, BitboardState.C5);
//		state.move(BitboardState.C5, BitboardState.B1);//
//		state.move(BitboardState.B5, BitboardState.D4);
		
		// Test mangia re sopra castello
//		state.move(BitboardState.D5, BitboardState.A1);//
//		state.move(BitboardState.B5, BitboardState.D4);
//		state.move(BitboardState.E5, BitboardState.D5);//
//		state.move(BitboardState.A5, BitboardState.C5);
//		state.move(BitboardState.C5, BitboardState.B1);//
//		state.move(BitboardState.A4, BitboardState.D6);
		
		// Test mangia re a sinistra del castello
//		state.move(BitboardState.F5, BitboardState.A1);//
//		state.move(BitboardState.B5, BitboardState.F4);
//		state.move(BitboardState.E5, BitboardState.F5);//
//		state.move(BitboardState.A4, BitboardState.F6);
//		state.move(BitboardState.G5, BitboardState.B1);//
//		state.move(BitboardState.A5, BitboardState.G5);
		
		// Test mangia re a sinistra completamente fuori castello
//		state.move(BitboardState.E5, BitboardState.D2);//
//		state.move(BitboardState.B5, BitboardState.D3);
		
		// Test mangia re a sinistra completamente fuori castello
//		state.move(BitboardState.E3, BitboardState.A1);//
//		state.move(BitboardState.B5, BitboardState.D3);
//		state.move(BitboardState.E5, BitboardState.E3);//
//		state.move(BitboardState.A5, BitboardState.F3);
		
		// Test movimenti vari
//		state.move(BitboardState.C5, BitboardState.C9);//
//		state.move(BitboardState.B5, BitboardState.C5);
//		state.move(BitboardState.D5, BitboardState.D5);//
//		state.move(BitboardState.A4, BitboardState.C4);
//		state.move(BitboardState.E4, BitboardState.D4);//
//		state.move(BitboardState.F1, BitboardState.G3);
//		state.move(BitboardState.G5, BitboardState.B4);//
//		state.move(BitboardState.E1, BitboardState.E1);
//		state.move(BitboardState.E7, BitboardState.G4);//
//		state.move(BitboardState.G4, BitboardState.G4);
//		state.move(BitboardState.E6, BitboardState.G2);//
//		state.move(BitboardState.E9, BitboardState.F2);
//		state.move(BitboardState.C5, BitboardState.D4);//
//		state.move(BitboardState.B5, BitboardState.C5);
//		state.move(BitboardState.D5, BitboardState.D6);//
//		state.move(BitboardState.E6, BitboardState.D7);//
		
//		System.out.println("KING: \n" + BitboardUtility.boardToString(state.getBoard()[2]) + "\n");
//		System.out.println("WHITE: \n" + BitboardUtility.boardToString(state.getBoard()[0]) + "\n");
//		System.out.println("BLACK: \n" + BitboardUtility.boardToString(state.getBoard()[1]) + "\nTurn: " + state.getTurn());
		
//		System.out.println("KING: \n" + BitboardUtility.boardToString(cloneState.getBoard()[2]) + "\n");
//		System.out.println("WHITE: \n" + BitboardUtility.boardToString(cloneState.getBoard()[0]) + "\n");
//		System.out.println("BLACK: \n" + BitboardUtility.boardToString(cloneState.getBoard()[1]) + "\nTurn: " + state.getTurn());
// 		System.out.println("STATE: \n" + state + "\nTurn: " + state.getTurn() + "\n");
 		
		/***** MATCH SIMULATION *****/
		
		// Strutture per esecuzione di alcuni algoritmi
		BitboardTieChecker tieChecker = new BitboardTieChecker();
		BitboardTranspositionTable transpositionTable = new BitboardTranspositionTable();	
//		BitboardTranspositionTable transpositionTable1 = new BitboardTranspositionTable();
 		BitboardHistoryTable historyTable = new BitboardHistoryTable();
 		BitboardHistoryTable historyTable1 = new BitboardHistoryTable();
 		BitboardButterflyTable butterflyTable = new BitboardButterflyTable();
 		BitboardButterflyTable butterflyTable1 = new BitboardButterflyTable();
 		
 		// Algoritmi vari
// 		MinMaxAlphaBetaAlgorithm alg = new MinMaxAlphaBetaAlgorithm();
// 		MinMaxAlphaBetaTTAlgorithmV2 alg = new MinMaxAlphaBetaTTAlgorithmV2(transpositionTable);
// 		MinMaxAlphaBetaTTAlgorithm alg = new MinMaxAlphaBetaTTAlgorithm();
 		
 		MinMaxAlphaBetaHistoryAlgorithm alg1 = new MinMaxAlphaBetaHistoryAlgorithm(tieChecker, historyTable);
 		MinMaxAlphaBetaHistoryAlgorithm alg = new MinMaxAlphaBetaHistoryAlgorithm(tieChecker, historyTable1);
// 		MinMaxAlphaBetaRelativeHistory alg = new MinMaxAlphaBetaRelativeHistory(historyTable2, butterflyTable);
// 		RelativeHistoryAlphaBetaTransposition alg = new RelativeHistoryAlphaBetaTransposition(tieChecker, historyTable, transpositionTable, butterflyTable);
// 		RelativeHistoryAlphaBetaTransposition alg1 = new RelativeHistoryAlphaBetaTransposition(tieChecker, historyTable1, transpositionTable, butterflyTable1);
// 		HistoryAlphaBetaTransposition alg = new HistoryAlphaBetaTransposition(historyTable, transpositionTable);
// 		HistoryAlphaBetaTransposition alg1 = new HistoryAlphaBetaTransposition(historyTable1, transpositionTable1);

 		// Esecuzione
 		int whiteDepth = 6;
 		int blackDepth = 4;
 		
 		for (int i=0; i < 100; i++) {
// 			if (i == 3)
// 				transpositionTable.clear();
 			if ( (i%2) == 0 ) {
 				tieChecker.addState(state);
 				state.move(alg.searchBestAction(state, whiteDepth, true).getAction());
 				System.out.println("STATE" + (i+1) + ": \n" + state + "\nTurn: " + state.getTurn() + "\n");
// 				transpositionTable.clear();
 			}
 			else {
 				tieChecker.addState(state);
 				state.move(alg1.searchBestAction(state, blackDepth, false).getAction());
 				System.out.println("STATE" + (i+1) + ": \n" + state + "\nTurn: " + state.getTurn() + "\n");
 			}
 		}
 		
 		/***********************************/
 		
		// Testing sector set
//		int move = 1;
//		int sector = 2;
//		int moveToAdd = BitboardUtility.setSector(move, sector);
//		System.out.println("Move: " + Integer.toHexString(moveToAdd));
		
 		/** GET MOVES SET **/
//		try {
//			List<IAction> pawnMoves = state.getPawnMoves(BitboardState.B7);
//			System.out.println("Pawn moves:\n" + pawnMoves + "\n" +  BitboardUtility.printAllMoves(pawnMoves));
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		pawnMoves[0] = pawnMoves[0] | BitboardState.CAMP_3;
//		pawnMoves[1] = pawnMoves[1] | BitboardState.CAMP_3;
//		pawnMoves[2] = pawnMoves[2] | BitboardState.CAMP_3;
		
		// Scan moves
//		for (int i = 0 ; i < pawnMoves.length ; i++) {
//			int move = BitboardState.scanMove(pawnMoves[i]);
//			while (pawnMoves[i] != 0) {
//				pawnMoves[i] = pawnMoves[i] - move;
//				System.out.println("Selected move:\n" + BitboardUtility.printSector(move));
//				move = BitboardState.scanMove(pawnMoves[i]);
//			}
//		}
		
 		/** GET ALL MOVES **/
//		try {
//			List<IAction> result = new ArrayList<>();
//			result = state.getCurrentMoves();
////			for (int i=0; i< state.getBoard()[state.getTurn().ordinal()].length ; i++) {
////				int temp_state = (BitboardState.BOARD_PAWNS_MASK & state.getBoard()[state.getTurn().ordinal()][i]);
////				System.out.println("Selected move:\n" + BitboardUtility.printSector(temp_state));
//////				int pawnToMove = BitboardState.scanMove(state.getBoard()[0][i]);
//////				pawnToMove = BitboardUtility.setSector(pawnToMove, i);
////				while (temp_state != 0) {
////					int pawnToMove = BitboardState.scanMove(temp_state);
////					pawnToMove = BitboardUtility.setSector(pawnToMove, i);
////					List<IAction> pawnMoves = state.getPawnMoves(pawnToMove);
////					result.addAll(pawnMoves);
////					System.out.println("Pawn moves:\n" + pawnMoves + "\n" +  BitboardUtility.printAllMoves(pawnMoves));
////					temp_state ^= (BitboardState.BOARD_PAWNS_MASK & pawnToMove);
////					System.out.println("Current state:\n" + BitboardUtility.printSector(temp_state));
////					System.out.println("Selected move:\n" + BitboardUtility.printSector(pawnToMove));
////					System.out.println("Selected move:\n" + Integer.toHexString(pawnToMove));
//////					pawnToMove = BitboardState.scanMove(temp_state);
//////					pawnToMove = BitboardUtility.setSector(pawnToMove, i);
////				}
////			}
//			System.out.println("Pawn moves:\n" + result + "\n" +  BitboardUtility.printAllMoves(result));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
// 		state.move(BitboardState.E5, BitboardState.B2);
//		if (Integer.numberOfTrailingZeros(state.getBoard()[2][0]) < 27)
//			try {
//				System.out.println(BitboardUtility.printAllMoves(state.getPawnMoves(state.getBoard()[2][0])));
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
 		
 		
 		/** HEURISTIC FUNCTION TEST SET **/
 		/* 1) black around king */
// 		int blackAroundKing = state.blackAroundKingValue();
// 		System.out.println("Minacing entities around King: " + blackAroundKing); // should be 0
 		
		// Test re a sinistra del castello con due pedine nere intorno
//		state.move(BitboardState.F5, BitboardState.A1);//
//		state.move(BitboardState.B5, BitboardState.F4);
//		state.move(BitboardState.E5, BitboardState.F5);//
//		state.move(BitboardState.A4, BitboardState.F6);
//		System.out.println(state);
// 		blackAroundKing = state.blackAroundKingValue();
// 		System.out.println("Minacing entities around King: " + blackAroundKing); 
 		
		// Test re a sinistra del castello con due pedine nere intorno
//		state.move(BitboardState.F5, BitboardState.A1);//
//		state.move(BitboardState.B5, BitboardState.F4);
//		state.move(BitboardState.E5, BitboardState.F5);//
//		state.move(BitboardState.A4, BitboardState.F6);
//		System.out.println(state);
// 		blackAroundKing = state.blackAroundKingValue();
// 		System.out.println("Minacing entities around King: " + blackAroundKing); 
 		
		// Test re  sotto il castello con due pedine nere intorno
//		state.move(BitboardState.E6, BitboardState.A1);//
//		state.move(BitboardState.B5, BitboardState.F6);
//		state.move(BitboardState.E5, BitboardState.E6);//
//		state.move(BitboardState.A4, BitboardState.D6);
//		state.move(BitboardState.E7, BitboardState.A2);//
//		state.move(BitboardState.D6, BitboardState.E7);
//		System.out.println(state);
// 		blackAroundKing = state.blackAroundKingValue();
// 		System.out.println("Minacing entities around King: " + blackAroundKing); 
 		
		// Test re sopra il castello con due pedine nere intorno
//		state.move(BitboardState.E4, BitboardState.A1);//
//		state.move(BitboardState.B5, BitboardState.F4);
//		state.move(BitboardState.E5, BitboardState.E4);//
//		state.move(BitboardState.A4, BitboardState.D6);
//		state.move(BitboardState.E3, BitboardState.A2);//
//		state.move(BitboardState.D6, BitboardState.E3);
//		System.out.println(state);
// 		blackAroundKing = state.blackAroundKingValue();
// 		System.out.println("Minacing entities around King: " + blackAroundKing); 
 		
 		/* 2) num pieces */
//		int[] numPieces = state.getNumPieces();
//		System.out.println("Num whites: " + numPieces[0]);
//		System.out.println("Num blacks: " + numPieces[1]);
	}
}