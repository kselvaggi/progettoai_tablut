package it.unibo.ai.didattica.competition.tablut.client;

import java.io.IOException;
import java.net.UnknownHostException;

import it.unibo.ai.didattica.competition.tablut.bitboard.BitboardHistoryTable;
import it.unibo.ai.didattica.competition.tablut.bitboard.BitboardState;
import it.unibo.ai.didattica.competition.tablut.bitboard.BitboardTieChecker;
import it.unibo.ai.didattica.competition.tablut.bitboard.BitboardUtility;
import it.unibo.ai.didattica.competition.tablut.bitboard.IAction;
import it.unibo.ai.didattica.competition.tablut.domain.*;
import it.unibo.ai.didattica.competition.tablut.domain.State.Turn;
import it.unibo.ai.didattica.competition.tablut.search.IterativeDeepening;
import it.unibo.ai.didattica.competition.tablut.search.MinMaxAlphaBetaHistoryAlgorithm;

/**
 * Bitboard client implementation that is capable to connect to the server.
 * 
 * @author Team CAMST - Tablut competition 2019
 *
 */
public class TablutBitboardClient extends TablutClient {
	private int startingDepth;
	private int maxmillis;
	private int securitymillis;
	private int startingdepth;
	private static int depthDefault = 6;

	public static final int STARTINGDEPTH = 1;
	public static final int MAXMILLIS = 60000;
	public static final int SECURITYMILLIS = 3000;
	

	public TablutBitboardClient(String player, String name, int startingDepth) throws UnknownHostException, IOException {
		super(player, name);
		this.startingDepth = startingDepth;
		securitymillis = SECURITYMILLIS;
		startingdepth = STARTINGDEPTH;
		maxmillis = MAXMILLIS;
	}
	
	public TablutBitboardClient(String player, String name, int startingDepth, int maxmillis) throws UnknownHostException, IOException {
		this(player, name, startingDepth);
		this.maxmillis = maxmillis;
		securitymillis = SECURITYMILLIS;
	}

//	public TablutBitboardClient(String player) throws UnknownHostException, IOException {
//		this(player, "proto", 4, depthDefault);
//	}
//
//	public TablutBitboardClient(String player, String name) throws UnknownHostException, IOException {
//		this(player, name, 4, depthDefault);
//	}
//
//	public TablutBitboardClient(String player, int depthChosen) throws UnknownHostException, IOException {
//		this(player, "proto", 4, depthChosen);
//	}

	//	public TablutBitboardClientOldHeuristic(String player, int gameChosen, int depthChosen) throws UnknownHostException, IOException {
	//		this(player, "proto", gameChosen, depthChosen);
	//	}

	public static void main(String[] args) throws UnknownHostException, IOException, ClassNotFoundException {
		String role = "";
		String name = "Team CAMST";
		int maxmillis = MAXMILLIS;
		// TODO: change the behavior?
		if (args.length < 1) {
			System.out.println("You must specify which player you are (WHITE or BLACK)");
			System.exit(-1);
		} else {
			System.out.println(args[0]);
			role = (args[0]);
		}
		if (args.length == 2) {
			System.out.println(args[1]);
			maxmillis = Integer.parseInt(args[1]);
		}
		
		System.out.println("Selected client: " + args[0]);
		System.out.println("Max millis: " + maxmillis);

		TablutBitboardClient client = new TablutBitboardClient(role, name, STARTINGDEPTH, maxmillis);
		client.run();
	}

	@Override
	public void run() {

		try {
			this.declareName();
		} catch (Exception e) {
			e.printStackTrace();
		}

		BitboardState bitboardState = null;
		
		// Stato originale del server
		State state = new StateTablut();
		
		// Oggetti necessari per gli algoritmi di ricerca
		BitboardTieChecker tieChecker = new BitboardTieChecker();
		BitboardHistoryTable historyTable = new BitboardHistoryTable();
//		BitboardTranspositionTable transpositionTable = new BitboardTranspositionTable();
//		BitboardButterflyTable butterflyTable = new BitboardButterflyTable();
		
		// Algoritmo di ricerca
		MinMaxAlphaBetaHistoryAlgorithm alg = new MinMaxAlphaBetaHistoryAlgorithm(tieChecker, historyTable);
//		RelativeHistoryAlphaBetaTransposition alg = new RelativeHistoryAlphaBetaTransposition(tieChecker, historyTable, transpositionTable, butterflyTable);
		
		// Iterative deeping
		IterativeDeepening iterativeDeepening;
		Thread iterativeDeepeningThread;
		
		
		System.out.println("You are player " + this.getPlayer().toString() + "!");

		while (true) {
			try {
				// Leggo lo stato mandato dal server
				this.read();
			} catch (ClassNotFoundException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				System.exit(1);
			}
			
			System.out.println("Current state:");
			state = this.getCurrentState();
			System.out.println(state.toString());

			// Traduco lo stato del server in uno stato bitboard
			bitboardState = BitboardUtility.getBitboard((StateTablut) state);
			tieChecker.addState(bitboardState);
			
			if (this.getPlayer().equals(Turn.WHITE)) { 
				/** TURNO BIANCO **/
				if (this.getCurrentState().getTurn().equals(StateTablut.Turn.WHITE)) {			

					long curMillis = System.currentTimeMillis();

					iterativeDeepening = new IterativeDeepening(alg, bitboardState, startingdepth, true);		
					iterativeDeepeningThread = new Thread(iterativeDeepening);
					iterativeDeepeningThread.start();


					long millis = maxmillis - (System.currentTimeMillis()-curMillis);
					while (millis > securitymillis) {
						curMillis = System.currentTimeMillis();
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							System.out.println("Shouldn't happen...");
							e.printStackTrace();
						}
						millis -= System.currentTimeMillis() - curMillis;
						if (!iterativeDeepeningThread.isAlive())
							break;

					}

					if (iterativeDeepeningThread.isAlive())
						iterativeDeepeningThread.stop();

					System.out.println("Thread time duration: " + millis);

					IAction bestMove = iterativeDeepening.getSelectedValuedAction().getAction();

					System.out.println("Chosen move: " + bestMove.toString());
					try {
						// Create move readible from the server
						Action a = new Action(bestMove.getFrom(), bestMove.getTo(), StateTablut.Turn.valueOf(bestMove.getTurn().name()) );

						this.write(a);
					} catch (ClassNotFoundException | IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				// � il turno dell'avversario
				else if (state.getTurn().equals(StateTablut.Turn.WHITE)) {
					System.out.println("Waiting for your opponent move... ");
				} else if (state.getTurn().equals(StateTablut.Turn.WHITEWIN)) {
					System.out.println("YOU WIN!");
					System.exit(0);
				} else if (state.getTurn().equals(StateTablut.Turn.BLACKWIN)) {
					System.out.println("YOU LOSE!");
					System.exit(0);
				} else if (state.getTurn().equals(StateTablut.Turn.DRAW)) {
					System.out.println("DRAW!");
					System.exit(0);
				}

			} else { 
				/** 	TURNO NERO 	**/
				if (this.getCurrentState().getTurn().equals(StateTablut.Turn.BLACK)) {
					long curMillis = System.currentTimeMillis();

					iterativeDeepening = new IterativeDeepening(alg, bitboardState, startingdepth, false);		
					iterativeDeepeningThread = new Thread(iterativeDeepening);
					iterativeDeepeningThread.start();

					long millis = maxmillis - (System.currentTimeMillis()-curMillis);
					while (millis > securitymillis) {
						curMillis = System.currentTimeMillis();
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							System.out.println("Shouldn't happen...");
							e.printStackTrace();
						}
						millis -= System.currentTimeMillis() - curMillis;
						if (!iterativeDeepeningThread.isAlive())
							break;

					}

					if (iterativeDeepeningThread.isAlive())
						iterativeDeepeningThread.stop();

					System.out.println("Thread time duration: " + millis);

					IAction bestMove = iterativeDeepening.getSelectedValuedAction().getAction();

					System.out.println("Chosen move: " + bestMove.toString());
					try {
						Action a = new Action(bestMove.getFrom(), bestMove.getTo(), StateTablut.Turn.valueOf(bestMove.getTurn().name()) );

						this.write(a);
					} catch (ClassNotFoundException | IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (state.getTurn().equals(StateTablut.Turn.WHITE)) {
					System.out.println("Waiting for your opponent move... ");
				} else if (state.getTurn().equals(StateTablut.Turn.WHITEWIN)) {
					System.out.println("YOU LOSE!");
					System.exit(0);
				} else if (state.getTurn().equals(StateTablut.Turn.BLACKWIN)) {
					System.out.println("YOU WIN!");
					System.exit(0);
				} else if (state.getTurn().equals(StateTablut.Turn.DRAW)) {
					System.out.println("DRAW!");
					System.exit(0);
				}
			}
		}
	}
}