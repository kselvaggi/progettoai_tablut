package it.unibo.ai.didattica.competition.tablut.search;

import it.unibo.ai.didattica.competition.tablut.bitboard.IState;

public interface SearchingAlgorithms {
	
	public ValuedAction searchBestAction(IState state, int depth, boolean isMaximizingPlayer);
//	public ValuedAction searchBestAction(State state, int depth) throws IOException; //per MinMaxAlphaBetaTTAlgorithmV2
	
}
