package it.unibo.ai.didattica.competition.tablut.bitboard;

/**
 * This class represents the removed pawns, used by the actions in the unmove()
 * 
 * @author Team CAMST
 * 
 */
public class RemovedPawn {

	private int removedPawn;
	private boolean isKing;

	public RemovedPawn() {
		this.removedPawn = -1;
		this.isKing = false;
	}

	public int getRemovedPawn() {
		return removedPawn;
	}

	public void setRemovedPawn(int removedPawn) {
		this.removedPawn = removedPawn;
	}

	public boolean isKing() {
		return isKing;
	}

	public void setKing(boolean isKing) {
		this.isKing = isKing;
	}
}
