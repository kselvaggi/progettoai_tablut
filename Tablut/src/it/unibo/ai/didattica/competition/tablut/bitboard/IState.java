package it.unibo.ai.didattica.competition.tablut.bitboard;

import java.util.List;
/**
 * This class interface of the State
 * 
 * @author Team CAMST
 * 
 */
public interface IState {
	
//	public void move(Action action, Game game);
//	public void move(int action, Game game);
	public void move(IAction action);
	public void move(int from, int to);
	public void unmove (IAction action);
	public int getHeuristicValue();
	public boolean isWinningState();
	public boolean hasBlackWon();
	public boolean hasWhiteWon();
	
	public List<IAction> getCurrentMoves();
	public Turn getTurn();
	public IState clone();
	public void setTurn(Turn turn);
	
	public List<IAction> getKingMoves();
	
	// Test client
//	public int getHeuristicOldValue();
//	public int getHeuristicValue2();
//	public int getHeuristicValue3();
//	public int getHeuristicValue4();
//	public int getHeuristicValue5();
	
	
}
