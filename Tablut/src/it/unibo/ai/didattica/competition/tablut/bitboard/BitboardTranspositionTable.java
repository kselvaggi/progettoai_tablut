package it.unibo.ai.didattica.competition.tablut.bitboard;

import java.io.IOException;

import org.apache.commons.collections4.map.LRUMap;

import it.unibo.ai.didattica.competition.tablut.search.ITranspositionTable;

/**
 * This class represents a Trasposition Table
 * 
 * We took inspiration from the code of another project (Here's the link: https://github.com/ilceltico/CIRAM-Mill-AI-Division) 
 * 
 * @author Team CAMST
 * 
 */
public class BitboardTranspositionTable implements ITranspositionTable {

	public static final int DEFAULT_SIZE = 3500000; //MAX SIZE is 5600000

	private LRUMap<String, BitBoardEntry> transpositionTable;

	public BitboardTranspositionTable(int size) {
		transpositionTable = new LRUMap<>(size, size);
	}
	
	public BitboardTranspositionTable() {
		transpositionTable = new LRUMap<>(DEFAULT_SIZE, DEFAULT_SIZE);
	}


	public BitboardAction getAction(IState state) throws IOException{
		BitboardState bState = (BitboardState) state;
		BitboardHash bHash = BitboardHash.getHashSymm(bState);
		String hash = bHash.getHash();
		byte symms = bHash.getSymms();
		int boardIndex;
		int[] tempBoard = new int[3];

		BitBoardEntry entry = transpositionTable.get(hash);
		if (entry == null)
			return null;

		int from;
		int to;
		Turn turn;

		from = entry.action.getBitboardFrom();
		to = entry.action.getBitboardTo();
		turn = entry.action.getTurn();
//		System.out.println("From: \n" + BitboardUtility.printSector(from) + " Board index: " + BitboardUtility.getBoardIndex(from));
//		System.out.println("To: \n" + BitboardUtility.printSector(to) + " Board index: " + BitboardUtility.getBoardIndex(to));
		
		switch(symms & 0b00011) {

		case BitboardHash.ROTATION_90:
			tempBoard = new int[3];
			boardIndex = BitboardUtility.getBoardIndex(from);
			tempBoard[boardIndex] |= from;
			tempBoard = BitboardUtility.rotationAntiClockwise90(tempBoard);
			from = getMoveBoardSection(tempBoard);
//			System.out.println("From: \n" + BitboardUtility.printSector(from) + " Board index: " + BitboardUtility.getBoardIndex(from));
			
			tempBoard = new int[3];
			boardIndex = BitboardUtility.getBoardIndex(to);
			tempBoard[boardIndex] |= to;
			tempBoard = BitboardUtility.rotationAntiClockwise90(tempBoard);
			to = getMoveBoardSection(tempBoard);
//			System.out.println("To: \n" + BitboardUtility.printSector(to) + " Board index: " + BitboardUtility.getBoardIndex(to));
			break;

		case BitboardHash.ROTATION_180:
			tempBoard = new int[3];
			boardIndex = BitboardUtility.getBoardIndex(from);
			tempBoard[boardIndex] |= from;
			tempBoard = BitboardUtility.rotationClockwise180(tempBoard);
			from = getMoveBoardSection(tempBoard);
			
			tempBoard = new int[3];
			boardIndex = BitboardUtility.getBoardIndex(to);
			tempBoard[boardIndex] |= to;
			tempBoard = BitboardUtility.rotationClockwise180(tempBoard);
			to = getMoveBoardSection(tempBoard);
			break;

		case BitboardHash.ROTATION_270:
			tempBoard = new int[3];
			boardIndex = BitboardUtility.getBoardIndex(from);
			tempBoard[boardIndex] |= from;
			tempBoard = BitboardUtility.rotationClockwise90(tempBoard);
			from = getMoveBoardSection(tempBoard);
			
			tempBoard = new int[3];
			boardIndex = BitboardUtility.getBoardIndex(to);
			tempBoard[boardIndex] |= to;
			tempBoard = BitboardUtility.rotationClockwise90(tempBoard);
			to = getMoveBoardSection(tempBoard);
			
			break;
		default:
		}
		
		if((symms & BitboardHash.MIRROR_VERTICAL) != 0) {
			tempBoard = new int[3];
			boardIndex = BitboardUtility.getBoardIndex(from);
			tempBoard[boardIndex] |= from;
			tempBoard = BitboardUtility.mirrorVertical(tempBoard);
			from = getMoveBoardSection(tempBoard);
//			System.out.println("From: \n" + BitboardUtility.printSector(from) + " Board index: " + BitboardUtility.getBoardIndex(from));
			
			tempBoard = new int[3];
			boardIndex = BitboardUtility.getBoardIndex(to);
			tempBoard[boardIndex] |= to;
			tempBoard = BitboardUtility.mirrorVertical(tempBoard);
			to = getMoveBoardSection(tempBoard);
//			System.out.println("To: \n" + BitboardUtility.printSector(to) + " Board index: " + BitboardUtility.getBoardIndex(to));
		}
	
		BitboardAction result = new BitboardAction(from, to, turn);
//    	if (result.getFrom().equalsIgnoreCase("e5") && result.getTo().equalsIgnoreCase("c5")) {
//    		System.out.println("ALT");
//    	}
    	
//    	int indTemp = BitboardUtility.getBoardIndex(result.getBitboardTo());
//    	if ((result.getBitboardTo() | bState.getBoard()[0][indTemp]) == bState.getBoard()[0][indTemp] ||
//    			(result.getBitboardTo() | bState.getBoard()[1][indTemp]) == bState.getBoard()[1][indTemp] ||
//    			(result.getBitboardTo() | bState.getBoard()[2][indTemp]) == bState.getBoard()[2][indTemp] ) {
//    		System.out.println("Collision? ");
//    		return null;
//    	}
//    	if (!((result.getBitboardFrom() | bState.getBoard()[0][indTemp]) == bState.getBoard()[0][indTemp] ||
//    			(result.getBitboardFrom() | bState.getBoard()[1][indTemp]) == bState.getBoard()[1][indTemp] ||
//    			(result.getBitboardFrom() | bState.getBoard()[2][indTemp]) == bState.getBoard()[2][indTemp] )) {
//    		System.out.println("Collision? ");
//    		return null;
//    	}
//    	indTemp = BitboardUtility.getBoardIndex(result.getBitboardFrom());
//    	if ((0x07ffffff & (result.getBitboardFrom() | bState.getBoard()[0][indTemp])) != (0x07ffffff & bState.getBoard()[0][indTemp]) ||
//    			(0x07ffffff & (result.getBitboardFrom() | bState.getBoard()[1][indTemp])) != (0x07ffffff &bState.getBoard()[1][indTemp]) ||
//    			(0x07ffffff & (result.getBitboardFrom() | bState.getBoard()[2][indTemp])) != (0x07ffffff &bState.getBoard()[2][indTemp] )) {
//    		System.out.println("ALT");
//    	}
		return result;	
	}

	private int getMoveBoardSection(int[] tempBoard) {
		for(int i=0; i<3; i++) {
			if((tempBoard[i] & 0x07ffffff) != 0) {
				return tempBoard[i];
			}
		}
		return -1;
	}



	public void putAction(IState state, IAction action, int depth) {
		if (action == null)
			throw new IllegalArgumentException("action\n" + state);
		BitboardState bState = (BitboardState) state;
		BitboardHash bHash = BitboardHash.getHashSymm(bState);
		String hash = bHash.getHash();

		byte symms = bHash.getSymms();
		BitboardAction actionToPut = (BitboardAction) action;
		int boardIndex;
		int[] tempBoard = new int[3];
		
		int from = actionToPut.getBitboardFrom();
		int to = actionToPut.getBitboardTo();
//		System.out.println("Initial From: \n" + BitboardUtility.printSector(from));
//		System.out.println("InitialTo: \n" + BitboardUtility.printSector(to) + "\n");
		Turn turn = actionToPut.getTurn();
		
		
		if((symms & BitboardHash.MIRROR_VERTICAL) != 0) {
			boardIndex = BitboardUtility.getBoardIndex(from);
			tempBoard[boardIndex] |= from;
			tempBoard = BitboardUtility.mirrorVertical(tempBoard);
			from = getMoveBoardSection(tempBoard);
//			System.out.println("From: \n" + BitboardUtility.printSector(from) + " Board index: " + BitboardUtility.getBoardIndex(from));

			tempBoard = new int[3];
			boardIndex = BitboardUtility.getBoardIndex(to);
			tempBoard[boardIndex] |= to;
			tempBoard = BitboardUtility.mirrorVertical(tempBoard);
			to = getMoveBoardSection(tempBoard);
//			System.out.println("To: \n" + BitboardUtility.printSector(to) + " Board index: " + BitboardUtility.getBoardIndex(to));
		} 
		
		tempBoard = new int[3];
		switch(symms & 0b00011) {

		case BitboardHash.ROTATION_90:
			boardIndex = BitboardUtility.getBoardIndex(from);
			tempBoard[boardIndex] |= from;
			tempBoard = BitboardUtility.rotationClockwise90(tempBoard);
			from = getMoveBoardSection(tempBoard);
//			System.out.println("From: \n" + BitboardUtility.printSector(from) + " Board index: " + BitboardUtility.getBoardIndex(from));
			
			tempBoard = new int[3];
			boardIndex = BitboardUtility.getBoardIndex(to);
			tempBoard[boardIndex] |= to;
			tempBoard = BitboardUtility.rotationClockwise90(tempBoard);
			to = getMoveBoardSection(tempBoard);
//			System.out.println("From: \n" + BitboardUtility.printSector(to) + " Board index: " + BitboardUtility.getBoardIndex(to));
			break;

		case BitboardHash.ROTATION_180:
			boardIndex = BitboardUtility.getBoardIndex(from);
			tempBoard[boardIndex] |= from;
			tempBoard = BitboardUtility.rotationClockwise180(tempBoard);
			from = getMoveBoardSection(tempBoard);

			tempBoard = new int[3];
			boardIndex = BitboardUtility.getBoardIndex(to);
			tempBoard[boardIndex] |= to;
			tempBoard = BitboardUtility.rotationClockwise180(tempBoard);
			to = getMoveBoardSection(tempBoard);
			break;

		case BitboardHash.ROTATION_270:
			boardIndex = BitboardUtility.getBoardIndex(from);
			tempBoard[boardIndex] |= from;
			tempBoard = BitboardUtility.rotationAntiClockwise90(tempBoard);
			from = getMoveBoardSection(tempBoard);
//			System.out.println("From: \n" + BitboardUtility.printSector(from) + " Board index: " + BitboardUtility.getBoardIndex(from));
			
			tempBoard = new int[3];
			boardIndex = BitboardUtility.getBoardIndex(to);
			tempBoard[boardIndex] |= to;
			tempBoard = BitboardUtility.rotationAntiClockwise90(tempBoard);
			to = getMoveBoardSection(tempBoard);
//			System.out.println("To: \n" + BitboardUtility.printSector(to) + " Board index: " + BitboardUtility.getBoardIndex(to));
			break;
		default:
		}

		BitboardAction transformedActionToPut = new BitboardAction(from, to, turn);

		BitBoardEntry entry = transpositionTable.get(hash);
		if (entry == null) {
			entry = new BitBoardEntry();
			entry.setDepth((byte) depth);
			entry.setAction(transformedActionToPut);
			
			transpositionTable.put(hash, entry);
		}
	}

	public void clear() {
		transpositionTable = new LRUMap<>(DEFAULT_SIZE, DEFAULT_SIZE);
		//		transpositionTable.clear();
	}

	class BitBoardEntry {
		private BitboardAction action;
		private byte depth;
		//		private BitBoardState state;

		BitBoardEntry() {
		}

		BitboardAction getAction() {
			return action;
		}

		void setAction(BitboardAction action) {
			this.action = action;
		}

		byte getDepth() {
			return depth;
		}

		void setDepth(byte depth) {
			this.depth = depth;
		}

		//		public BitBoardState getState() {
		//		return state;
		//	}
		//
		//	public void setState(BitBoardState state) {
		//		this.state = state;
		//	}

	}

}
