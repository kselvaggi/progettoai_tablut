package it.unibo.ai.didattica.competition.tablut.bitboard;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * this class represents an action of a player
 * 
 * @author Team CAMST
 * 
 */
public class BitboardAction implements Serializable, IAction {

	private static final long serialVersionUID = 1L;

	private int from;
	private int to;
	private List<RemovedPawn> removedPawns;

	private Turn turn;

	public BitboardAction(int from, int to, Turn t) {
//	if (from.length() < 0 || to.length() < 0) {
//		throw new InvalidParameterException("the FROM and the TO string must have length=2");
//	} else {
			this.from = from;
			this.to = to;
			this.turn = t;
			this.removedPawns = new ArrayList<>();
//		}
	}

	public List<RemovedPawn> getRemovedPawns() {
		return removedPawns;
	}

	public BitboardAction() throws IOException {
		this(0, 0, Turn.WHITE);
	}

	public String getFrom() {
		return BitboardUtility.positionFromBoard(from);
	}
	
	public String getTo() {
		return BitboardUtility.positionFromBoard(to);
	}
	
	public int getBitboardFrom() {
		return this.from;
	}
	
	public void setBitboardFrom(int from) {
		this.from = from;
	}

	public int getBitboardTo() {
		return to;
	}

	public void setBitboardTo(int to) {
		this.to = to;
	}

	public Turn getTurn() {
		return turn;
	}

	public void setTurn(Turn turn) {
		this.turn = turn;
	}

	public String toString() {
		return "Turn: " + this.turn + " " + "Pawn from " + BitboardUtility.positionFromBoard(from) + " to " + BitboardUtility.positionFromBoard(to) + "\n";
	}
	
	public boolean isSectorChanging() {
		return (BitboardUtility.getBoardIndex(from) != BitboardUtility.getBoardIndex(to));
	}

//	/**
//	 * @return means the index of the column where the pawn is moved from
//	 */
//	public int getColumnFrom() {
////		return Character.toLowerCase(this.from.charAt(0)) - 97;
//	}
//
//	/**
//	 * @return means the index of the column where the pawn is moved to
//	 */
//	public int getColumnTo() {
//		return Character.toLowerCase(this.to.charAt(0)) - 97;
//	}

//	/**
//	 * @return means the index of the row where the pawn is moved from
//	 */
//	public int getRowFrom() {
//		return Integer.parseInt(this.from.charAt(1) + "") - 1;
//	}
//
//	/**
//	 * @return means the index of the row where the pawn is moved to
//	 */
//	public int getRowTo() {
//		return Integer.parseInt(this.to.charAt(1) + "") - 1;
//	}

}
