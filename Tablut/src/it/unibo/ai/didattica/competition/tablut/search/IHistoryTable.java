package it.unibo.ai.didattica.competition.tablut.search;

import it.unibo.ai.didattica.competition.tablut.bitboard.IAction;

public interface IHistoryTable {
	public long getValue(IAction action);
	public void incrementValue(IAction action, long depth);
}
