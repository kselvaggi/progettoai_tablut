package it.unibo.ai.didattica.competition.tablut.bitboard;

import java.util.HashMap;
import java.util.List;

import it.unibo.ai.didattica.competition.tablut.domain.State.Pawn;
import it.unibo.ai.didattica.competition.tablut.domain.StateTablut;
import it.unibo.ai.didattica.competition.tablut.exceptions.BoardIndexException;

/**
 * Some Utility functions
 * 
 * @author Team CAMST
 * 
 */
public class BitboardUtility {

private static final HashMap<Integer, String> bitStringDictionary = new HashMap<Integer, String>() {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

{
	// rank 1	
	put(BitboardState.A1, "a1");		
	put(BitboardState.B1, "b1");
	put(BitboardState.C1, "c1");
	put(BitboardState.D1, "d1");
	put(BitboardState.E1, "e1");
	put(BitboardState.F1, "f1");
	put(BitboardState.G1, "g1");
	put(BitboardState.H1, "h1");
	put(BitboardState.I1, "i1");
	
	// rank 2	
	put(BitboardState.A2, "a2");		
	put(BitboardState.B2, "b2");
	put(BitboardState.C2, "c2");
	put(BitboardState.D2, "d2");
	put(BitboardState.E2, "e2");
	put(BitboardState.F2, "f2");
	put(BitboardState.G2, "g2");
	put(BitboardState.H2, "h2");
	put(BitboardState.I2, "i2");
	
	// rank 3	
	put(BitboardState.A3, "a3");		
	put(BitboardState.B3, "b3");
	put(BitboardState.C3, "c3");
	put(BitboardState.D3, "d3");
	put(BitboardState.E3, "e3");
	put(BitboardState.F3, "f3");
	put(BitboardState.G3, "g3");
	put(BitboardState.H3, "h3");
	put(BitboardState.I3, "i3");
	
	// rank 4	
	put(BitboardState.A4, "a4");		
	put(BitboardState.B4, "b4");
	put(BitboardState.C4, "c4");
	put(BitboardState.D4, "d4");
	put(BitboardState.E4, "e4");
	put(BitboardState.F4, "f4");
	put(BitboardState.G4, "g4");
	put(BitboardState.H4, "h4");
	put(BitboardState.I4, "i4");
	
	// rank 5	
	put(BitboardState.A5, "a5");		
	put(BitboardState.B5, "b5");
	put(BitboardState.C5, "c5");
	put(BitboardState.D5, "d5");
	put(BitboardState.E5, "e5");
	put(BitboardState.F5, "f5");
	put(BitboardState.G5, "g5");
	put(BitboardState.H5, "h5");
	put(BitboardState.I5, "i5");
	
	// rank 6	
	put(BitboardState.A6, "a6");		
	put(BitboardState.B6, "b6");
	put(BitboardState.C6, "c6");
	put(BitboardState.D6, "d6");
	put(BitboardState.E6, "e6");
	put(BitboardState.F6, "f6");
	put(BitboardState.G6, "g6");
	put(BitboardState.H6, "h6");
	put(BitboardState.I6, "i6");
	
	// rank 7	
	put(BitboardState.A7, "a7");		
	put(BitboardState.B7, "b7");
	put(BitboardState.C7, "c7");
	put(BitboardState.D7, "d7");
	put(BitboardState.E7, "e7");
	put(BitboardState.F7, "f7");
	put(BitboardState.G7, "g7");
	put(BitboardState.H7, "h7");
	put(BitboardState.I7, "i7");
	
	// rank 8	
	put(BitboardState.A8, "a8");		
	put(BitboardState.B8, "b8");
	put(BitboardState.C8, "c8");
	put(BitboardState.D8, "d8");
	put(BitboardState.E8, "e8");
	put(BitboardState.F8, "f8");
	put(BitboardState.G8, "g8");
	put(BitboardState.H8, "h8");
	put(BitboardState.I8, "i8");
	
	// rank 9	
	put(BitboardState.A9, "a9");		
	put(BitboardState.B9, "b9");
	put(BitboardState.C9, "c9");
	put(BitboardState.D9, "d9");
	put(BitboardState.E9, "e9");
	put(BitboardState.F9, "f9");
	put(BitboardState.G9, "g9");
	put(BitboardState.H9, "h9");
	put(BitboardState.I9, "i9");
}};
    
 // Utility

	public static String positionFromBoard(int position) {
		String result = bitStringDictionary.get(position);
		
		if(result == null)
			return "";
		
		return result;
	}
	
 	public static String boardToString(int[] board) {
 		String result = "";
 		for (int n=0 ; n < board.length ; n++) {
 			result = result + printSector(board[n]);
 		}
 		return result; 
 	}
 	
 	public static String boardToString(int[] boardWhite, int[] boardBlack, int[] boardKing) {
 		String result = "";
 		for (int i=0 ; i < boardWhite.length ; i++) {
 			result = result + printSector(boardWhite[i], boardBlack[i], boardKing[i]);
 		}
 		return result; 
 	}
 	
 	public static String printSector(int sector) {
 		String result = "";
 		String temp_string = "";
 		String temp = Integer.toBinaryString(0xF8000000 |sector);
 		int j=0;
 		
 		for (int i=temp.length()-1 ; i >= 5; i--) { // Start from the 5th character, it 
 			if (j == 9) {
 				StringBuilder sb = new StringBuilder(temp_string);
 				result = result + sb.reverse().toString() + "\n";
 				temp_string = "";
 				j=0;
 			}
 			temp_string = temp_string + temp.charAt(i) + " "; // Prints first the higher file (has to be reversed!)
 			j++;
 		}
 		StringBuilder sb = new StringBuilder(temp_string);
 		result = result + sb.reverse().toString()  + "\n";
 		return result;
 	}
 	
    public static void printShrinkedBoard(long b) {
        StringBuilder sb = new StringBuilder();
        long i = 0x8000000000000000L;
        while (i != 0) {
            sb.append(((b & i) != 0 ? "1 " : "0 "));
            if ((i & 0x0101010101010101L) != 0) {
                sb.append("\n");
            }
            i >>>= 1;
        }
        System.out.println("Shrinked bitboard: \n" + sb.reverse().toString());
    }
 	
 	public static String printSector(int whiteSector, int blackSector, int kingSector) {
 		String result = "";
 		String temp_string = "";
 		String tempWhite = Integer.toBinaryString(0xF8000000 | whiteSector);
 		String tempBlack = Integer.toBinaryString(0xF8000000 | blackSector);
 		String tempKing = Integer.toBinaryString(0xF8000000 | kingSector);
 		int j=0;
 		
 		for (int i=tempWhite.length()-1 ; i >= 5; i--) { // Start from the 5th character, it 
 			if (j == 9) {
 				StringBuilder sb = new StringBuilder(temp_string);
 				result = result + sb.reverse().toString() + "\n";
 				temp_string = " ";
 				j=0;
 			}
 			if (tempWhite.charAt(i) == '1') {
 				temp_string = temp_string + "W ";
 			} else if (tempBlack.charAt(i) == '1') {
 				temp_string = temp_string + "B ";
 			} else if (tempKing.charAt(i) == '1') {
 				temp_string = temp_string + "K ";
 			} else {
 				temp_string = temp_string + "0 ";
 			}
// 			temp_string = temp_string + tempWhite.charAt(i); // Prints first the higher file (has to be reversed!)
 			j++;
 		}
 		StringBuilder sb = new StringBuilder(temp_string);
 		result = result + sb.reverse().toString()  + "\n";
 		return result;
 	}

	public static int setSector(int move, int sector) {
		int temp = sector << 27;
		return (move | temp);
	}
	
	public static String printAllMoves(List<IAction> moves) throws Exception {
		int[] result = new int[BitboardState.BOARD_DIVISIONS];
		for (IAction a : moves ) {
				int move_index = getBoardIndex( ((BitboardAction) a).getBitboardTo());
				if (move_index >= BitboardState.BOARD_DIVISIONS || move_index < 0)
					throw new BoardIndexException(move_index);
				
				result[move_index] = result[move_index] | ((BitboardAction) a).getBitboardTo();
		}
		return boardToString(result);
	}
	
	public static int getBoardIndex(int move) {
		return (BitboardState.BOARD_NUMBER_MASK & move) >> 27;
	}

	public static int getFile(int position) {
		int temp = Integer.numberOfTrailingZeros(position) < 9 ? 1 : Integer.numberOfTrailingZeros(position)/9 + 1; // :)
		return 9 - ((Integer.numberOfTrailingZeros(position)) - (temp-1)*9)  ;
		
	}
	
	public static int getRank(int position, int boardIndex) {
		int current_rank_temp = Integer.numberOfTrailingZeros(position) < 9 ? 1 : Integer.numberOfTrailingZeros(position)/9 + 1; // :)
		int current_file = 9 - ((Integer.numberOfTrailingZeros(position)) - (current_rank_temp-1)*9)  ;
		//System.out.println("Move: \n" + BitboardUtility.printSector(pawn));
		return boardIndex == 0 ? current_rank_temp : current_rank_temp + (boardIndex == 1 ? 3 : 6);
	}
	
	public static long getHash(BitboardState state) {
		long result = 1;
		for (int i=0 ; i<3 ; i++) {
			for (int j=0; j<3; j++) {
				result = 31 * result + state.getBoard()[i][j];
			}
		}
		result = 31 * result + state.getTurn().ordinal();
		return result;
	}
	
	/** FUCTIONS FOR SYMMETRIES **/
	
	public static int  verticalFlipSector(int sector) {
		/* 		
		 * 		#	Prendo le 3 righe di un settore qualsiasi della board;
		 * 	
		 * 			---	Prendo la prima riga della mia board (facendo l'AND tra le 3 righe della mia board e tutti 1 nelle posizioni della prima riga);
		 * 				-- Faccio lo shift a sinistra di questa prima riga di 18 posizioni, in modo da farla andare nella terza riga!
		 * 					(N.B. Facciamo lo shift a sinistra perch� la prima riga si trova nei bit meno significativi)
		 * 			
		 * 			---	Prendo la seconda riga della mia board (facendo l'AND tra le 3 righe della mia board e tutti 1 nelle posizioni della seconda riga);
		 * 				-- Siccome la seconda riga rimarr� uguale nella simmetria verticale, non devo fare nulla!
		 * 
		 *  		---	Prendo la terza riga della mia board (facendo l'AND tra le 3 righe della mia board e tutti 1 nelle posizioni della terza riga);
		 * 				-- Faccio lo shift a destra di questa terza riga di 18 posizioni, in modo da farla andare nella prima riga!
		 *					(N.B. Facciamo lo shift a destra perch� la prima riga si trova nei bit pi� significativi)

		 *
		 * 	***************************************************************************************************
		 *	 RICORDA:	i bit 32-28 sono i pi� significativi. I bit 28 e 29 indicano il settore.
		 *				Ogni 4 bit c'� un numero esadecimale; questa 'divisione' � data dai #:
		 *
		 *				
		 *	
		 *				9		8		7		6		5		4		3		2		1
		 *			 _______________________________________________________________________			 
		 *			|	0	#	0		1		1		0	#	1		0		0		0	|
		 *			|	0		0	#	0		1		0		0	#	0		1		0	|
		 *			|	0		0		0	#	1		0		0		0	#	1		0	|
		 *			 �����������������������������������������������������������������������
		 *				27		26		25		24		23		22		21		20		19
		 *
		 *
		 *												32		31		30		29	 	28
		 *												0		0		0		0	#	0
		 *
		 *	***************************************************************************************************
		 *
		 *		9	8	7	6	5	4	3	2	1
		 *	 _______________________________________			 
		 *	|	0	0	1	1	0	1	0	0	0	|
		 *	|	0	0	0	1	0	0	0	1	0	|
		 *	|	0	0	0	1	0	0	0	1	0	|
		 *	 ���������������������������������������
		 *		27	26	25	24	23	22	21	20	19
		 *			
		 *
		 *				Flip verticale -->	
		 *
		 *
		 *		9	8	7	6	5	4	3	2	1
		 *	 _______________________________________
		 *	|	0	0	0	1	0	0	0	1	0	|	
		 *	|	0	0	0	1	0	0	0	1	0	|
		 *	|	0	0	1	1	0	1	0	0	0	|
		 *	 ���������������������������������������
		 *		27	26	25	24	23	22	21	20	19	
		 *
		 *	
		 */
		
		
		return  ((sector & 0x00001FF) << 18 ) |
				((sector & 0x003FE00)  ) |
				((sector & 0x7FC0000) >> 18 );
	}
	
	public static int mirrorHorizontalSector(int sector) { 
//		// Maschere da usare sulla board 9x9... Ma come esattamente?
//		int k1 = 0x52a954a;
//		int k2 = 0x633198c;
//		int k4 = 0x783c1e0;
		int centralFile = sector & 0x402010;
		
		// tolgo la colonna centrale (fila 5)
		int new_sector = (sector & 0x000000F) | 
				( (sector & 0x0001FE0) >> 1 ) | 
				( (sector & 0x03FC000) >> 2 ) | 
				( (sector & 0x7800000) >> 3 );
		
		// maschere per effettuare il mirror orizzontale
		int k1 = 0x555555;
		int k2 = 0x333333;
		int k4 = 0x0f0f0f;
		
		new_sector = ((new_sector >> 1) & k1) +  2*(new_sector & k1);
		new_sector = ((new_sector >> 2) & k2) +  4*(new_sector & k2);
		new_sector = ((new_sector >> 4) & k4) + 16*(new_sector & k4);
		
		// riscrivo il settore con la riga centrale
		sector = (new_sector & 0xF) | 
				( (new_sector & 0x000FF0) << 1 ) | 
				( (new_sector & 0x0FF000) << 2 ) | 
				( (new_sector & 0xF00000) << 3 ) |
				centralFile;
		
		return sector;
    }
	
	public static int[] mirrorVertical(int[] board) {
		int[] result = new int[3];
		result[0] = verticalFlipSector(board[2]);
		result[1] = 0x08000000 | verticalFlipSector(board[1]);
		result[2] = 0x10000000 | verticalFlipSector(board[0]);
		return result;
	}
	 
	public static int[] mirrorHorizontal(int[] board) { 
		int[] result = new int[3];
		result[0] = mirrorHorizontalSector(board[0]);
		result[1] = 0x08000000 | mirrorHorizontalSector(board[1]);
		result[2] = 0x10000000 | mirrorHorizontalSector(board[2]);
		return result;
    }
    
    public static int[] mirrorDiagonal (int[] board) {
    	int[] result = new int[3];
    	int centralFile0 = board[0] & 0x402010;
    	int centralFile1 = board[1] & 0x402010;
    	int centralFile2 = board[2] & 0x402010;
    	int centralRank  = board[1] & 0x03FE00;
    	
    	// creo le colonne/rige ribaltate in diagonale
    	int new_right_half 	= 
    			(centralFile0 & 0x000010) << 5 |
    			(centralFile0 & 0x002000) >> 3  |
    			(centralFile0 & 0x400000) >> 11  |
    			(centralFile1 & 0x000010) << 8 ;
    	
		int new_left_half 	= 
				(centralFile2 & 0x000010) << 11 |
    			(centralFile2 & 0x002000) << 3  |
    			(centralFile2 & 0x400000) >> 5  |
    			(centralFile1 & 0x400000) >> 8 ;
//		System.out.println("Debug: \n" + Integer.toBinaryString(new_right_half));
				
		int new_centralFile2 = 
				(centralRank & 0x20000) << 5  | 
				(centralRank & 0x10000) >> 3  |
				(centralRank & 0x08000) >> 11 ;
				
		int new_centralFile1 = 
				(centralRank & 0x02000) 	   |
				(centralRank & 0x04000) << 8  |
				(centralRank & 0x01000) >> 8  ;
				
		int new_centralFile0 = 
				(centralRank & 0x800) << 11 | 
				(centralRank & 0x400) << 3  |
				(centralRank & 0x200) >> 5  ;
    	
		// tolgo la fila centrale (fila 5)
		int new_sector0 = (board[0] & 0x000000F) | 
				( (board[0] & 0x0001FE0) >> 1 ) | 
				( (board[0] & 0x03FC000) >> 2 ) | 
				( (board[0] & 0x7800000) >> 3 );
		
		int new_sector1 = (board[1] & 0x000000F)  | 
				( (board[1] & 0x00001E0) >> 1 )  | 
				( (board[1] & 0x03C0000) >> 10 )  | 
				( (board[1] & 0x7800000) >> 11 )  ;
		
		int new_sector2 = (board[2] & 0x000000F) | 
				( (board[2] & 0x0001FE0) >> 1 ) | 
				( (board[2] & 0x03FC000) >> 2 ) | 
				( (board[2] & 0x7800000) >> 3 );
		

//		         10000000000000001001000
//		10000000010000000000000001001000
		long new_board = (new_sector0 ) |  
				(((long) new_sector1) << 24) |
				(((long) new_sector2) << 40);
		
//		printShrinkedBoard(new_board);
		
		// Effettuo il ribaltamento diagonale della board 8x8
		long t;
		long k1 = 0x5500550055005500L;
		long k2 = 0x3333000033330000L;
		long k4 = 0x0f0f0f0f00000000L;

		t  = k4 & (new_board ^ (new_board << 28));
		new_board ^=       t ^ (t >> 28) ;
		t  = k2 & (new_board ^ (new_board << 14));
		new_board ^=       t ^ (t >> 14) ;
		t  = k1 & (new_board ^ (new_board <<  7));
		new_board ^=       t ^ (t >>  7) ;
		
//		printShrinkedBoard(new_board);
		
		result[0] = (int) 
				( (new_board & 0xF) | 
				( (new_board & 0x00000000FF0) << 1 ) | 
				( (new_board & 0x000000FF000) << 2 ) | 
				( (new_board & 0x00000F00000) << 3 ) |
				new_centralFile0);
		
		result[1] = (int) ( 0x08000000 |
				( (new_board & 0x0000F000000L) >> 24 )  | 
				( (new_board & 0x000F0000000L) >> 23 ) | 
				( (new_board & 0x00F00000000L) >> 14 ) |
				( (new_board & 0x0F000000000L) >> 13 ) |
				new_centralFile1 |
				new_left_half    |
				new_right_half    );
		
		result[2] = (int) ( //0x10000000 |
				( (new_board & 0x00000F0000000000L) >> 40 ) | 
				( (new_board & 0x000FF00000000000L) >> 39 ) | 
				( (new_board & 0x0FF0000000000000L) >> 38 ) | 
				( (new_board & 0xF000000000000000L) >> 37 ) |
				new_centralFile2);
		
		result[2] = (result[2] & 0x07ffffff) | 0x10000000;
//		int temp = (int) ((new_board & 0xF000000000000000L) >> 37) ;
//		if (result[2] < 0) {
//			System.out.println("AL T\n" + Long.toBinaryString(new_board)+"\n"+Integer.toBinaryString(temp));
//			result[2] |= 0x10000000;
//		}
		return result;
    }
    
    public static int[] flipAntiDiagonal (int[] board) {
        return mirrorDiagonal (rotationClockwise180 (board) );
    }
     
	public static int[] rotationClockwise90(int[] board) {
        return mirrorVertical (mirrorDiagonal (board) );
    }
	
	public static int[] rotationClockwise180(int[] board) {
		 return mirrorHorizontal (mirrorVertical (board) );
	}

    public static int[] rotationAntiClockwise90(int[] board) {
    	return mirrorDiagonal (mirrorVertical (board) );
    }
    
    
    /***********************************************************/
  
	
	/**** UTILITY FOR HEURISTIC   ****/

	private static double[][] pawnEvalWhite = {
			{ 0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, 0.0 } ,
			{ 0.0,  2.0,  2.0, 40.0,  0.0, 40.0,  2.0,  2.0, 0.0 } ,
			{ 0.0,  2.0,  2.0, 10.0,  0.0, 10.0,  2.0,  2.0, 0.0 } ,
			{ 0.0, 40.0, 10.0,  4.0,  2.0,  4.0, 10.0, 40.0, 0.0 } ,
			{ 0.0,  0.0,  0.0,  2.0,  0.0,  2.0,  0.0,  0.0, 0.0 } ,
			{ 0.0, 40.0, 10.0,  4.0,  2.0,  4.0, 10.0, 40.0, 0.0 } ,
			{ 0.0,  2.0,  2.0, 10.0,  0.0, 10.0,  2.0,  2.0, 0.0 } ,
			{ 0.0,  2.0,  2.0, 40.0,  0.0, 40.0,  0.0,  2.0, 0.0 } ,
			{ 0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, 0.0 }
	} ;

	private static double[][] pawnEvalBlack = {
			{  0.0,  -10.0,  -10.0, -2.0,  -1.0,  -2.0, -10.0, -10.0,   0.0 } ,
			{ -10.0, -15.0, -20.0, -10.0,  -1.0, -10.0, -20.0, -15.0, -10.0 } ,
			{ -10.0, -20.0,  -10.0, -1.0,  -1.0,  -1.0, -10.0, -20.0, -10.0 } ,
			{ -2.0,  -10.0,  -1.0, -20.0, -40.0, -20.0,  -1.0, -10.0,  -2.0 } ,
			{ -1.0,  -1.0,  -1.0, -40.0,   0.0, -40.0,  -1.0,  -1.0,  -1.0 } ,
			{ -2.0,  -10.0,  -1.0, -20.0, -40.0, -20.0,  -1.0, -10.0,  -2.0 } ,
			{ -10.0, -20.0,  -10.0, -1.0,  -1.0,  -1.0, -10.0, -20.0, -10.0 } ,
			{ -10.0, -15.0, -20.0, -1.0,  -1.0, -10.0, -20.0, -15.0, -10.0 } ,
			{  0.0,  -10.0,  -10.0, -2.0,  -1.0,  -2.0, -10.0, -10.0,   0.0 }
	} ;

	private static double[][] pawnEvalKing = {
			{   0.0, 900.0, 900.0,   0.0,   0.0,   0.0, 900.0, 900.0,   0.0 } ,
			{ 900.0, 300.0, 300.0,  10.0,   0.0,  10.0, 300.0, 300.0, 900.0 } ,
			{ 900.0, 300.0, 200.0, 150.0, 200.0, 150.0, 200.0, 300.0, 900.0 } ,
			{   0.0,  10.0, 100.0,   0.0, 100.0,   0.0, 100.0,  10.0,   0.0 } ,
			{   0.0,   0.0, 200.0, 100.0,   0.0, 100.0, 200.0,   0.0,   0.0 } ,
			{   0.0,  10.0, 100.0,   0.0, 100.0,   0.0, 100.0,  10.0,   0.0 } ,
			{ 900.0, 300.0, 200.0, 150.0, 200.0, 150.0, 200.0, 300.0, 900.0 } ,
			{ 900.0, 300.0, 300.0,  10.0,   0.0,  10.0, 300.0, 300.0, 900.0 } ,
			{   0.0, 900.0, 900.0,   0.0,   0.0,   0.0, 900.0, 900.0,   0.0 }
	} ;
	
//	private int[] kingPos() {
//		int[] rowColKing = new int[2];
//
//		for (int i = 0; i < this.getBoard().length; i++) {
//			for (int j = 0; j < this.getBoard().length; j++) {
//				if (this.getPawn(i, j).equalsPawn(State.Pawn.KING.toString())) {
//					rowColKing[0] = i;
//					rowColKing[1] = j;
//				}
//			}
//		}
//		return rowColKing;
//	}

//	private double blackAroundKing(Pawn pawn) {
//		int rowKing = 0;
//		int columnKing = 0;
//		int[] rowColKing = new int[2];
//		double numBlack = 0;
//
//		rowColKing = kingPos();
//		
//		rowKing = rowColKing[0];
//		columnKing = rowColKing[1];
//
//		if (this.getBox(rowKing, columnKing).equals("e5")) { //priorit� bassa
//
//			if (this.getPawn(rowKing+1, columnKing).equalsPawn(pawn.toString()))
//				numBlack++;
//
//			if (this.getPawn(rowKing-1, columnKing).equalsPawn(pawn.toString())) 
//				numBlack++;
//
//			if (this.getPawn(rowKing, columnKing+1).equalsPawn(pawn.toString())) 
//				numBlack++;
//
//			if (this.getPawn(rowKing, columnKing-1).equalsPawn(pawn.toString())) 
//				numBlack++;
//		}
//
//		else if ( (this.getBox(rowKing, columnKing).equals("e4")) ) { //priorit� media
//
//			if (this.getPawn(rowKing-1, columnKing).equalsPawn(pawn.toString())) 
//				numBlack++;
//
//			if (this.getPawn(rowKing, columnKing+1).equalsPawn(pawn.toString())) 
//				numBlack++;
//
//			if (this.getPawn(rowKing, columnKing-1).equalsPawn(pawn.toString())) 
//				numBlack++;
//		}
//
//		else if ( (this.getBox(rowKing, columnKing).equals("e6")) ) { //priorit� media
//
//			if (this.getPawn(rowKing+1, columnKing).equalsPawn(pawn.toString())) 
//				numBlack++;
//
//			if (this.getPawn(rowKing, columnKing+1).equalsPawn(pawn.toString())) 
//				numBlack++;
//
//			if (this.getPawn(rowKing, columnKing-1).equalsPawn(pawn.toString())) 
//				numBlack++;
//		}
//
//		else if ( (this.getBox(rowKing, columnKing).equals("d5")) ) { //priorit� media
//
//			if (this.getPawn(rowKing-1, columnKing).equalsPawn(pawn.toString())) 
//				numBlack++;
//
//			if (this.getPawn(rowKing+1, columnKing).equalsPawn(pawn.toString())) 
//				numBlack++;
//
//			if (this.getPawn(rowKing, columnKing-1).equalsPawn(pawn.toString())) 
//				numBlack++;
//		}
//
//		else if ( (this.getBox(rowKing, columnKing).equals("f5")) ) { //priorit� media
//
//			if (this.getPawn(rowKing-1, columnKing).equalsPawn(pawn.toString())) 
//				numBlack++;
//
//			if (this.getPawn(rowKing+1, columnKing).equalsPawn(pawn.toString())) 
//				numBlack++;
//
//			if (this.getPawn(rowKing, columnKing+1).equalsPawn(pawn.toString())) 
//				numBlack++;
//		}
//
//		else { //PRIORITA' ALTA
//			if (this.getPawn(rowKing-1, columnKing).equalsPawn(pawn.toString())) 
//				numBlack++;
//
//			if (this.getPawn(rowKing+1, columnKing).equalsPawn(pawn.toString())) 
//				numBlack++;
//
//			if (this.getPawn(rowKing, columnKing-1).equalsPawn(pawn.toString())) 
//				numBlack++;
//
//			if (this.getPawn(rowKing, columnKing+1).equalsPawn(pawn.toString())) 
//				numBlack++;
//		}
//
//		return numBlack;
//	}
	

//	private double priorityKing(Pawn pawn) {
//		int rowKing = 0;
//		int columnKing = 0;
//		int[] rowColKing = new int[2];
//
//		rowColKing = kingPos();
//		
//		rowKing = rowColKing[0];
//		columnKing = rowColKing[1];
//
//		if (this.getBox(rowKing, columnKing).equals("e5"))  //priorit� bassa
//			return 5;
//		else if ( this.getBox(rowKing, columnKing).equals("e4") || this.getBox(rowKing, columnKing).equals("e6") ||
//				this.getBox(rowKing, columnKing).equals("d5") || this.getBox(rowKing, columnKing).equals("f5") ) //priorità media
//			return 10;
//		else //priorità alta
//			return 20;
//	}

	/*private double blackAroundCorners(Pawn pawn) {
		double numBlack = 0;

		if (this.getPawn(2, 2).equalsPawn(State.Pawn.BLACK.toString())) {
			numBlack++;
		}
		if (this.getPawn(2, 8).equalsPawn(State.Pawn.BLACK.toString())) {
			numBlack++;
		}
		if (this.getPawn(8, 2).equalsPawn(State.Pawn.BLACK.toString())) {
			numBlack++;
		}
		if (this.getPawn(8, 8).equalsPawn(State.Pawn.BLACK.toString())) {
			numBlack++;
		}		
		return numBlack;	
	}
	*/


	public static double getPieceValue(String pawn, int i, int j) {
		double value = 0;
		if (pawn.toString() == "O") 
			return 0;
		if (pawn.equals("W")) {
			value = 20 + pawnEvalWhite[i][j]; // - priorityKing(pawn) * blackAroundKing(pawn);
			//idea alla base: se il bianco vede che il re è circondato da uno/più neri ci va lui nel punto in cui vorrebbe anadare il nero e circonda lui il re!
		}
		else if (pawn.equals("B")) {
			value = -20 + pawnEvalBlack[i][j]; // - priorityKing(pawn) * blackAroundKing(pawn); //- 5 * blackAroundCorners(pawn);
		}
		else if (pawn.equals("K")) {
			value = 1000 + pawnEvalKing[i][j]; // + priorityKing(pawn) * blackAroundKing(pawn);
		}
		else
			return -1;
		return value;
	}

	public static BitboardState getBitboard(StateTablut state) {
		BitboardState result = new BitboardState();
		int board[][] = new int[3][3];
		int boardIndex = 0;
		for (int i = 0; i < 9; i++) {
			if (i != 0 && i%3 == 0)
				boardIndex++;
			for (int j = 0; j < 9 ; j++) {
				int temp = 8 - j;
				int pawn = 0;
				if (i<3)
					pawn = 1 << ((i)*9)+temp;
				else 
					pawn = 1 << ((i%3)*9)+temp;

				
				if (state.getBoard()[i][j] == Pawn.WHITE) {
					board[BitboardState.WHITE][boardIndex] |= pawn;
				} else if (state.getBoard()[i][j] == Pawn.BLACK) {
					board[BitboardState.BLACK][boardIndex] |= pawn;
				} else if (state.getBoard()[i][j] == Pawn.KING) {
					board[BitboardState.KING][boardIndex] |= pawn;
				}
			}			
		}
		
		for (int i = 0 ; i < 3 ; i++) {
			board[i][1] = 0x08000000 | board[i][1];
		}
		for (int i = 0 ; i < 3 ; i++) {
			board[i][2] = 0x10000000 | board[i][2];
		}
		
		result.setBoard(board);
		result.setTurn(Turn.valueOf(state.getTurn().name()));
		return result;
	}
 	
}
