package it.unibo.ai.didattica.competition.tablut.bitboard;

import java.util.ArrayList;

import it.unibo.ai.didattica.competition.tablut.search.ITieChecker;

/**
 * This class is used in order to detect tie situations.
 * 
 * @author Team CAMST
 * 
 */
public class BitboardTieChecker implements ITieChecker {
	private ArrayList<String> statesAlreadySeen = new ArrayList<>();
	
	@Override
	public boolean isTie(IState state) {
//		System.out.println(statesAlreadySeen.size());
//		return statesAlreadySeen.contains(((BitBoardState) state).getHash());
//		return statesAlreadySeen.contains(state);
		BitboardState bState = (BitboardState) state;
		BitboardHash bHash = BitboardHash.getHashBoard(bState);
		int index = statesAlreadySeen.indexOf(bHash.getHash());
//		if (index < 0)
//			throw new IllegalArgumentException();
		return index < statesAlreadySeen.size()-1 && index >= 0;
	}
	
	public void addState(IState state) {
		
		BitboardState bState = (BitboardState) state;
		BitboardHash bHash = BitboardHash.getHashBoard(bState);
//		if(bState.getGamePhase() == BitboardState.MIDGAME)
		statesAlreadySeen.add(bHash.getHash());
		
	}
	
	public void removeState(IState state) {
//		statesAlreadySeen.remove(((BitBoardState) state).getHash());
		BitboardState bState = (BitboardState) state;
		BitboardHash bHash = BitboardHash.getHashBoard(bState);
		int lastIndex = statesAlreadySeen.lastIndexOf(bHash.getHash());
		if (lastIndex >= 0)
			if (lastIndex == statesAlreadySeen.size()-1)
				statesAlreadySeen.remove(statesAlreadySeen.size()-1);
			else
				throw new IllegalArgumentException();
	}
	
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append("TieChecker:");
		
		result.append( " states ");
		
		for (int i=0; i<statesAlreadySeen.size()-1; i++) {
			result.append( (i+1) + ", ");
		}
		result.append(statesAlreadySeen.size() + ".");
		result.append("\n");
		
//		for (BitboardState state : statesAlreadySeen)
//			result.append(state + "\n");
//		result.append("****	tieChecker print ended	*****\n");
		
		return result.toString();
	}
	
	public BitboardTieChecker clone() {
		BitboardTieChecker result = new BitboardTieChecker();
		result.statesAlreadySeen = new ArrayList<>(statesAlreadySeen);
		return result;
	}
	
	public void swapList(ArrayList<String> newStatesAlreadySeen) {
		this.statesAlreadySeen = new ArrayList<>(newStatesAlreadySeen);
	}
}