package it.unibo.ai.didattica.competition.tablut.bitboard;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import it.unibo.ai.didattica.competition.tablut.bitboard.IState;


/**
 * This class represents the board as a bitboard
 * 
 * @author Team CAMST
 * 
 */
public class BitboardState implements IState {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Logical division: 3 integers (32bit) each one representing 1/3 of the board
	// We use 2 bits to identify the part of the board we are using
	public static final int FIRST_THIRD = 0;
	public static final int SECOND_THIRD = 1;
	public static final int THIRD_THIRD = 2;


	// Create a dictionary with those values??
	// rank 1
	public static final int I1 = 1;
	public static final int H1 = I1 << 1;
	public static final int G1 = H1 << 1;
	public static final int F1 = G1 << 1;
	public static final int E1 = F1 << 1;
	public static final int D1 = E1 << 1;
	public static final int C1 = D1 << 1;
	public static final int B1 = C1 << 1;
	public static final int A1 = B1 << 1;

	// rank 2
	public static final int I2 = A1 << 1;
	public static final int H2 = I2 << 1;
	public static final int G2 = H2 << 1;
	public static final int F2 = G2 << 1;
	public static final int E2 = F2 << 1;
	public static final int D2 = E2 << 1;
	public static final int C2 = D2 << 1;
	public static final int B2 = C2 << 1;
	public static final int A2 = B2 << 1;

	// rank 3
	public static final int I3 = A2 << 1;
	public static final int H3 = I3 << 1;
	public static final int G3 = H3 << 1;
	public static final int F3 = G3 << 1;
	public static final int E3 = F3 << 1;
	public static final int D3 = E3 << 1;
	public static final int C3 = D3 << 1;
	public static final int B3 = C3 << 1;
	public static final int A3 = B3 << 1;

	// rank 4
	public static final int I4 = 1 + 0x08000000; 	// Second part of the board
	public static final int H4 = ((0x07FFFFFF & I4) << 1) + 0x08000000;
	public static final int G4 = ((0x07FFFFFF & H4) << 1) + 0x08000000;
	public static final int F4 = ((0x07FFFFFF & G4) << 1) + 0x08000000;
	public static final int E4 = ((0x07FFFFFF & F4) << 1) + 0x08000000;
	public static final int D4 = ((0x07FFFFFF & E4) << 1) + 0x08000000;
	public static final int C4 = ((0x07FFFFFF & D4) << 1) + 0x08000000;
	public static final int B4 = ((0x07FFFFFF & C4) << 1) + 0x08000000;
	public static final int A4 = ((0x07FFFFFF & B4) << 1) + 0x08000000;

	// rank 5
	public static final int I5 = ((0x07FFFFFF & A4) << 1) + 0x08000000;
	public static final int H5 = ((0x07FFFFFF & I5) << 1) + 0x08000000;
	public static final int G5 = ((0x07FFFFFF & H5) << 1) + 0x08000000;
	public static final int F5 = ((0x07FFFFFF & G5) << 1) + 0x08000000;
	public static final int E5 = ((0x07FFFFFF & F5) << 1) + 0x08000000;
	public static final int D5 = ((0x07FFFFFF & E5) << 1) + 0x08000000;
	public static final int C5 = ((0x07FFFFFF & D5) << 1) + 0x08000000;
	public static final int B5 = ((0x07FFFFFF & C5) << 1) + 0x08000000;
	public static final int A5 = ((0x07FFFFFF & B5) << 1) + 0x08000000;

	// rank 6
	public static final int I6 = ((0x07FFFFFF & A5) << 1) + 0x08000000;
	public static final int H6 = ((0x07FFFFFF & I6) << 1) + 0x08000000;
	public static final int G6 = ((0x07FFFFFF & H6) << 1) + 0x08000000;
	public static final int F6 = ((0x07FFFFFF & G6) << 1) + 0x08000000;
	public static final int E6 = ((0x07FFFFFF & F6) << 1) + 0x08000000;
	public static final int D6 = ((0x07FFFFFF & E6) << 1) + 0x08000000;
	public static final int C6 = ((0x07FFFFFF & D6) << 1) + 0x08000000;
	public static final int B6 = ((0x07FFFFFF & C6) << 1) + 0x08000000;
	public static final int A6 = ((0x07FFFFFF & B6) << 1) + 0x08000000;

	// rank 7
	public static final int I7 = 1 + 0x10000000;		// Third part of the board
	public static final int H7 = ((0x07FFFFFF & I7) << 1) + 0x10000000;
	public static final int G7 = ((0x07FFFFFF & H7) << 1) + 0x10000000;
	public static final int F7 = ((0x07FFFFFF & G7) << 1) + 0x10000000;
	public static final int E7 = ((0x07FFFFFF & F7) << 1) + 0x10000000;
	public static final int D7 = ((0x07FFFFFF & E7) << 1) + 0x10000000;
	public static final int C7 = ((0x07FFFFFF & D7) << 1) + 0x10000000;
	public static final int B7 = ((0x07FFFFFF & C7) << 1) + 0x10000000;
	public static final int A7 = ((0x07FFFFFF & B7) << 1) + 0x10000000;

	// rank 8
	public static final int I8 = ((0x07FFFFFF & A7) << 1) + 0x10000000;	
	public static final int H8 = ((0x07FFFFFF & I8) << 1) + 0x10000000; 	
	public static final int G8 = ((0x07FFFFFF & H8) << 1) + 0x10000000;
	public static final int F8 = ((0x07FFFFFF & G8) << 1) + 0x10000000;
	public static final int E8 = ((0x07FFFFFF & F8) << 1) + 0x10000000;
	public static final int D8 = ((0x07FFFFFF & E8) << 1) + 0x10000000;
	public static final int C8 = ((0x07FFFFFF & D8) << 1) + 0x10000000;
	public static final int B8 = ((0x07FFFFFF & C8) << 1) + 0x10000000;
	public static final int A8 = ((0x07FFFFFF & B8) << 1) + 0x10000000;

	// rank 9
	public static final int I9 = ((0x07FFFFFF & A8) << 1) + 0x10000000;
	public static final int H9 = ((0x07FFFFFF & I9) << 1) + 0x10000000;
	public static final int G9 = ((0x07FFFFFF & H9) << 1) + 0x10000000;
	public static final int F9 = ((0x07FFFFFF & G9) << 1) + 0x10000000;
	public static final int E9 = ((0x07FFFFFF & F9) << 1) + 0x10000000;
	public static final int D9 = ((0x07FFFFFF & E9) << 1) + 0x10000000;
	public static final int C9 = ((0x07FFFFFF & D9) << 1) + 0x10000000;
	public static final int B9 = ((0x07FFFFFF & C9) << 1) + 0x10000000;
	public static final int A9 = ((0x07FFFFFF & B9) << 1) + 0x10000000;

	// ranks
	public static final int RANK_1 = A1 | B1 | C1 | D1 | E1 | F1 | G1 | H1 | I1;
	public static final int RANK_2 = A2 | B2 | C2 | D2 | E2 | F2 | G2 | H2 | I2;
	public static final int RANK_3 = A3 | B3 | C3 | D3 | E3 | F3 | G3 | H3 | I3;
	public static final int RANK_4 = A4 | B4 | C4 | D4 | E4 | F4 | G4 | H4 | I4;
	public static final int RANK_5 = A5 | B5 | C5 | D5 | E5 | F5 | G5 | H5 | I5;
	public static final int RANK_6 = A6 | B6 | C6 | D6 | E6 | F6 | G6 | H6 | I6;
	public static final int RANK_7 = A7 | B7 | C7 | D7 | E7 | F7 | G7 | H7 | I7;
	public static final int RANK_8 = A8 | B8 | C8 | D8 | E8 | F8 | G8 | H8 | I8;
	public static final int RANK_9 = A9 | B9 | C9 | D9 | E9 | F9 | G9 | H9 | I9;

	// files
	public static final int FILE_1 = A1 | A2 | A3 | A4 | A5 | A6 | A7 | A8 | A9;
	public static final int FILE_2 = B1 | B2 | B3 | B4 | B5 | B6 | B7 | B8 | B9;
	public static final int FILE_3 = C1 | C2 | C3 | C4 | C5 | C6 | C7 | C8 | C9;
	public static final int FILE_4 = D1 | D2 | D3 | D4 | D5 | D6 | D7 | D8 | D9;
	public static final int FILE_5 = E1 | E2 | E3 | E4 | E5 | E6 | E7 | E8 | E9;
	public static final int FILE_6 = F1 | F2 | F3 | F4 | F5 | F6 | F7 | F8 | F9;
	public static final int FILE_7 = G1 | G2 | G3 | G4 | G5 | G6 | G7 | G8 | G9;
	public static final int FILE_8 = H1 | H2 | H3 | H4 | H5 | H6 | H7 | H8 | H9;
	public static final int FILE_9 = I1 | I2 | I3 | I4 | I5 | I6 | I7 | I8 | I9;

	// camps - from the top one, clock wise
	public static final int CAMP_1 = D1 | E1 | F1 | E2;
	public static final int CAMP_2 = I4 | I5 | I6 | H5;
	public static final int CAMP_3 = D9 | E9 | F9 | E8;
	public static final int CAMP_4 = A4 | A5 | A6 | B5;

	public static final int CAMPS = CAMP_1 | CAMP_2 | CAMP_3 | CAMP_4;

	// castle
	public static final int CASTLE = E5;

	// black pawn inside the camp
	public static final int IS_INSIDE = 0x2000000; // if the second bit is set (0b0010), the black pawn didn't exit the camp yet

	// Board number mask
	public static final int BOARD_NUMBER_MASK = 0x18000000;
	public static final int BOARD_PAWNS_MASK = 0x07FFFFFF;

	public static final int PIECE_TYPES = 3; // 3 piece types: King, white pawns, black pawns
	public static final int BOARD_DIVISIONS = 3;

	public static final int WHITE = 0;
	public static final int BLACK = 1;
	public static final int KING = 2;

	//		private int[][] board_white = new int[3][]; // every 27 bit new slice of the board
	//		private int[] board_king = new int[3];
	//		private int[] board_black = new int[3];

	private int[][] board = new int[PIECE_TYPES][BOARD_DIVISIONS];
	private int[] camps= new int[BOARD_DIVISIONS];
//	private List<Integer> winningPoints = new ArrayList<>();

	// MASKS FOR CAPTURING
	private int[] camps_mask_for_capturing= new int[BOARD_DIVISIONS];
	private static final int CAPTURED_KING_MASK = E6 | F5 | D5 | E4 | CASTLE;

	protected Turn turn;

	public BitboardState() {

		this.turn = Turn.WHITE;

		// initialization
		//white pieces
		board[WHITE][0] = E3; 
		board[WHITE][1] = E4 | E6 | C5 | D5 | F5 | G5;
		board[WHITE][2] = E7;

		// black pieces
		board[BLACK][0] = CAMP_1;
		board[BLACK][1] = CAMP_2 | CAMP_4;
		board[BLACK][2] = CAMP_3;

		board[KING][0] = 0;
		board[KING][1] = E5;
		board[KING][2] = 0x10000000;

		// camps
		camps[0] = CAMP_1;
		camps[1] = CAMP_2 | CAMP_4;
		camps[2] = CAMP_3;

		// camp mask for capturing (we dont consider the internal one)
		camps_mask_for_capturing[0] = D1 | F1 | E2;
		camps_mask_for_capturing[1] = I4 | I6 | H5 | A4 | A6 | B5;
		camps_mask_for_capturing[2] = D9 | F9 | E8;

		// winning points
//		winningPoints.add(A2);
//		winningPoints.add(A3);
//		winningPoints.add(A7);
//		winningPoints.add(A8);
//		winningPoints.add(B1);
//		winningPoints.add(B9);
//		winningPoints.add(C1);
//		winningPoints.add(C9);
//		winningPoints.add(G1);
//		winningPoints.add(G9);
//		winningPoints.add(H1);
//		winningPoints.add(H9);
//		winningPoints.add(I2);
//		winningPoints.add(I3);
//		winningPoints.add(I7);
//		winningPoints.add(I8);

	}

	public BitboardState clone() {
		BitboardState result = new BitboardState();

		for (int i = 0 ; i< BOARD_DIVISIONS; i++) {
			for (int j = 0; j < PIECE_TYPES; j++) {
				result.board[i][j] = this.board[i][j];
			}
		}
		//			result.board = this.board.clone(); 
		result.turn = this.turn;

		return result;
	}	


	/**
	 * Gets the current turn
	 * @return current turn 
	 */
	public Turn getTurn() {
		return turn;
	}

	/**
	 * Sets the current turn
	 * @param turn
	 */
	public void setTurn(Turn turn) {
		this.turn = turn;
	}

	@Override
	public void move(IAction action) {
		move( ((BitboardAction) action).getBitboardFrom(), ((BitboardAction) action).getBitboardTo(), action);
	}

	// Move format 000x x | 0b??? 0x??? 
	@Override
	public void move(int from, int to) {
		// check which part of the board we are
		int index_from = from >> 27;
			int index_to = to >> 27;
		if (this.board[KING][index_from] == from) {
			this.board[KING][index_from] ^= (BOARD_PAWNS_MASK & from);
			this.board[KING][index_to] |= to;
			
			this.checkPawnCaptured(to);
	
			turn = Turn.BLACK;
		} else if (turn == Turn.WHITE) { // pawn move
			this.board[WHITE][index_from] ^= (BOARD_PAWNS_MASK & from);
			this.board[WHITE][index_to] |= to;
			this.checkPawnCaptured(to);
			BitboardUtility.getBoardIndex(this.getBoard()[0][1]);
			turn = Turn.BLACK;
		} else {
			this.board[BLACK][index_from] ^= (BOARD_PAWNS_MASK & from);
			this.board[BLACK][index_to] |= to;
			this.checkPawnCaptured(to);
			turn = Turn.WHITE;
		}
	}
	
	public void move(int from, int to, IAction action) {
		
		// check which part of the board we are
		int index_from = from >> 27;
		int index_to = to >> 27;
	
		if (this.board[KING][index_from] == from) {
			this.board[KING][index_from] ^= from;
			this.board[KING][index_to] |= to;
			((BitboardAction) action).getRemovedPawns().addAll(this.checkPawnCaptured(to));
			
			turn = Turn.BLACK;
		} else if (turn == Turn.WHITE) { // pawn move
			this.board[WHITE][index_from] ^= from;
			this.board[WHITE][index_to] |= to;
//			this.checkPawnCaptured(to);
			((BitboardAction) action).getRemovedPawns().addAll(this.checkPawnCaptured(to));
			
			turn = Turn.BLACK;
		} else {
			this.board[BLACK][index_from] ^= from;
			this.board[BLACK][index_to] |= to;
//			this.checkPawnCaptured(to);
			((BitboardAction) action).getRemovedPawns().addAll(this.checkPawnCaptured(to));
			
			turn = Turn.WHITE;
		}
	}

	private List<RemovedPawn> checkPawnCaptured(int pawnPosition) {
//		BitboardPosition opponentPawnMock;
//		BitboardPosition minacingEntity;
		int opponentColor = this.turn == Turn.WHITE ? Turn.BLACK.ordinal() : Turn.WHITE.ordinal();
//		int current_file = BitboardUtility.getFile(pawnPosition);
		int boardIndex = BitboardUtility.getBoardIndex(pawnPosition);
		List<RemovedPawn> capturedPawns = new ArrayList<>();
		RemovedPawn temp;
		
		// CHECK CAPTURED OPPONENT PAWN ON THE LEFT SQUARE
		// check if my pawn is in the left edge
		if (!contentIsContained(pawnPosition, FILE_1 | FILE_2)) {
			temp = tryCapturePawn(pawnPosition, boardIndex, opponentColor, "left", !contentIsContained( pawnPosition, FILE_1));
			if (temp.getRemovedPawn() >= 0) 
				capturedPawns.add(temp);
		}
		
		// CHECK CAPTURED PAWN ON THE RIGHT SQUARE
		// check if my pawn is in the right edge
		if (!contentIsContained( pawnPosition, FILE_8 | FILE_9)) {
			temp = tryCapturePawn(pawnPosition, boardIndex, opponentColor, "right", !contentIsContained( pawnPosition, FILE_9));
			if (temp.getRemovedPawn() >= 0) 
				capturedPawns.add(temp);

		}
		
		// CHECK CAPTURED PAWN ON THE SQUARE ABOVE
		// check if current pawn is in the upper edge
		if (!contentIsContained(pawnPosition, RANK_1 | RANK_2)) {
			temp = tryCapturePawn(pawnPosition, boardIndex, opponentColor, "up", !contentIsContained(pawnPosition, RANK_1 | RANK_2));
			if (temp.getRemovedPawn() >= 0) 
				capturedPawns.add(temp);

		}
		
		// CHECK CAPTURED PAWN ON THE SQUARE BELOW
		// check if my pawn is in the bottom edge
		if (!contentIsContained2(pawnPosition, RANK_8 | RANK_9)) {
			temp = tryCapturePawn(pawnPosition, boardIndex, opponentColor, "down", !contentIsContained2(pawnPosition, RANK_8 | RANK_9));
			if (temp.getRemovedPawn() >= 0) 
				capturedPawns.add(temp);

		}
		return capturedPawns;
	}

	private RemovedPawn tryCapturePawn(int pawnPosition, int boardIndex, int opponentColor, String direction, boolean excludingCondition) {
		BitboardPosition opponentPawnMock = moveToDirection((BOARD_PAWNS_MASK & pawnPosition), boardIndex, direction);
		BitboardPosition minacingEntity;
		RemovedPawn capturedPawn = new RemovedPawn();
		int opponentBoardSector = opponentPawnMock.getBoardSector();
		
		// check if there is a opponent pawn not [in the edge]
		if ( excludingCondition 		// Ridonante?
				&& contentIsContained(opponentPawnMock.getPosition(), board[opponentColor][opponentBoardSector])) {
	
			minacingEntity = moveToDirection(opponentPawnMock.getPosition(), opponentBoardSector, direction);
			//check se a c'� un accampamento, un castello o un'altra mia pedina
			if (isPawnCaptured(minacingEntity.getPosition(), minacingEntity.getBoardSector(), turn.ordinal()) ) {
				board[opponentColor][opponentBoardSector] ^= opponentPawnMock.getPosition();
				capturedPawn.setRemovedPawn(opponentPawnMock.getPosition());
				capturedPawn.setRemovedPawn(BitboardUtility.setSector(capturedPawn.getRemovedPawn(), opponentPawnMock.getBoardSector()));
				return capturedPawn;
			}
		} else if ( opponentColor == WHITE 
				&& excludingCondition
				&& contentIsContained(opponentPawnMock.getPosition(), board[KING][opponentBoardSector]) ) {
			
				if (// !contentIsContained(opponentPawnMock.getPosition(), FILE_1) // la prima condizione � inutile nel caso del re?
						opponentBoardSector == SECOND_THIRD
						&& contentIsContained(board[KING][opponentBoardSector], CAPTURED_KING_MASK)) {
					if (isKingCaptured(opponentPawnMock, opponentBoardSector)) { // CONTROLLARE
						capturedPawn.setRemovedPawn(board[KING][opponentBoardSector]);
						capturedPawn.setKing(true);
						board[KING][opponentBoardSector] ^= opponentPawnMock.getPosition();
						return capturedPawn;
					}
				} else {
					minacingEntity = moveToDirection(opponentPawnMock.getPosition(), opponentBoardSector, direction);
					if (isPawnCaptured(minacingEntity.getPosition(), minacingEntity.getBoardSector(), turn.ordinal()) ) {
						capturedPawn.setRemovedPawn(board[KING][opponentBoardSector]);
						capturedPawn.setKing(true);
						board[KING][opponentBoardSector] ^= opponentPawnMock.getPosition();
						return capturedPawn;
					}
				}
		}
		return capturedPawn;
	}

	private boolean isPawnCaptured(int position, int boardIndex, int color) {
		return ((boardIndex == SECOND_THIRD && contentIsContained(position, CASTLE) ) 
				|| contentIsContained(position, board[color][boardIndex]) 
				|| contentIsContained(position, camps_mask_for_capturing[boardIndex]));
	}

	private boolean isKingMinaced(int position, int boardIndex, int color) {
		return ( (boardIndex == SECOND_THIRD && contentIsContained(position, CASTLE) ) 
				|| contentIsContained(position, board[color][boardIndex]) );
	}

	// USE THOSE?
	//		private boolean isCapturedByCastle(int position, int boardIndex, int color) {
	//			return boardIndex == SECOND_THIRD && contentIsContained(position, CASTLE);
	//		}
	//		
	//		private boolean isCapturedByCamp(int position, int boardIndex, int color) {
	//			return contentIsContained(position, camps_mask_for_capturing[boardIndex]);
	//		}
	//		
	//		private boolean isCapturedByPawn(int position, int boardIndex, int color) {
	//			return contentIsContained(position, board[color][boardIndex]) ;
	//		}

	//		private boolean checkKingCaptured(int pawnPosition) {
	//			BitboardPosition opponentPawnMock;
	//			BitboardPosition minacingEntity;
	//			int opponentColor = this.turn == Turn.WHITE ? Turn.BLACK.ordinal() : Turn.WHITE.ordinal();
	//			int current_file = BitboardUtility.getFile(pawnPosition);
	//			int boardIndex = BitboardUtility.getBoardIndex(pawnPosition);
	//			
	//			// 
	//			if (contentIsContained(pawnPosition, FILE_5 | FILE_6 | FILE_4 | RANK_4 | RANK_6 | RANK_5)) { // da correggere questo check
	//				opponentPawnMock = moveLeft((BOARD_PAWNS_MASK & pawnPosition), boardIndex); //
	////				if (// !contentIsContained(opponentPawnMock.getPosition(), FILE_1) // la prima condizione � inutile nel caso del re?
	////						contentIsContained(opponentPawnMock.getPosition(), board[KING][boardIndex])
	////						&& contentIsContained(board[KING][boardIndex], CAPTURED_KING_MASK)) {
	////					//check if king is surrounded by black pawns/throne
	////					BitboardPosition minacingEntity1 = moveLeft(opponentPawnMock.getPosition(), boardIndex);
	////					BitboardPosition minacingEntity2 = moveUp(opponentPawnMock.getPosition(), boardIndex);
	////					BitboardPosition minacingEntity3 = moveDown(opponentPawnMock.getPosition(), boardIndex);
	////					BitboardPosition minacingEntity4 = moveRight(opponentPawnMock.getPosition(), boardIndex);
	////					//check se c'� un castello o un'altra mia pedina nera su tutti i lati
	////					if (isKingMinaced(minacingEntity1.getPosition(), boardIndex, turn.ordinal()) 
	////							&& isKingMinaced(minacingEntity2.getPosition(), minacingEntity2.getBoardSector(), turn.ordinal()) 
	////							&& isKingMinaced(minacingEntity3.getPosition(), minacingEntity3.getBoardSector(), turn.ordinal()) 
	////							&& isKingMinaced(minacingEntity4.getPosition(), minacingEntity4.getBoardSector(), turn.ordinal()) ) {
	////						//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");Turn.values()[opponentColor].toString() + " PIECE EATEN");
	////						board[KING][boardIndex] = board[KING][boardIndex]- opponentPawnMock.getPosition();
	////						return true;
	////					}
	////				}	
	//				if (isKingCaptured(opponentPawnMock, boardIndex)) {
	//					//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");Turn.values()[opponentColor].toString() + " PIECE EATEN");
	////					board[KING][boardIndex] = board[KING][boardIndex]- opponentPawnMock.getPosition();
	//					board[KING][boardIndex] ^= opponentPawnMock.getPosition();
	//					return true;
	//				}
	//				
	//				opponentPawnMock = moveRight((BOARD_PAWNS_MASK & pawnPosition), boardIndex); //
	//				if ( contentIsContained(opponentPawnMock.getPosition(), board[KING][boardIndex])
	//						&& contentIsContained(board[KING][boardIndex], CAPTURED_KING_MASK)) {
	//					//check if king is surrounded by black pawns/throne
	//					BitboardPosition minacingEntity1 = moveRight(opponentPawnMock.getPosition(), boardIndex);
	//					BitboardPosition minacingEntity2 = moveUp(opponentPawnMock.getPosition(), boardIndex);
	//					BitboardPosition minacingEntity3 = moveDown(opponentPawnMock.getPosition(), boardIndex);
	//					//check se c'� un castello o un'altra mia pedina nera su tutti i lati
	//					if (isKingMinaced(minacingEntity1.getPosition(), boardIndex, turn.ordinal()) 
	//							&& isKingMinaced(minacingEntity2.getPosition(), minacingEntity2.getBoardSector(), turn.ordinal()) 
	//							&& isKingMinaced(minacingEntity3.getPosition(), minacingEntity3.getBoardSector(), turn.ordinal())  ) {
	//						//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");Turn.values()[opponentColor].toString() + " PIECE EATEN");
	////						board[KING][boardIndex] = board[KING][boardIndex]- opponentPawnMock.getPosition();
	//						board[KING][boardIndex] ^= opponentPawnMock.getPosition();
	//						
	//						return true;
	//					}
	//				}	
	//				opponentPawnMock = moveUp((BOARD_PAWNS_MASK & pawnPosition), boardIndex); //
	//				if ( contentIsContained(opponentPawnMock.getPosition(), board[KING][boardIndex])
	//						&& contentIsContained(board[KING][boardIndex], CAPTURED_KING_MASK)) {
	//					//check if king is surrounded by black pawns/throne
	//					BitboardPosition minacingEntity1 = moveLeft(opponentPawnMock.getPosition(), boardIndex);
	//					BitboardPosition minacingEntity2 = moveUp(opponentPawnMock.getPosition(), boardIndex);
	//					BitboardPosition minacingEntity3 = moveRight(opponentPawnMock.getPosition(), boardIndex);
	//					//check se c'� un castello o un'altra mia pedina nera su tutti i lati
	//					if (isKingMinaced(minacingEntity1.getPosition(), boardIndex, turn.ordinal()) 
	//							&& isKingMinaced(minacingEntity2.getPosition(), minacingEntity2.getBoardSector(), turn.ordinal()) 
	//							&& isKingMinaced(minacingEntity3.getPosition(), boardIndex, turn.ordinal())  ) {
	//						//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");Turn.values()[opponentColor].toString() + " PIECE EATEN");
	////						board[KING][boardIndex] = board[KING][boardIndex]- opponentPawnMock.getPosition();
	//						board[KING][boardIndex] ^= opponentPawnMock.getPosition();
	//						return true;
	//					}
	//				}	
	//				opponentPawnMock = moveDown((BOARD_PAWNS_MASK & pawnPosition), boardIndex); //
	//				if ( contentIsContained(opponentPawnMock.getPosition(), board[KING][boardIndex]) // Manca una condizione? (Check se la mossa � sul bordo?, sembrerebbe inutile)
	//						&& contentIsContained(board[KING][boardIndex], CAPTURED_KING_MASK)) {
	//					//check if king is surrounded by black pawns/throne
	//					BitboardPosition minacingEntity1 = moveLeft(opponentPawnMock.getPosition(), boardIndex);
	//					BitboardPosition minacingEntity2 = moveDown(opponentPawnMock.getPosition(), boardIndex);
	//					BitboardPosition minacingEntity3 = moveRight(opponentPawnMock.getPosition(), boardIndex);
	//					//check se c'� un castello o un'altra mia pedina nera su tutti i lati
	//					if (isKingMinaced(minacingEntity1.getPosition(), boardIndex, turn.ordinal()) 
	//							&& isKingMinaced(minacingEntity2.getPosition(), minacingEntity2.getBoardSector(), turn.ordinal()) 
	//							&& isKingMinaced(minacingEntity3.getPosition(), boardIndex, turn.ordinal())  ) {
	//						//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");Turn.values()[opponentColor].toString() + " PIECE EATEN");
	////						board[KING][boardIndex] = board[KING][boardIndex]- opponentPawnMock.getPosition();
	//						board[KING][boardIndex] ^= opponentPawnMock.getPosition();
	//						return true;
	//					}
	//				}	
	//			}
	//			return false;
	//		}


	private boolean isKingCaptured(BitboardPosition king, int boardIndex) {
		//check if king is surrounded by black pawns/throne
		BitboardPosition minacingEntity1 = moveLeft(king.getPosition(), boardIndex);
		BitboardPosition minacingEntity2 = moveUp(king.getPosition(), boardIndex);
		BitboardPosition minacingEntity3 = moveDown(king.getPosition(), boardIndex);
		BitboardPosition minacingEntity4 = moveRight(king.getPosition(), boardIndex);
		//check se c'� un castello o un'altra mia pedina nera su tutti i lati
		return (isKingMinaced(minacingEntity1.getPosition(), boardIndex, turn.ordinal()) 
				&& isKingMinaced(minacingEntity2.getPosition(), minacingEntity2.getBoardSector(), turn.ordinal()) 
				&& isKingMinaced(minacingEntity3.getPosition(), minacingEntity3.getBoardSector(), turn.ordinal()) 
				&& isKingMinaced(minacingEntity4.getPosition(), minacingEntity4.getBoardSector(), turn.ordinal()) );

	}

	private BitboardPosition moveToDirection(int pawnToMove, int boardIndex, String direction) {
		BitboardPosition result = null;
		switch (direction) {
		case "left":
			result = moveLeft(pawnToMove, boardIndex);
			break;
		case "right":
			result = moveRight(pawnToMove, boardIndex);
			break;
		case "down":
			result = moveDown(pawnToMove, boardIndex);
			break;
		case "up":
			result = moveUp(pawnToMove, boardIndex);
			break;
		}
		return result;
	}

	private BitboardPosition moveRight(int pawnToMove, int boardIndex) {
		return new BitboardPosition((pawnToMove >> 1), boardIndex);
	}


	private BitboardPosition moveLeft(int pawnToMove, int boardIndex) {
		return new BitboardPosition( ( pawnToMove << 1), boardIndex);
	}


	/**
	 * 
	 * @param pawn
	 * @return
	 */
	private BitboardPosition moveDown(int pawnToMove, int currentBoardIndex) {
		int move = 0;
		int current_file = BitboardUtility.getFile(pawnToMove);
		if (Integer.numberOfTrailingZeros(pawnToMove) > 18) {
			currentBoardIndex++;
			move = 0x08000000 >> (current_file + 18);
		}  else {
			move = pawnToMove << 9;
		}
		return new BitboardPosition(move, currentBoardIndex);
	}

	/**
	 * 
	 * @param pawn
	 * @return
	 */
	private BitboardPosition moveUp(int pawnToMove, int currentBoardIndex) {
		int move = 0;
		int current_file = BitboardUtility.getFile(pawnToMove);
		if (Integer.numberOfTrailingZeros(pawnToMove) < 9) {
			currentBoardIndex--;
			move = 0x08000000 >> current_file;
		}  else {
			move = pawnToMove >> 9;
		}
		return new BitboardPosition(move, currentBoardIndex);
	}


	private static boolean contentIsContained(int content, int container) {
		//			System.out.println("DEBUG: " + BitboardUtility.printSector(content) + " =? " + BitboardUtility.printSector((content | container)));
		//			//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"DEBUG: " + Integer.toHexString(container) + " =? " + Integer.toHexString((content | container)));
		//			if (BitboardUtility.getBoardIndex(content) != BitboardUtility.getBoardIndex(container))
		//				return false;
		if ( (BOARD_PAWNS_MASK & content) == 0)
			return false;
		return (content | container) == container;
	}

	private static boolean contentIsContained2(int content, int container) {
		//			System.out.println("DEBUG: " + BitboardUtility.printSector(content) + " =? " + BitboardUtility.printSector((content | container)));
		if (BitboardUtility.getBoardIndex(content) != BitboardUtility.getBoardIndex(container))
			return false;
		return (content | container) == container;
	}

//		private boolean checkMove(BitboardState bitboardState, int from, int to) {
//			// TODO
//			
//			return true;
//		}


	public List<IAction> getPawnMoves(int pawn) throws IOException {
		if (isWinningState())
			return new ArrayList<>();
		List<IAction> possibleMoves = new ArrayList<>();
		int board_index = BitboardUtility.getBoardIndex(pawn); // as argument?
		int current_rank_temp = Integer.numberOfTrailingZeros(pawn) < 9 ? 1 : Integer.numberOfTrailingZeros(pawn)/9 + 1; // :)
		int current_file = 9 - ((Integer.numberOfTrailingZeros(pawn)) - (current_rank_temp-1)*9)  ;
		int opponentColor = this.turn == Turn.WHITE ? Turn.BLACK.ordinal() : Turn.WHITE.ordinal();
		int currentColor = this.turn.ordinal();
		boolean isInsideCamp = (currentColor == Turn.BLACK.ordinal() && contentIsContained(pawn, camps[board_index]));
//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"Pawn: \n" + BitboardUtility.printSector(pawn));
//			int current_rank = board_index == 0 ? current_rank_temp : current_rank_temp + (board_index == 1 ? 3 : 6);
		boolean canMove = true;
//			if (BitboardUtility.positionFromBoard(pawn).equalsIgnoreCase("g4")) {
//				System.out.println("Well�!");
//			}
		int move = 0;
		// check up
		if ((RANK_1 | pawn) != RANK_1) {
			move = (BOARD_PAWNS_MASK & pawn);
			int temp_index = board_index;

			while (canMove) {
//				//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"Move:" + Integer.toHexString(BOARD_PAWNS_MASK) + "\n" + BitboardUtility.printSector(move));
				move = move >> 9;
		if (temp_index > 0 && move == 0) {
			temp_index--;
			move = 0x08000000 >> current_file;
	//						System.out.println("Move: \n" + BitboardUtility.printSector(move));
		} 
		// DEBUG
//		System.out.println("Move: \n" + BitboardUtility.printSector(move));
//
		int control;
		if (isInsideCamp) {
			control = this.getBoard()[currentColor][temp_index] | this.getBoard()[opponentColor][temp_index] | this.getBoard()[KING][temp_index] | CASTLE;
		} else {
			control = this.camps[temp_index] | this.getBoard()[currentColor][temp_index] | this.getBoard()[opponentColor][temp_index] | this.getBoard()[KING][temp_index] | CASTLE;
		}
		// DEBUG
//					System.out.println("Control: \n" + BitboardUtility.printSector(control));
//
		if (control != (control | move)) {
			int moveToAdd = BitboardUtility.setSector(move, temp_index);
			possibleMoves.add(new BitboardAction(pawn, moveToAdd, turn)); //result[temp_index] = result[temp_index] | move;
		} else {
			canMove = false;
		}

			}
		}
		// check down
		canMove=true;
		if (!contentIsContained2(pawn, RANK_9)) {
			move = (0x07FFFFFF & pawn);
			int temp_index = board_index;
			while (canMove) {
//					move = moveDown(move, board_index);
//					move = move << 9;
//					if (temp_index < (BOARD_DIVISIONS-1) && move == 0) {
//						temp_index++;
//						move = 0x08000000 >> (current_file + 18);
//						//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"Move: \n" + BitboardUtility.printSector(move));
//					} 
//					System.out.println("Zeros: " + Integer.numberOfTrailingZeros(move));
				if (Integer.numberOfTrailingZeros(move) > 18) {
					temp_index++;
					move = 0x08000000 >> (current_file + 18);
//					//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"Move: \n" + BitboardUtility.printSector(minacingEntity));
				}  else {
					move = move << 9;
				}
				if (temp_index >= BOARD_DIVISIONS)
					break;
//					// DEBUG
//					System.out.println("Move: \n" + BitboardUtility.printSector(move) + " " + Integer.numberOfTrailingZeros(move));
//					//
				int control;
				if (isInsideCamp) {
					control = this.getBoard()[currentColor][temp_index] | this.getBoard()[opponentColor][temp_index] | this.getBoard()[KING][temp_index] | CASTLE;
				} else {
					control = this.camps[temp_index] | this.getBoard()[currentColor][temp_index] | this.getBoard()[opponentColor][temp_index] | this.getBoard()[KING][temp_index] | CASTLE;
				}
				// DEBUG
//					//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"Control: \n" + BitboardUtility.printSector(control));
//					//
				if (control != (control | move)) {
					int moveToAdd = BitboardUtility.setSector(move, temp_index);
					possibleMoves.add(new BitboardAction(pawn, moveToAdd, turn));
				} else {
					canMove = false;
				}

			}
		}
		// check right
		// if he isn't in FILE i
		canMove=true;
		if ((FILE_9 | pawn) != FILE_9) {
			move = (BOARD_PAWNS_MASK & pawn);
			while (canMove) {
				move = move >> 1;
					// DEBUG
					//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"Move: \n" + BitboardUtility.printSector(move));
					//
					int control;
					if (isInsideCamp) {
						control = this.getBoard()[currentColor][board_index] | this.getBoard()[opponentColor][board_index] | this.getBoard()[KING][board_index] | CASTLE | FILE_1;
					} else {
						control = this.camps[board_index] | this.getBoard()[currentColor][board_index] | this.getBoard()[opponentColor][board_index] | this.getBoard()[KING][board_index] | CASTLE | FILE_1;
					}
					// DEBUG
					//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"Control: \n" + BitboardUtility.printSector(control));
					//
					if (control != (control | move)) {
						int moveToAdd = BitboardUtility.setSector(move, board_index);
						possibleMoves.add(new BitboardAction(pawn, moveToAdd, turn));
						//							//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"Temp result: \n" + BitboardUtility.printSector(result[board_index]));
					} else {
						canMove = false;
					}
			}
		}
		// check left
		canMove=true;
		if ((FILE_1 | pawn) != FILE_1) {
			move = (BOARD_PAWNS_MASK & pawn);
			while (canMove) {
				move = move << 1;
				// DEBUG
				//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"Move: \n" + BitboardUtility.printSector(move));
				//
				int control;
				if (isInsideCamp) {
					control = this.getBoard()[currentColor][board_index] | this.getBoard()[opponentColor][board_index] | this.getBoard()[KING][board_index] | CASTLE | FILE_9;
				} else {
					control = this.camps[board_index] | this.getBoard()[currentColor][board_index] | this.getBoard()[opponentColor][board_index] | this.getBoard()[KING][board_index] | CASTLE | FILE_9;
				}
				// DEBUG
				//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"Control: \n" + BitboardUtility.printSector(control));
				//
				if (control != (control | move)) {
					int moveToAdd = BitboardUtility.setSector(move, board_index);
					possibleMoves.add(new BitboardAction(pawn, moveToAdd, turn));
					//							//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"Temp result: \n" + BitboardUtility.printSector(result[board_index]));
				} else {
					canMove = false;
				}
			}
		}

		return possibleMoves;
	}

	public static int scanMove(int moves) {
		return 1 << Integer.numberOfTrailingZeros(moves);
	}


	public int[][] getBoard() {
		return board;
	}


	public void setBoard(int[][] board) {
		this.board = board;
	}

	@Override
	public void unmove(IAction action) {
		int opponentColor = action.getTurn() == Turn.WHITE ? 1 : 0;
		int boardIndex;
		this.setTurn(action.getTurn());
		int from = ((BitboardAction) action).getBitboardTo();
		int to = ((BitboardAction) action).getBitboardFrom();
		int index_from = from >> 27;
		int index_to = to >> 27;
	
		if (this.board[KING][index_from] == from) {
			this.board[KING][index_from] ^= (BOARD_PAWNS_MASK & from);
			this.board[KING][index_to] |= to;
		} else if (turn == Turn.WHITE) { // pawn move
			this.board[WHITE][index_from] ^= (BOARD_PAWNS_MASK & from);
			this.board[WHITE][index_to] |= to;
		} else {
			this.board[BLACK][index_from] ^= (BOARD_PAWNS_MASK & from);
			this.board[BLACK][index_to] |= to;
		}
		
		for (RemovedPawn pawn : ((BitboardAction) action).getRemovedPawns()) {
			boardIndex = BitboardUtility.getBoardIndex(pawn.getRemovedPawn());
			if (pawn.isKing()) {
				this.board[KING][boardIndex] |= pawn.getRemovedPawn();
			} else {
				this.board[opponentColor][boardIndex] |= pawn.getRemovedPawn();
			}
		}
//		((BitboardAction) action).getRemovedPawns().clear();
	}


	/* 			OLD			*/
//	@Override
//	public int getHeuristicOldValue() {
//		double value = 0;
//		for (int i=0; i < BOARD_DIVISIONS; i++) {
//			int stateWhite = BOARD_PAWNS_MASK & board[WHITE][i];
//			int stateBlack = BOARD_PAWNS_MASK & board[BLACK][i];
//			int stateKing = BOARD_PAWNS_MASK & board[KING][i];
//
//			while (stateWhite != 0) {
//				int pawn = BitboardState.scanMove(stateWhite);
//				int rank = BitboardUtility.getRank(pawn, i)-1;
//				int file = BitboardUtility.getFile(pawn)-1;
//				value = value + BitboardUtility.getPieceValue("W", rank, file);
//				stateWhite ^= pawn;
//			}
//			while (stateBlack != 0) {
//				int pawn = BitboardState.scanMove(stateBlack);
//				int rank = BitboardUtility.getRank(pawn, i)-1;
//				int file = BitboardUtility.getFile(pawn)-1;
//				value = value + BitboardUtility.getPieceValue("B", rank, file);
//				stateBlack ^= pawn;
//			}
//			while (stateKing != 0) {
//				int pawn = BitboardState.scanMove(stateKing);
//				int rank = BitboardUtility.getRank(pawn, i)-1;
//				int file = BitboardUtility.getFile(pawn)-1;
//				value = value + BitboardUtility.getPieceValue("K", rank, file);
//				stateKing ^= pawn;
//			}
//			//					if (value >= 5000)
//			//						return value;
//		}	
//		return (int) value;
//	}


	/*			NEW			*/

	@Override
	public int getHeuristicValue() {
		int value = 0;
		int[] numPieces = new int[2];
		
		numPieces = getNumPieces();
		value = numPieces[0] - numPieces[1] - blackAroundKingValue();// + kingWinningPointDistancesScore(this, 0);// - kingMovesToWinningPointsValue();// - kingWinningPointDistancesScore(this, 0) ;
		
		return value;
	}
	
	
	private int kingWinningPointDistancesScore (IState state, int moveTerminalCondition) {
		if ( state.hasWhiteWon() )
			return moveTerminalCondition;
		else if (moveTerminalCondition == 2) // Dopo aver fatto due mosse non vincenti, non mi interessano le altre
			return 3; // con 3 non entra nel case della funzione chiamante. 
		
		int value=0;
		
		BitboardState tempState = (BitboardState) state.clone();
		tempState.setTurn(Turn.WHITE);
		List<IAction> availableKingMoves = state.getKingMoves();
		
		// in [0] registro il numero di winning point a distanza 1
		// in [1] registro il numero di winning point a distanza 2
		int [] moveCounts = new int[2];
		
		int moveIndex = 0;

		try {
			for (IAction a : availableKingMoves) {
				tempState = (BitboardState) state.clone();
//				tempState.setTurn(Turn.WHITE);
				tempState.move(a);

				//qua ci sarebbe un if... Se questa mossa portasse il re a una distanza dalla vittoria minore di quella di partenza:
				moveIndex = kingWinningPointDistancesScore(tempState, moveTerminalCondition+1);
				if (moveIndex < 3)
					moveCounts[moveIndex-1]++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (moveCounts[0] == 0 && moveCounts[1] == 0)
			return 3;
		
		value = moveCounts[0]*15 + moveCounts[1]*10;
		
		return value;
	}
	

//	private int kingMovesToWinningPointsValue() {
//		int value = 0;
//		int kingPosition = 0;
//
////		long elapsedTime = System.currentTimeMillis();		
//		List<Integer> winningPoints = getWinningPoints();
//
//		for (int i=0; i < BOARD_DIVISIONS; i++) {
//			if (Integer.numberOfTrailingZeros(board[KING][i]) < 27) { //trovo il settore in cui si trova il re
//				kingPosition = board[KING][i];
//			}
//		}
//
////		long elapsedTime = System.currentTimeMillis();
//		BitboardState tempState = (BitboardState) this.clone();
//		tempState.setTurn(Turn.WHITE);
//		List<IAction> availableKingMoves = tempState.getKingMoves();
//
//		if (!availableKingMoves.isEmpty()) {
//
////			min number of moves to reach each of the 16 winning Points
//			int[] distances = new int[16];
//
////			iterate through all the winning points, calculating the min number of moves to reach each one
//			int winningPointIndex = 0;
//			for (int winningPoint : winningPoints) {
//				distances[winningPointIndex] = calcMinMovesToWinningPoint (tempState, winningPoint, 0, kingPosition);
//				winningPointIndex++;
//			}
//			//				elapsedTime = System.currentTimeMillis() - elapsedTime;
//			for (int i = 0; i < distances.length; i++) {
//				switch (distances[i]) {
//				case 1: value += 15; //distanza dalla vittoria = 1 --> valore molto alto!!
//				break;
//
//				case 2: value += 10;
//				break;
//
//				default: value += 0;
//				break;
//				}
//			}
//		}
//		elapsedTime = System.currentTimeMillis() - elapsedTime;
//		if (elapsedTime > 5)
//		System.out.println("ops");
//		return value;
//	}

//	private int calcMinMovesToWinningPoint (IState state, int winningPoint, int moveTerminalCondition, int kingPosition) {
//		if ( ( kingPosition ^ winningPoint ) == 0 )
//			return moveTerminalCondition;
//		else if (moveTerminalCondition == 2) // Dopo aver fatto due mosse non vincenti, non mi interessano le altre
//			return 3; // con 3 non entra nel case della funzione chiamante. 
//
//		List<IAction> availableKingMoves = state.getKingMoves();
//
//		//teniamo traccia del numero di passi per ogni mossa
//		int [] moveCounts = new int[availableKingMoves.size()];
//
//		//iteriamo lungo le possibili mosse correnti del re e vediamo quanto siamo vicini a un Winning Point.
//		int moveIndex = 0;
//		int newKingPos = 0;
//
//		try {
//			for (IAction a : availableKingMoves) {
//				BitboardState tempState = (BitboardState) state.clone();
//				tempState.move(a);
//				tempState.setTurn(Turn.WHITE);
//
//				for (int i=0; i < BOARD_DIVISIONS; i++) {
//					if (Integer.numberOfTrailingZeros(tempState.getBoard()[KING][i]) < 27) { //trovo il settore in cui si trova il re
//						newKingPos = tempState.getBoard()[KING][i];
//					}
//				}
//
//				//qua ci sarebbe un if... Se questa mossa portasse il re a una distanza dalla vittoria minore di quella di partenza:
//				moveCounts[moveIndex] = calcMinMovesToWinningPoint(tempState, winningPoint, moveTerminalCondition+1, newKingPos);
//				moveIndex++;
//			}
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
//
//		//trovo il numero minimo di mosse per raggiungere un winning point, altrimento ritorno 50 se irraggiungibile
//		int min = 50;
//		for (int i = 0; i < moveCounts.length; i++) {
//			int current = moveCounts[i];
//			if (current != 0 && current < min) {
//				min = current;
//			}
//		}
//
//		return min;
//	}

//	public List<Integer> getWinningPoints() {
//		return winningPoints;
//	}


	public int[] getNumPieces() { //int[0] = num bianchi, int[1] = num neri
		int numPieces[] = new int[2];
		for (int i=0; i < BOARD_DIVISIONS; i++) {
			int stateWhite = BOARD_PAWNS_MASK & board[WHITE][i];
			int stateBlack = BOARD_PAWNS_MASK & board[BLACK][i];
			//				int stateKing = BOARD_PAWNS_MASK & board[KING][i];

			while (stateWhite != 0) {
				int pawn = BitboardState.scanMove(stateWhite);
				numPieces[WHITE]++;
				stateWhite ^= pawn;
			}
			while (stateBlack != 0) {
				int pawn = BitboardState.scanMove(stateBlack);
				numPieces[BLACK]++;
				stateBlack ^= pawn;
			}
			if (Integer.numberOfTrailingZeros(board[KING][i]) < 27) {
				numPieces[WHITE]++;
			}
		}	
		return numPieces;
	}

	public List<IAction> getKingMoves() {
		try {
			List<IAction> result = new ArrayList<>();
			for (int i=0; i < BOARD_DIVISIONS; i++) {
				if (Integer.numberOfTrailingZeros(board[KING][i]) < 27)
					result.addAll(this.getPawnMoves(board[KING][i]));
			}
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	public int blackAroundKingValue() {
		int numBlackAround = 0;
		int priority = 0;

		/*		1 - Trovo RE
					2 - Controllo dove si trova:
						-- Fuori dal castello e non nei posti adiacenti a esso: 2 minacingEntities
							--- In questo caso: 	per ogni minacingEntity reale --> numBlackAround += 1
													priority = 2
						-- Caso speciale: 4 minacingEntities
							--- In questo caso:		per ogni minacingEntity reale --> numBlackAround += 1
													priority = 1
		 */


		for (int i=0; i < BOARD_DIVISIONS; i++) {
			if (Integer.numberOfTrailingZeros(board[KING][i]) < 27) { //trovo il settore in cui si trova il re
				int king = BOARD_PAWNS_MASK & board[KING][i];
				BitboardPosition minacingEntity1 = moveLeft(king, i); 	// Si ottiene la cella a sinistra del re
				BitboardPosition minacingEntity2 = moveUp(king, i); 	// Si ottiene la cella sopra il re
				BitboardPosition minacingEntity3 = moveDown(king, i);	// Si ottiene la cella sotto il re
				BitboardPosition minacingEntity4 = moveRight(king, i); 	// Si ottiene la cella a destra del re

				if (i == SECOND_THIRD && contentIsContained(board[KING][i], CAPTURED_KING_MASK)) {
					//in questo caso il re si trova in una posizione "speciale", ovvero in un punto in cui pu� essere mangiato mediante 4 minacingEntities

					//check se c'� un castello o un'altra mia pedina nera su tutti i lati
					if (isKingMinaced(minacingEntity1.getPosition(), i, BLACK) )
						numBlackAround++;
					if (isKingMinaced(minacingEntity2.getPosition(), minacingEntity2.getBoardSector(), BLACK) )
						numBlackAround++;
					if (isKingMinaced(minacingEntity3.getPosition(), minacingEntity3.getBoardSector(), BLACK) )
						numBlackAround++;
					if (isKingMinaced(minacingEntity4.getPosition(), minacingEntity4.getBoardSector(), BLACK) )
						numBlackAround++;
					priority = 1;
				}

				else { // caso di re come 'normale' pedina --> 2 minacingEntites
					//check se c'� un accampamento o un'altra pedina nera sui lati adiacenti
					if (isPawnCaptured(minacingEntity1.getPosition(), i, BLACK) )
						numBlackAround++;
					if (isPawnCaptured(minacingEntity2.getPosition(), minacingEntity2.getBoardSector(), BLACK) )
						numBlackAround++;
					if (isPawnCaptured(minacingEntity3.getPosition(), minacingEntity3.getBoardSector(), BLACK) )
						numBlackAround++;
					if (isPawnCaptured(minacingEntity4.getPosition(), minacingEntity4.getBoardSector(), BLACK) )
						numBlackAround++;
					priority = 2;
				}

			}
		}

		return numBlackAround * priority;
	}

	public String toString() {
		return BitboardUtility.boardToString(board[WHITE], board[BLACK], board[KING]); 
	}

	//		private List<IAction> getMoves(int partialState, Turn turn) {
	//			try {
	//				List<IAction> result = new ArrayList<>();
	//				for (int i=0; i < BOARD_DIVISIONS ; i++) {					
	//					while (partialState != 0) {
	//						int pawnToMove = BitboardState.scanMove(partialState);
	//						pawnToMove = BitboardUtility.setSector(pawnToMove, i);
	//						result.addAll(this.getPawnMoves(pawnToMove));
	////						temp_state = temp_state - (BitboardState.BOARD_PAWNS_MASK & pawnToMove);
	//						partialState ^= (BOARD_PAWNS_MASK & pawnToMove);
	//					}
	//				}
	////				//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"Pawn moves:\n" + result + "\n" +  BitboardUtility.printAllMoves(result));
	//				return result;
	//			} catch (Exception e) {
	//				e.printStackTrace();
	//			}
	//			return null;
	//		}

	public List<IAction> getCurrentMoves() {
		try {
			List<IAction> result = new ArrayList<>();
			for (int i=0; i < BOARD_DIVISIONS ; i++) {
				int temp_state = (BOARD_PAWNS_MASK & board[turn.ordinal()][i]);
				//					result.addAll(getMoves(temp_state, turn));
				while (temp_state != 0) {
					int pawnToMove = BitboardState.scanMove(temp_state);
					pawnToMove = BitboardUtility.setSector(pawnToMove, i);
					result.addAll(this.getPawnMoves(pawnToMove));
					//						temp_state = temp_state - (BitboardState.BOARD_PAWNS_MASK & pawnToMove);
					temp_state ^= (BOARD_PAWNS_MASK & pawnToMove); // QUESTO SPOSTALO SUBITO DOPO SCANMOVE!!!!! (cos� non usi la maschera)
				}
				if (turn == Turn.WHITE) {
//						temp_state = (BOARD_PAWNS_MASK & board[KING][i]);
//						result.addAll(getMoves(temp_state, turn));
//					}

					// Get KING moves
//						temp_state = (BOARD_PAWNS_MASK & board[KING][i]);

					if (Integer.numberOfTrailingZeros(board[KING][i]) < 27)
						result.addAll(this.getPawnMoves(board[KING][i]));
//						while (temp_state != 0) {
////							int pawnToMove = BitboardState.scanMove(temp_state);
////							pawnToMove = BitboardUtility.setSector(pawnToMove, i);
////							result.addAll(this.getPawnMoves(pawnToMove));
////							temp_state = temp_state - (BitboardState.BOARD_PAWNS_MASK & pawnToMove);
////							temp_state ^= (BOARD_PAWNS_MASK & pawnToMove);
//							temp_state = BitboardUtility.setSector(temp_state, i);
//							result.addAll(this.getPawnMoves(temp_state));
//						}
				}
			}
//			//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"Pawn moves:\n" + result + "\n" +  BitboardUtility.printAllMoves(result));
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	public boolean isWinningState() {
		return hasBlackWon() || hasWhiteWon();
	}

	public boolean hasWhiteWon() {
		if (contentIsContained(board[KING][0], FILE_1 | FILE_9 | RANK_1)  
				|| contentIsContained(board[KING][1], FILE_1 | FILE_9)  
				|| contentIsContained(board[KING][2], FILE_1 | FILE_9 | RANK_9)  ) {
			return true;
		}
		return false;
	}

	public boolean hasBlackWon() {
		return ((BOARD_PAWNS_MASK & board[KING][0]) == 0 
				&& (BOARD_PAWNS_MASK & board[KING][1]) == 0 
				&& (BOARD_PAWNS_MASK & board[KING][2]) == 0);
	}

	/// CODICE QUA SOTTO E' MALE.... Ma forse pi� performante
//	private void checkBlackPawnCaptured_evil(int newWhitePawnPosition) {
//		int blackPawnMock = 0;
//		int minacingEntity = 0;
//		int current_file = BitboardUtility.getFile(newWhitePawnPosition);
//		int boardIndex = BitboardUtility.getBoardIndex(newWhitePawnPosition);
//		// check black pawn on the left
//		// check if white is in the left edge
//		if (!contentIsContained(newWhitePawnPosition, FILE_1)) {
//			blackPawnMock = (BOARD_PAWNS_MASK & newWhitePawnPosition) << 1;//
//
//			// check if there is a black pawn not in the edge
//			if (!contentIsContained(blackPawnMock, FILE_1) 
//					&& contentIsContained(blackPawnMock, board[BLACK][boardIndex])) {
//				minacingEntity = blackPawnMock << 1;
//				//check se a sinistra c'� un accampamento, un castello o un'altra pedina bianca
//				if (contentIsContained(minacingEntity, camps_mask_for_capturing[boardIndex]) 
//						|| (boardIndex == SECOND_THIRD && contentIsContained(minacingEntity, CASTLE) )
//						|| contentIsContained(minacingEntity, board[WHITE][boardIndex]) ) {
//					//						//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"BLACK PIECE EATEN");
//					//						board[BLACK][boardIndex] = board[BLACK][boardIndex]- blackPawnMock;
//					board[BLACK][boardIndex] ^= blackPawnMock;
//				}
//			}
//		}
//
//		// check black pawn on the right
//		// check if white is in the right edge
//		if (!contentIsContained(newWhitePawnPosition, FILE_9)) {
//			blackPawnMock = (BOARD_PAWNS_MASK & newWhitePawnPosition) >> 1;
//				// check if there is a black pawn not in the edge
//				if (!contentIsContained(blackPawnMock, FILE_9) 
//						&& contentIsContained(blackPawnMock, board[BLACK][boardIndex])) {
//					minacingEntity = blackPawnMock >> 1;
//			//check se a destra c'� un accampamento, un castello o un'altra pedina bianca
//			if (contentIsContained(minacingEntity, camps_mask_for_capturing[boardIndex]) 
//					|| (boardIndex == SECOND_THIRD && contentIsContained(minacingEntity, CASTLE) )
//					|| contentIsContained(minacingEntity, board[WHITE][boardIndex]) ) {
//				//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"BLACK PIECE EATEN");
//				//						board[BLACK][boardIndex] = board[BLACK][boardIndex]- blackPawnMock;
//				board[BLACK][boardIndex] ^= blackPawnMock;
//			}
//				}
//		}
//
//		// check black pawn on the up
//		// check if white is in the upper edge
//		if (!contentIsContained(newWhitePawnPosition, RANK_1 | RANK_2)) {
//			//				blackPawnMock = (BOARD_PAWNS_MASK & newWhitePawnPosition) >> 9;
//			//				//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"Move: \n" + BitboardUtility.printSector(newWhitePawnPosition));
//			//				//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"Move: \n" + BitboardUtility.printSector(blackPawnMock));
//			int blackMockBoard_index = boardIndex;
//			//				if (blackMockBoard_index > 0 && blackPawnMock == 0) {
//			if (Integer.numberOfTrailingZeros(newWhitePawnPosition) < 9) {
//				blackMockBoard_index--;
//				blackPawnMock = 0x08000000 >> current_file;
//			//					//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"Move: \n" + BitboardUtility.printSector(blackPawnMock));
//			} else {
//				blackPawnMock = (BOARD_PAWNS_MASK & newWhitePawnPosition) >> 9;
//			}
//			// check if there is a black pawn not in the edge
//			if (contentIsContained(blackPawnMock, board[BLACK][blackMockBoard_index])) {
//				int temp_index = blackMockBoard_index;
//				if (Integer.numberOfTrailingZeros(blackPawnMock) < 9) {
//					temp_index--;
//					minacingEntity = 0x08000000 >> current_file;
//				//						//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"Move: \n" + BitboardUtility.printSector(minacingEntity));
//				}  else {
//					minacingEntity = blackPawnMock >> 9;
//				}
//				//check se in alto c'� un accampamento, un castello o un'altra pedina bianca
//				if (contentIsContained(minacingEntity, camps_mask_for_capturing[temp_index]) 
//						|| (temp_index == SECOND_THIRD && contentIsContained(minacingEntity, CASTLE) )
//						|| contentIsContained(minacingEntity, board[WHITE][temp_index]) ) {
//					//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"BLACK PIECE EATEN");
//					//						board[BLACK][blackMockBoard_index] = board[BLACK][blackMockBoard_index]- blackPawnMock;
//					board[BLACK][blackMockBoard_index] ^= blackPawnMock;
//				}
//			}
//		}
//
//		// check black pawn on the down
//		// check if white is in the bottom edge
//		if (!contentIsContained2(newWhitePawnPosition, RANK_8 | RANK_9)) {
//			//				blackPawnMock = (BOARD_PAWNS_MASK & newWhitePawnPosition) << 9;
//			//				//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"Move: \n" + BitboardUtility.printSector(newWhitePawnPosition));
//			//				//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"Move: \n" + BitboardUtility.printSector(blackPawnMock));
//			int blackMockBoard_index = boardIndex;
//			//				if (blackMockBoard_index < (BOARD_DIVISIONS-1) && blackPawnMock == 0) {
//			if (Integer.numberOfTrailingZeros(blackPawnMock) > 18) {
//				blackMockBoard_index++;
//				blackPawnMock = 0x08000000 >> (current_file + 18);
//		//					//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"Move: \n" + BitboardUtility.printSector(blackPawnMock));
//			} else {
//				blackPawnMock = (BOARD_PAWNS_MASK & newWhitePawnPosition) << 9;
//			}
//			// check if there is a black pawn not in the edge
//			if (contentIsContained(blackPawnMock, board[BLACK][blackMockBoard_index])) {
//				//					BitboardPosition downMovement = moveDown(blackPawnMock, blackMockBoard_index);
//				int temp_index = blackMockBoard_index;
//				if (Integer.numberOfTrailingZeros(blackPawnMock) > 18) {
//					temp_index++;
//					minacingEntity = 0x08000000 >> (current_file + 18);
//				//						//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"Move: \n" + BitboardUtility.printSector(minacingEntity));
//				}  else {
//					minacingEntity = blackPawnMock << 9;
//				}
//				//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"Move: \n" + Integer.numberOfLeadingZeros(blackPawnMock));
//				//check se in basso c'� un accampamento, un castello o un'altra pedina bianca
//				if (contentIsContained(minacingEntity, camps_mask_for_capturing[temp_index]) 
//						|| ( temp_index == SECOND_THIRD && contentIsContained(minacingEntity, CASTLE) )
//						|| contentIsContained(minacingEntity, board[WHITE][temp_index]) ) {
//
//					//						board[BLACK][blackMockBoard_index] = board[BLACK][blackMockBoard_index]- blackPawnMock;
//					board[BLACK][blackMockBoard_index] ^= blackPawnMock;
//					//+++System.out.println("Pawn moves:\n" + availableMoves + "\n");"BLACK PIECE EATEN D");
//				}
//			}
//		}
//	}



}


