package it.unibo.ai.didattica.competition.tablut.search;

import it.unibo.ai.didattica.competition.tablut.bitboard.IState;

public interface ITieChecker {
	public boolean isTie(IState state);
}
