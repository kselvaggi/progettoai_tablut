package it.unibo.ai.didattica.competition.tablut.exceptions;

public class BoardIndexException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public BoardIndexException(int index)
	{
		super("The index of the move is not correct: " + index);
	}
}
