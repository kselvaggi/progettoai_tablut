package it.unibo.ai.didattica.competition.tablut.search;

import java.io.IOException;

import it.unibo.ai.didattica.competition.tablut.bitboard.BitboardAction;
import it.unibo.ai.didattica.competition.tablut.bitboard.IAction;
import it.unibo.ai.didattica.competition.tablut.bitboard.IState;

public interface ITranspositionTable {

	public BitboardAction getAction(IState state) throws IOException;
	public void putAction(IState state, IAction action, int depth);
	public void clear();
	
}
