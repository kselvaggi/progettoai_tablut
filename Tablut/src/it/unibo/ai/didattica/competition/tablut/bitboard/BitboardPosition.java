package it.unibo.ai.didattica.competition.tablut.bitboard;

/**
 * This class represents the position of a pawn in the bitboard.
 * It is used inside the state to make the code a bit more readable :)
 * 
 * @author Team CAMST
 * 
 */
public class BitboardPosition {
	
	private int position;
	private int boardSector;
	
	public BitboardPosition() {
		this(0,0);
	}

	public BitboardPosition(int position, int sector) {
		this.position = (0x07FFFFFF & position);
		this.boardSector = sector;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = (0x07FFFFFF & position);
	}

	public int getBoardSector() {
		return boardSector;
	}

	public void setBoardSector(int boardSector) {
		this.boardSector = boardSector;
	}

}
