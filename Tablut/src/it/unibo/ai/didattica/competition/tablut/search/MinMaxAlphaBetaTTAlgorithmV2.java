package it.unibo.ai.didattica.competition.tablut.search;

import java.io.IOException;
import java.util.List;

import it.unibo.ai.didattica.competition.tablut.bitboard.IAction;
import it.unibo.ai.didattica.competition.tablut.bitboard.IState;

public class MinMaxAlphaBetaTTAlgorithmV2 implements SearchingAlgorithms {

	private long elapsedTime; 
	private int expandedStates = 0;
	private ITranspositionTable tranpositionTable;
	
	public MinMaxAlphaBetaTTAlgorithmV2(ITranspositionTable transpositionTable) {
		this.tranpositionTable = transpositionTable;
	}
	
	public ValuedAction searchBestAction(IState state, int depth, boolean isMaximizingPlayer){
		elapsedTime = System.currentTimeMillis();
		ValuedAction valuedAction = new ValuedAction();
		try {
			// ITERATIVE DEEPING TEST
			for (int i=0; i<=depth ; i++) {
				valuedAction = isMaximizingPlayer ? max(state, i, Integer.MIN_VALUE, Integer.MAX_VALUE) : min(state, i, Integer.MIN_VALUE, Integer.MAX_VALUE);
			}
//			valuedAction = isMaximizingPlayer ? max(state, depth, Integer.MIN_VALUE, Integer.MAX_VALUE) : min(state, depth, Integer.MIN_VALUE, Integer.MAX_VALUE);
			
			elapsedTime = System.currentTimeMillis() - elapsedTime;
			System.out.println("MiniMaxAlphaBetaTTV2:");
			System.out.println("Elapsed time: " + elapsedTime);
			System.out.println("Expanded states: " + expandedStates);
			System.out.println(valuedAction);
			expandedStates = 0;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return valuedAction;
	}
	
	private ValuedAction max(IState state, int maxDepth, int alpha, int beta) throws IOException {
		ValuedAction result = new ValuedAction(null, Integer.MIN_VALUE);
		ValuedAction temp = new ValuedAction();
		IAction action = tranpositionTable.getAction(state);
		
		expandedStates++;
	    if(action != null) {
	    	
	    	IState tempState = state.clone();
			tempState.move(action);
			if (tempState.isWinningState()) {
				result.set(action, Integer.MAX_VALUE-1);
				tranpositionTable.putAction(state, action, maxDepth);
				return result;
				
			} else if (maxDepth > 1) {
	
				temp = min(tempState, maxDepth - 1, alpha, beta);
	
			} else {
				temp.set(action, tempState.getHeuristicValue());
			}
				
			if (temp.getValue() > result.getValue()) {
				result.set(action, temp.getValue());
			}
			if (result.getValue() >= beta) {
				tranpositionTable.putAction(state, action, maxDepth);
				return result;
			}
			if(result.getValue() >= alpha) {
				alpha = (int) result.getValue();
			}
	    }
		
		List<IAction> actions = state.getCurrentMoves();

		for (IAction a : actions) {
			expandedStates++;
			IState tempState = state.clone();

			tempState.move(a);

			if (tempState.isWinningState()) {
				result.set(a, Integer.MAX_VALUE-1);
				tranpositionTable.putAction(state, a, maxDepth);
				return result;
			
			} else if (maxDepth > 1) {

				temp = min(tempState, maxDepth - 1, alpha, beta);

			} else {
				temp.set(a, tempState.getHeuristicValue());
			}
			
			if (temp.getValue() > result.getValue()) {
				result.set(a, temp.getValue());
			}
			if (result.getValue() >= beta) {
				tranpositionTable.putAction(state, a, maxDepth);
				return result;
			}
			if(result.getValue() >= alpha) {
				alpha = (int) result.getValue();
			}
//			if (maxDepth == 4)
//				System.out.println("ALT");
			
		}
		
		if (result.getAction() != null) {
			tranpositionTable.putAction(state, result.getAction(), maxDepth);
//	    	if (result.getAction().getFrom().equalsIgnoreCase("f2") && result.getAction().getTo().equalsIgnoreCase("f2")) {
//	    		System.out.println("ALT");
//	    		BitboardHash hash = BitboardHash.getHashSymm((BitboardState) state);
//	    		long tem1p = hash.getHash();
//	    		tem1p = 0;
//	    	}
		}
		return result;
	}
	
	private ValuedAction min(IState state, int maxDepth, int alpha, int beta) throws IOException {
		ValuedAction result = new ValuedAction(null, Integer.MAX_VALUE);
		ValuedAction temp = new ValuedAction();
		IAction action = tranpositionTable.getAction(state);
		expandedStates++;
		
		if (action != null) {
//			if (action.getFrom().equalsIgnoreCase("h5") && action.getTo().equalsIgnoreCase("g5")) {
//				System.out.println("ALT");
//			}
			IState tempState = state.clone();
			tempState.move(action);
			
			if (tempState.isWinningState()) {
				result.set(action, Integer.MIN_VALUE+1); 	
				tranpositionTable.putAction(state, action,maxDepth);
				return result;
				
			} else if (maxDepth > 1) {
	
				temp = max(tempState, maxDepth - 1, alpha, beta);
	
			} else {
				temp.set(action, tempState.getHeuristicValue());
			}
				
			if (temp.getValue() < result.getValue()) {
				result.set(action, temp.getValue());
			}
			if (result.getValue() <= alpha) {
				tranpositionTable.putAction(state, action, maxDepth);
				return result;
			}
			if(result.getValue() <= beta) {
				beta = (int) result.getValue();
			}
		}
		List<IAction> actions = state.getCurrentMoves();

		for (IAction a : actions) {
			expandedStates++;
			IState tempState = state.clone();
			tempState.move(a);
			
			if (tempState.isWinningState()) {
				result.set(a, Integer.MIN_VALUE+1);
//		    	if (result.getAction().getFrom().equalsIgnoreCase("h5") && result.getAction().getTo().equalsIgnoreCase("g5")) {
//		    		System.out.println("ALT");
//		    	}
				tranpositionTable.putAction(state, a, maxDepth);
				return result;
			
			} else if (maxDepth > 1) {

				temp = max(tempState, maxDepth - 1, alpha, beta);

			} else {
				temp.set(a, tempState.getHeuristicValue());
			}
//			if (maxDepth == 3)
//				System.out.println("ALT");
			if (temp.getValue() < result.getValue()) {
				result.set(a, temp.getValue());
			}
			if (result.getValue() <= alpha) {
//		    	if (result.getAction().getFrom().equalsIgnoreCase("h5") && result.getAction().getTo().equalsIgnoreCase("g5")) {
//		    		System.out.println("ALT");
//		    	}
				tranpositionTable.putAction(state, a, maxDepth);
				return result;
			}
			if(result.getValue() <= beta) {
				beta = (int) result.getValue();
			}
			
		}
//		if (maxDepth == 3)
//			System.out.println("ALT");
		if (result.getAction() != null) 
			tranpositionTable.putAction(state, result.getAction(), maxDepth);
		
		return result;
	}

}
