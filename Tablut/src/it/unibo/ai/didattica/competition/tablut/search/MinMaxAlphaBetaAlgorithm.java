package it.unibo.ai.didattica.competition.tablut.search;

import java.util.List;

import it.unibo.ai.didattica.competition.tablut.bitboard.IAction;
import it.unibo.ai.didattica.competition.tablut.bitboard.IState;

public class MinMaxAlphaBetaAlgorithm implements SearchingAlgorithms {

	private long elapsedTime;
	private int expandedStates; 
	
	public MinMaxAlphaBetaAlgorithm() {
	}
	
	@Override
	public ValuedAction searchBestAction(IState state, int depth, boolean isMaximizingPlayer) {
		expandedStates = 0;
		elapsedTime = System.currentTimeMillis();
		ValuedAction valuedAction = calculate(state, depth, isMaximizingPlayer);
		elapsedTime = System.currentTimeMillis() - elapsedTime;
		System.out.println("MiniMaxAphaBeta:");
		System.out.println("Elapsed time: " + elapsedTime);
		System.out.println("Expanded states: " + expandedStates);
		System.out.println(valuedAction);
		
		return valuedAction;
	}
	
	public ValuedAction calculate(IState state, int depth, boolean isMaximizingPlayer) {
		List<IAction> availableMoves = null;
		double max = Integer.MIN_VALUE;
		double min = Integer.MAX_VALUE;
		double alpha = -10000;
		double beta = 10000;
		ValuedAction bestMove = new ValuedAction();

		try {
			availableMoves = state.getCurrentMoves();
//			System.out.println("Pawn moves:\n" + availableMoves + "\n" +  BitboardUtility.printAllMoves(availableMoves));	
			for (IAction action : availableMoves) {
				expandedStates++;
//				if (action.getFrom().equalsIgnoreCase("b3") && action.getTo().equalsIgnoreCase("a3")) {
//					System.out.println("blabla");
//				}
				IState tempState = state.clone();
				tempState.move(action);
				
				double currentValue = calculate_rec(depth, tempState, alpha, beta, !isMaximizingPlayer);
				if (isMaximizingPlayer) {
					if (currentValue >= max) {
						max = currentValue;
//						bestMove = action;
						bestMove.set(action, (int) currentValue);
					}
				} else {
					if (currentValue <= min) {
						min = currentValue;
//						bestMove = action;
						bestMove.set(action, (int) currentValue);
					}
				}
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return bestMove;
	}

	public double calculate_rec(int depth, IState state, double alpha, double beta, boolean isMaximizingPlayer) {
		
		if (state.hasWhiteWon() && state.getTurn().ordinal() == 1)
			return Integer.MAX_VALUE;
		else if (state.hasWhiteWon() && state.getTurn().ordinal() == 0)
			return Integer.MIN_VALUE;
		else if (state.hasBlackWon() && state.getTurn().ordinal() == 1)
			return Integer.MAX_VALUE;
		else if (state.hasBlackWon() && state.getTurn().ordinal() == 0)
			return Integer.MIN_VALUE;
		
		if (depth == 0 ) {
			return state.getHeuristicValue();
		}
		double bestValue;
		List<IAction> availableMoves = null;
		if (isMaximizingPlayer) {
			bestValue = -9999;
			try {
				availableMoves = state.getCurrentMoves();
//				if (availableMoves.isEmpty())
//					System.out.println("WELLA");
				//System.out.println("Pawn moves:\n" + availableMoves +  "\n" +  BitboardUtility.printAllMoves(availableMoves));
				for (IAction action : availableMoves) {
					expandedStates++;
//					if (action.getFrom().contentEquals("f7") && action.getTo().contentEquals("h7")) 
//						System.out.println("yo");
//					if (action.getFrom().contentEquals("f7") && action.getTo().contentEquals("i7")) 
//						System.out.println("yo");
					IState tempState = state.clone();
//					System.out.println("State:\n" +  tempState);
					tempState.move(action);
//					System.out.println("State:\n" +  tempState);
					bestValue = Math.max (bestValue, calculate_rec(depth-1, tempState, alpha, beta, !isMaximizingPlayer));	
					alpha = Math.max(alpha, bestValue);
					if (beta <= alpha) {
						return bestValue;
					}
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} else {
			bestValue = 9999;
			try {
				availableMoves = state.getCurrentMoves();
//				if (availableMoves.isEmpty())
					//System.out.println("WELLA");
				//System.out.println("Pawn moves:\n" + availableMoves +  "\n" +  BitboardUtility.printAllMoves(availableMoves));
				for (IAction action : availableMoves) {
					expandedStates++;
					IState tempState = state.clone();
//					System.out.println("State:\n" +  tempState);
					tempState.move(action);
//					System.out.println("State:\n" +  tempState);
					bestValue = Math.min (bestValue, calculate_rec(depth-1, tempState, alpha, beta, !isMaximizingPlayer));	
					beta = Math.min(beta, bestValue);
					if (beta <= alpha) {
						return bestValue;
					}
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return bestValue;
	}

}
