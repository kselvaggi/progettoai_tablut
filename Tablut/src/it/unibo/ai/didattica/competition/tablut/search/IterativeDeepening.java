package it.unibo.ai.didattica.competition.tablut.search;

import it.unibo.ai.didattica.competition.tablut.bitboard.IState;

public class IterativeDeepening implements Runnable{

	private SearchingAlgorithms minimax;
	protected IState state;
	protected int startingDepth;
	protected boolean isMaximizingPlayer = true;
	
	protected ValuedAction selectedValuedAction;
	
	public IterativeDeepening(SearchingAlgorithms minimax, IState state, int startingDepth, boolean isMaximizingPlayer) {
		if (minimax == null)
			throw new IllegalArgumentException("Minimax can't be null");
		if (state == null)
			throw new IllegalArgumentException("State can't be null");
		if (startingDepth < 0)
			throw new IllegalArgumentException("Starting depth can't be negative");
		
		this.minimax = minimax;
		this.state = state;
		this.startingDepth = startingDepth;
		this.isMaximizingPlayer = isMaximizingPlayer;
		
		selectedValuedAction = null;
	}

	@Override
	public void run() {
		int depth = startingDepth;
		while (true) {
			selectedValuedAction = (ValuedAction) minimax.searchBestAction(state, depth, isMaximizingPlayer);
			System.out.println("Level " + depth + " completed\n");
			if (selectedValuedAction.getValue() >= Integer.MAX_VALUE-3 || selectedValuedAction.getValue() <= Integer.MIN_VALUE+3)
				break;
			depth++;
		}
	}
	
	public ValuedAction getSelectedValuedAction() {
		return this.selectedValuedAction;
	}
	
	public SearchingAlgorithms getMinimax() {
		return this.minimax;
	}

	
}
