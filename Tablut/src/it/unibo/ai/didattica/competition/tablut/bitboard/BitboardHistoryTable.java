package it.unibo.ai.didattica.competition.tablut.bitboard;

import it.unibo.ai.didattica.competition.tablut.search.IHistoryTable;

/**
 * This class represents the History table for the History Heuristic
 * 
 * @author Team CAMST
 * 
 */
public class BitboardHistoryTable implements IHistoryTable {

	private int[][] historyTable;
	
	public BitboardHistoryTable() {
		// 25 perche' il 25esimo e' la posizione vuota
		historyTable = new int[81][81];
	}

	@Override
	public long getValue(IAction action) {
		BitboardAction bAction = (BitboardAction) action;
		int boardIndexFrom = BitboardUtility.getBoardIndex(bAction.getBitboardFrom());
		int boardIndexTo = BitboardUtility.getBoardIndex(bAction.getBitboardTo());
		int positionFrom = Integer.numberOfTrailingZeros(BitboardState.BOARD_PAWNS_MASK & bAction.getBitboardFrom()) + boardIndexFrom * 27;
		int positionTo = Integer.numberOfTrailingZeros(BitboardState.BOARD_PAWNS_MASK & bAction.getBitboardTo()) + boardIndexTo * 27;
		return historyTable[positionFrom][positionTo];
	}

	@Override
	public void incrementValue(IAction action, long depth) {
		BitboardAction bAction = (BitboardAction) action;
		int boardIndexFrom = BitboardUtility.getBoardIndex(bAction.getBitboardFrom());
		int boardIndexTo = BitboardUtility.getBoardIndex(bAction.getBitboardTo());
		int positionFrom = Integer.numberOfTrailingZeros(BitboardState.BOARD_PAWNS_MASK & bAction.getBitboardFrom()) + boardIndexFrom * 27;
		int positionTo = Integer.numberOfTrailingZeros(BitboardState.BOARD_PAWNS_MASK & bAction.getBitboardTo()) + boardIndexTo * 27;
		historyTable[positionFrom][positionTo] += depth * depth;
	}

}
