//package it.unibo.ai.didattica.competition.tablut.bitboard;
//
//public class BitboardHash {
//			private long hash;
//			private byte symms;
//			
//			public BitboardHash(long hash, byte symms) {
//				this.hash = hash;
//				this.symms = symms;
//			}
//			
//			public long getHash() {
//				return hash;
//			}
//			
//			public byte getSymms() {
//				return symms;
//			}
//		
//		
//		public static final byte ROTATION_90 = 			0b00001;
//		public static final byte ROTATION_180 = 		0b00010;
//		public static final byte ROTATION_270 =			0b00011;
//		public static final byte MIRROR_VERTICAL =		0b00100;
//		public static final byte MIRROR_DIAGONAL = 		0b01000;
//		public static final byte MIRROR_HORIZONTAL =	0b10000;
//		
//		public static BitboardHash getHashSymm(BitboardState state) {
//			byte symms = 0;
//			long hash = 1;
//			int[] stateWhite = state.getBoard()[0]; // WHITE
//			int[] stateBlack = state.getBoard()[1]; // BLACK
//			int[] stateKing  = state.getBoard()[2];  // KING
//			int[] tempBoardW;
//			int[] tempBoardB;
//			int[] tempBoardK;
//			long tempHash = 1;
//
////			for (int i=0 ; i<3 ; i++) {
////				for (int j=0; j<3; j++) {
////					hash = 31 * hash + state.getBoard()[i][j];
////				}
////			}
//			
//			hash = 3 * hash  + state.getBoard()[0][0];
//			hash = 5 * hash  + state.getBoard()[0][1];
//			hash = 7 * hash  + state.getBoard()[0][2];
//			hash = 11 * hash + state.getBoard()[1][0];
//			hash = 13 * hash + state.getBoard()[1][1];
//			hash = 17 * hash + state.getBoard()[1][2];
//			hash = 19 * hash + state.getBoard()[2][0];
//			hash = 23 * hash + state.getBoard()[2][1];
//			hash = 29 * hash + state.getBoard()[2][2];
//			 
////			// rotation 90
////			tempHash = 1;
////			tempBoardW = BitboardUtility.rotationClockwise90(stateWhite);
////			tempBoardB = BitboardUtility.rotationClockwise90(stateBlack);
////			tempBoardK = BitboardUtility.rotationClockwise90(stateKing);
////
////			for (int a=0 ; a<3 ; a++) {
////				tempHash = 31 * tempHash + tempBoardW[a];
////				tempHash = 31 * tempHash + tempBoardB[a];
////				tempHash = 31 * tempHash + tempBoardK[a];
////			}
////
////			if(tempHash < hash) {
////				hash = tempHash;
////				symms = ROTATION_90;
////			}
////			
////			// rotation 180
////			tempHash = 1;
////			tempBoardW = BitboardUtility.rotationClockwise180(stateWhite);
////			tempBoardB = BitboardUtility.rotationClockwise180(stateBlack);
////			tempBoardK = BitboardUtility.rotationClockwise180(stateKing);
////
////			for (int a=0 ; a<3 ; a++) {
////				tempHash = 31 * tempHash + tempBoardW[a];
////				tempHash = 31 * tempHash + tempBoardB[a];
////				tempHash = 31 * tempHash + tempBoardK[a];
////			}
////
////			if(tempHash < hash) {
////				hash = tempHash;
////				symms = ROTATION_180;
////			}
////
////			// rotation 270
////			tempHash = 1;
////			tempBoardW = BitboardUtility.rotationAntiClockwise90(stateWhite);
////			tempBoardB = BitboardUtility.rotationAntiClockwise90(stateBlack);
////			tempBoardK = BitboardUtility.rotationAntiClockwise90(stateKing);
////
////			for (int a=0 ; a<3 ; a++) {
////				tempHash = 31 * tempHash + tempBoardW[a];
////				tempHash = 31 * tempHash + tempBoardB[a];
////				tempHash = 31 * tempHash + tempBoardK[a];
////			}
////
////			if(tempHash < hash) {
////				hash = tempHash;
////				symms = ROTATION_270;
////			}
////
////			// mirror Vertical
////			tempHash = 1;
////			tempBoardW = BitboardUtility.mirrorVertical(stateWhite);
////			tempBoardB = BitboardUtility.mirrorVertical(stateBlack);
////			tempBoardK = BitboardUtility.mirrorVertical(stateKing);
////
////			for (int a=0 ; a<3 ; a++) {
////				tempHash = 31 * tempHash + tempBoardW[a];
////				tempHash = 31 * tempHash + tempBoardB[a];
////				tempHash = 31 * tempHash + tempBoardK[a];
////			}
////
////			if(tempHash < hash) {
////				hash = tempHash;
////				symms = MIRROR_VERTICAL;
////			}
////
////			// mirror vertical - rotation 90
////			tempHash = 1;
////			tempBoardW = BitboardUtility.rotationClockwise90(BitboardUtility.mirrorVertical(stateWhite));
////			tempBoardB = BitboardUtility.rotationClockwise90(BitboardUtility.mirrorVertical(stateBlack));
////			tempBoardK = BitboardUtility.rotationClockwise90(BitboardUtility.mirrorVertical(stateKing));
////
////			for (int a=0 ; a<3 ; a++) {
////				tempHash = 31 * tempHash + tempBoardW[a];
////				tempHash = 31 * tempHash + tempBoardB[a];
////				tempHash = 31 * tempHash + tempBoardK[a];
////			}
////
////			if(tempHash < hash) {
////				hash = tempHash;
////				symms = MIRROR_VERTICAL;
////				symms |= ROTATION_90;
////			}
////
////			// mirror vertical - rotation 180
////			tempHash = 1;
////			tempBoardW = BitboardUtility.rotationClockwise180(BitboardUtility.mirrorVertical(stateWhite));
////			tempBoardB = BitboardUtility.rotationClockwise180(BitboardUtility.mirrorVertical(stateBlack));
////			tempBoardK = BitboardUtility.rotationClockwise180(BitboardUtility.mirrorVertical(stateKing));
////
////			for (int a=0 ; a<3 ; a++) {
////				tempHash = 31 * tempHash + tempBoardW[a];
////				tempHash = 31 * tempHash + tempBoardB[a];
////				tempHash = 31 * tempHash + tempBoardK[a];
////			}
////
////			if(tempHash < hash) {
////				hash = tempHash;
////				symms = MIRROR_VERTICAL;
////				symms |= ROTATION_180;
////			}
////
////			// mirror vertical - rotation 270
////			tempHash = 1;
////			tempBoardW = BitboardUtility.rotationAntiClockwise90(BitboardUtility.mirrorVertical(stateWhite));
////			tempBoardB = BitboardUtility.rotationAntiClockwise90(BitboardUtility.mirrorVertical(stateBlack));
////			tempBoardK = BitboardUtility.rotationAntiClockwise90(BitboardUtility.mirrorVertical(stateKing));
////
////			for (int a=0 ; a<3 ; a++) {
////				tempHash = 31 * tempHash + tempBoardW[a];
////				tempHash = 31 * tempHash + tempBoardB[a];
////				tempHash = 31 * tempHash + tempBoardK[a];
////			}
////
////			if(tempHash < hash) {
////				hash = tempHash;
////				symms = MIRROR_VERTICAL;
////				symms |= ROTATION_270;
////			}
//
//			hash = hash | (state.getTurn().ordinal() << 63);
//
//			return new BitboardHash(hash, symms);
//		}
//}






/** HASH CON STRINGHE **/
package it.unibo.ai.didattica.competition.tablut.bitboard;

/**
 * Used to compute the "hash"* of the table
 * There are two usages of this class:
 * - The first one is with trasposition tables in which we consider the board symmetries
 * - The simple unique identifier of the a board, without simmetries. It is used in order to check tie situation.
 * 
 *  *It is not an hash, it is just a concatenation between string. If we compute the hash, there are collisions...
 *   It may be a good thing if a perfect hashing function could be applied (performances)
 * 
 * @author Team CAMST
 * 
 */
public class BitboardHash {
			private String hash;
			private byte symms;
			
			public BitboardHash(String hash, byte symms) {
				this.hash = hash;
				this.symms = symms;
			}
			
			public String getHash() {
				return hash;
			}
			
			public byte getSymms() {
				return symms;
			}
		
		
			
		public static final byte ROTATION_90 = 			0b00001;
		public static final byte ROTATION_180 = 		0b00010;
		public static final byte ROTATION_270 =			0b00011;
		public static final byte MIRROR_VERTICAL =		0b00100;
		public static final byte MIRROR_DIAGONAL = 		0b01000;
		public static final byte MIRROR_HORIZONTAL =	0b10000;
		
		public static BitboardHash getHashSymm(BitboardState state) {
			byte symms = 0;
			String hash;
			int[] stateWhite = state.getBoard()[0]; // WHITE
			int[] stateBlack = state.getBoard()[1]; // BLACK
			int[] stateKing = state.getBoard()[2];  // KING
			int[] tempBoardW;
			int[] tempBoardB;
			int[] tempBoardK;
			StringBuilder sb = new StringBuilder();
			
			for (int a=0; a<3; a++) {
				sb.append(state.getBoard()[0][a]);
				sb.append(state.getBoard()[1][a]);
				sb.append(state.getBoard()[2][a]);
			}
			hash = sb.toString();

			// rotation 90
			sb.setLength(0);
			tempBoardW = BitboardUtility.rotationClockwise90(stateWhite);
			tempBoardB = BitboardUtility.rotationClockwise90(stateBlack);
			tempBoardK = BitboardUtility.rotationClockwise90(stateKing);

			for (int a=0; a<3; a++) {
				sb.append(tempBoardW[a]);
				sb.append(tempBoardB[a]);
				sb.append(tempBoardK[a]);
			}

			if(sb.toString().compareTo(hash) < 0) {
				hash = sb.toString();
				symms = ROTATION_90;
			}
			
			// rotation 180
			sb.setLength(0);
			tempBoardW = BitboardUtility.rotationClockwise180(stateWhite);
			tempBoardB = BitboardUtility.rotationClockwise180(stateBlack);
			tempBoardK = BitboardUtility.rotationClockwise180(stateKing);

			for (int a=0; a<3; a++) {
				sb.append(tempBoardW[a]);
				sb.append(tempBoardB[a]);
				sb.append(tempBoardK[a]);
			}

			if(sb.toString().compareTo(hash) < 0) {
				hash = sb.toString();
				symms = ROTATION_180;
			}

			// rotation 270
			sb.setLength(0);
			tempBoardW = BitboardUtility.rotationAntiClockwise90(stateWhite);
			tempBoardB = BitboardUtility.rotationAntiClockwise90(stateBlack);
			tempBoardK = BitboardUtility.rotationAntiClockwise90(stateKing);

			for (int a=0; a<3; a++) {
				sb.append(tempBoardW[a]);
				sb.append(tempBoardB[a]);
				sb.append(tempBoardK[a]);
			}

			if(sb.toString().compareTo(hash) < 0) {
				hash = sb.toString();
				symms = ROTATION_270;
			}

			// mirror Vertical
			sb.setLength(0);
			tempBoardW = BitboardUtility.mirrorVertical(stateWhite);
			tempBoardB = BitboardUtility.mirrorVertical(stateBlack);
			tempBoardK = BitboardUtility.mirrorVertical(stateKing);

			for (int a=0; a<3; a++) {
				sb.append(tempBoardW[a]);
				sb.append(tempBoardB[a]);
				sb.append(tempBoardK[a]);
			}

			if(sb.toString().compareTo(hash) < 0) {
				hash = sb.toString();
				symms = MIRROR_VERTICAL;
			}
			
			// mirror vertical - rotation 90
			sb.setLength(0);
			tempBoardW = BitboardUtility.rotationClockwise90(BitboardUtility.mirrorVertical(stateWhite));
			tempBoardB = BitboardUtility.rotationClockwise90(BitboardUtility.mirrorVertical(stateBlack));
			tempBoardK = BitboardUtility.rotationClockwise90(BitboardUtility.mirrorVertical(stateKing));

			for (int a=0; a<3; a++) {
				sb.append(tempBoardW[a]);
				sb.append(tempBoardB[a]);
				sb.append(tempBoardK[a]);
			}

			if(sb.toString().compareTo(hash) < 0) {
				hash = sb.toString();
				symms = MIRROR_VERTICAL;
				symms |= ROTATION_90;
			}

			// mirror vertical - rotation 180
			sb.setLength(0);
			tempBoardW = BitboardUtility.rotationClockwise180(BitboardUtility.mirrorVertical(stateWhite));
			tempBoardB = BitboardUtility.rotationClockwise180(BitboardUtility.mirrorVertical(stateBlack));
			tempBoardK = BitboardUtility.rotationClockwise180(BitboardUtility.mirrorVertical(stateKing));

			for (int a=0; a<3; a++) {
				sb.append(tempBoardW[a]);
				sb.append(tempBoardB[a]);
				sb.append(tempBoardK[a]);
			}

			if(sb.toString().compareTo(hash) < 0) {
				hash = sb.toString();
				symms = MIRROR_VERTICAL;
				symms |= ROTATION_180;
			}

			// mirror vertical - rotation 270
			sb.setLength(0);
			tempBoardW = BitboardUtility.rotationAntiClockwise90(BitboardUtility.mirrorVertical(stateWhite));
			tempBoardB = BitboardUtility.rotationAntiClockwise90(BitboardUtility.mirrorVertical(stateBlack));
			tempBoardK = BitboardUtility.rotationAntiClockwise90(BitboardUtility.mirrorVertical(stateKing));

			for (int a=0; a<3; a++) {
				sb.append(tempBoardW[a]);
				sb.append(tempBoardB[a]);
				sb.append(tempBoardK[a]);
			}

			if(sb.toString().compareTo(hash) < 0) {
				hash = sb.toString();
				symms = MIRROR_VERTICAL;
				symms |= ROTATION_270;
			}


			hash += state.getTurn().ordinal();

			return new BitboardHash(hash, symms);
		}
		
		public static BitboardHash getHashBoard(BitboardState state) {
			byte symms = 0;
			String hash;
			StringBuilder sb = new StringBuilder();
			
			for (int a=0; a<3; a++) {
				sb.append(state.getBoard()[0][a]);
				sb.append(state.getBoard()[1][a]);
				sb.append(state.getBoard()[2][a]);
			}
			hash = sb.toString();

			hash += state.getTurn().ordinal();

			return new BitboardHash(hash, symms);
		}
}