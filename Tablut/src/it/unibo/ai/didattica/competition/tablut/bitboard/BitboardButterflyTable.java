package it.unibo.ai.didattica.competition.tablut.bitboard;

import java.util.Arrays;

import it.unibo.ai.didattica.competition.tablut.search.IHistoryTable;

public class BitboardButterflyTable implements IHistoryTable{

private int[][][] butterflyTable;
	
	public BitboardButterflyTable() {
		// 25 perche' il 25esimo e' la posizione vuota
		butterflyTable = new int[2][81][81];
		for (int i = 0; i < 2 ; i++) {
			for (int j = 0; j < 81 ; j++) {
				for (int k = 0; k < 81 ; k++) {
					butterflyTable[i][j][k] = 1;
				}
			}
		}
	}

	@Override
	public long getValue(IAction action) {
		BitboardAction bAction = (BitboardAction) action;
		
		int boardIndexFrom = BitboardUtility.getBoardIndex(bAction.getBitboardFrom());
		int boardIndexTo = BitboardUtility.getBoardIndex(bAction.getBitboardTo());
		int positionFrom = Integer.numberOfTrailingZeros(BitboardState.BOARD_PAWNS_MASK & bAction.getBitboardFrom()) + boardIndexFrom * 27;
		int positionTo = Integer.numberOfTrailingZeros(BitboardState.BOARD_PAWNS_MASK & bAction.getBitboardTo()) + boardIndexTo * 27;
		return butterflyTable[bAction.getTurn().ordinal()][positionFrom][positionTo];
	}

	@Override
	public void incrementValue(IAction action, long depth) {
		BitboardAction bAction = (BitboardAction) action;
		int boardIndexFrom = BitboardUtility.getBoardIndex(bAction.getBitboardFrom());
		int boardIndexTo = BitboardUtility.getBoardIndex(bAction.getBitboardTo());
		int positionFrom = Integer.numberOfTrailingZeros(BitboardState.BOARD_PAWNS_MASK & bAction.getBitboardFrom()) + boardIndexFrom * 27;
		int positionTo = Integer.numberOfTrailingZeros(BitboardState.BOARD_PAWNS_MASK & bAction.getBitboardTo()) + boardIndexTo * 27;
		butterflyTable[bAction.getTurn().ordinal()][positionFrom][positionTo] += 1;
	}
	
}
