package it.unibo.ai.didattica.competition.tablut.search;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;

import it.unibo.ai.didattica.competition.tablut.bitboard.IAction;
import it.unibo.ai.didattica.competition.tablut.bitboard.IState;

public class HistoryAlphaBetaTransposition implements SearchingAlgorithms {
	private int expandedStates = 0;
	private long elapsedTime;
	
	private IHistoryTable historyTable;
	private ITranspositionTable transpositionTable;

	public HistoryAlphaBetaTransposition(IHistoryTable historyTable, ITranspositionTable transpositionTable) {
		this.historyTable = historyTable;
		this.transpositionTable = transpositionTable;
	}

	@Override
	public ValuedAction searchBestAction(IState state, int depth, boolean isMaximizingPlayer) {
		elapsedTime = System.currentTimeMillis();
		ValuedAction valuedAction = new ValuedAction();

		try {
//			Iterative deeping search
//			for (int i=0; i<=depth ; i++) {
//				valuedAction = isMaximizingPlayer ? max(state, i, Integer.MIN_VALUE, Integer.MAX_VALUE) : min(state, i, Integer.MIN_VALUE, Integer.MAX_VALUE);
//			}

			valuedAction = isMaximizingPlayer ? max(state, depth, Integer.MIN_VALUE, Integer.MAX_VALUE) : min(state, depth, Integer.MIN_VALUE, Integer.MAX_VALUE);

			elapsedTime = System.currentTimeMillis() - elapsedTime;
			System.out.println("HistoryAlphaBetaTransposition:");
			System.out.println("Elapsed time: " + elapsedTime);
			System.out.println("Expanded states: " + expandedStates);
			System.out.println("Selected action is: " + valuedAction);

			expandedStates = 0;
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return valuedAction;
	}
	
	private ValuedAction max(IState state, int maxDepth, int alpha, int beta) throws IOException {
		ValuedAction result = new ValuedAction(null, Integer.MIN_VALUE);
		ValuedAction temp = new ValuedAction();

		//Tranposition Handling
		IAction action =  transpositionTable.getAction(state);

		if (action != null) {
			IState tempState = state.clone();
			tempState.move(action);

			if (tempState.isWinningState()) {
				result.set(action, Integer.MAX_VALUE-1);
				transpositionTable.putAction(state, action, maxDepth);
				historyTable.incrementValue(result.getAction(), maxDepth);
				return result;
				
			} else if (maxDepth > 1) {
				temp = min(tempState, maxDepth - 1, alpha, beta);

			} else {
				temp.set(action, tempState.getHeuristicValue());
			}

			if (temp.getValue() > result.getValue()) {
				result.set(action, temp.getValue());
			}
			if (result.getValue() >= beta) {
				transpositionTable.putAction(state, action, maxDepth);
				historyTable.incrementValue(result.getAction(), maxDepth);
				return result;
			} 
			
			if(result.getValue() >= alpha) {
				alpha = (int) result.getValue();
			}
		}

		List<IAction> actions = state.getCurrentMoves();

		for (IAction a : actions) {
			expandedStates++;
			IState tempState = state.clone();

			tempState.move(a);

			if (tempState.isWinningState()) {
				result.set(a, Integer.MAX_VALUE-1);
				transpositionTable.putAction(state, a, maxDepth);
				historyTable.incrementValue(result.getAction(), maxDepth);
				return result;

			} else if (maxDepth > 1) {
				temp = min(tempState, maxDepth - 1, alpha, beta);

			} else {
				temp.set(a, tempState.getHeuristicValue());
			}

			if (temp.getValue() > result.getValue()) {
				result.set(a, temp.getValue());
			}
			if (result.getValue() >= beta) {
				transpositionTable.putAction(state, a, maxDepth);
				historyTable.incrementValue(result.getAction(), maxDepth);

				return result;
			} 
			
			if(result.getValue() >= alpha) {
				alpha = (int) result.getValue();
			}
		}

		if (result.getAction() != null) {
			transpositionTable.putAction(state, result.getAction(), maxDepth);
			historyTable.incrementValue(result.getAction(), maxDepth);
		}
		return result;
	}
	
	private ValuedAction min(IState state, int maxDepth, int alpha, int beta) throws IOException {
		ValuedAction result = new ValuedAction(null, Integer.MAX_VALUE);
		ValuedAction temp = new ValuedAction();
		IAction action = transpositionTable.getAction(state);
		
		expandedStates++;
		
		if (action != null) {

			IState tempState = state.clone();
			tempState.move(action);
			
			if (tempState.isWinningState()) {
				result.set(action, Integer.MIN_VALUE+1); 	
				transpositionTable.putAction(state, action,maxDepth);
				historyTable.incrementValue(result.getAction(), maxDepth);
				return result;
				
			} else if (maxDepth > 1) {
	
				temp = max(tempState, maxDepth - 1, alpha, beta);
	
			} else {
				temp.set(action, tempState.getHeuristicValue());
			}
				
			if (temp.getValue() < result.getValue()) {
				result.set(action, temp.getValue());
			}
			if (result.getValue() <= alpha) {
				transpositionTable.putAction(state, action, maxDepth);
				historyTable.incrementValue(result.getAction(), maxDepth);

				return result;
			} 
			
			if(result.getValue() <= beta) {
				beta = (int) result.getValue();
			}
		}
		
		List<IAction> actions = state.getCurrentMoves();

		for (IAction a : actions) {
			expandedStates++;
			IState tempState = state.clone();
			tempState.move(a);
			
			if (tempState.isWinningState()) {
				result.set(a, Integer.MIN_VALUE+1);
				transpositionTable.putAction(state, a, maxDepth);
				historyTable.incrementValue(result.getAction(), maxDepth);

				return result;
			
			} else if (maxDepth > 1) {

				temp = max(tempState, maxDepth - 1, alpha, beta);

			} else {
				temp.set(a, tempState.getHeuristicValue());
			}
			
			if (temp.getValue() < result.getValue()) {
				result.set(a, temp.getValue());
			}
			
			if (result.getValue() <= alpha) {
				transpositionTable.putAction(state, a, maxDepth);
				historyTable.incrementValue(result.getAction(), maxDepth);

				return result;
				
			} 
			
			if (result.getValue() <= beta) {
				beta = (int) result.getValue();
			}
		}
		
		if (result.getAction() != null) 
			transpositionTable.putAction(state, result.getAction(), maxDepth);
		
		return result;
	}
	
	class IActionComparator implements Comparator<IAction> {
		
		@Override
		public int compare(IAction arg0, IAction arg1) {			
			return historyTable.getValue(arg0) == historyTable.getValue(arg1)  ? 0 : historyTable.getValue(arg0) > historyTable.getValue(arg1) ? -1 : 1;
		}
	
	}
}
