package it.unibo.ai.didattica.competition.tablut.bitboard;

/**
 * This class represents the inferace for the Actions that can be performed on the board
 * @author Team CAMST
 * 
 */
public interface IAction {

	public String getFrom();
	public String getTo();
	public Turn getTurn();
//
//	public void setFrom(String from) {
//		this.from = from;
//	}
//
//	public String getTo() {
//		return to;
//	}
//
//	public void setTo(String to) {
//		this.to = to;
//	}
//
//	public StateTablut.Turn getTurn() {
//		return turn;
//	}
//
//	public void setTurn(StateTablut.Turn turn) {
//		this.turn = turn;
//	}
//
//	public String toString() {
//		return "Turn: " + this.turn + " " + "Pawn from " + from + " to " + to;
//	}
//
//	/**
//	 * @return means the index of the column where the pawn is moved from
//	 */
//	public int getColumnFrom() {
//		return Character.toLowerCase(this.from.charAt(0)) - 97;
//	}
//
//	/**
//	 * @return means the index of the column where the pawn is moved to
//	 */
//	public int getColumnTo() {
//		return Character.toLowerCase(this.to.charAt(0)) - 97;
//	}
//
//	/**
//	 * @return means the index of the row where the pawn is moved from
//	 */
//	public int getRowFrom() {
//		return Integer.parseInt(this.from.charAt(1) + "") - 1;
//	}
//
//	/**
//	 * @return means the index of the row where the pawn is moved to
//	 */
//	public int getRowTo() {
//		return Integer.parseInt(this.to.charAt(1) + "") - 1;
//	}

}
