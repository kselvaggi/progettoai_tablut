package it.unibo.ai.didattica.competition.tablut.bitboard;

/**
 * This enumerative represents a Turn
 * 
 * @author Team CAMST
 * 
 */
public enum Turn {
	
	WHITE("W"), BLACK("B"), WHITEWIN("WW"), BLACKWIN("BW"), DRAW("D");
	private final String turn;

	private Turn(String s) {
		turn = s;
	}

	public boolean equalsTurn(String otherName) {
		return (otherName == null) ? false : turn.equals(otherName);
	}

	public String toString() {
		return turn;
	}
	
}
