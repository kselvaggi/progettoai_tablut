package it.unibo.ai.didattica.competition.tablut.searchingAlgorithms;

import java.util.Set;

import it.unibo.ai.didattica.competition.tablut.domain.Action;
import it.unibo.ai.didattica.competition.tablut.domain.Game;
import it.unibo.ai.didattica.competition.tablut.domain.GameAshtonTablut;
import it.unibo.ai.didattica.competition.tablut.domain.State;

public class NegaScoutAlternativeAlgorithm extends SearchingAlgorithms {
	private Utils utils;

	public NegaScoutAlternativeAlgorithm(Utils utils) {
		super();
		this.utils = utils;
	}

	@Override
	public double calculate_rec(int depth, State state, Game rules, double alpha, double beta, boolean isMaximizingPlayer) {
		if (depth == 0) {
			return state.getHeuristicValue();
		}
		
		double score;
		double b = beta;

		Set<Action> availableMoves = null;
		try {
			availableMoves = utils.getAvailableMoves(state, (GameAshtonTablut) rules, isMaximizingPlayer);
			int i = 0;

			for (Action action : availableMoves) {
				i++;
				State tempState = state.clone();
				tempState = rules.checkMove(tempState, action);

				score = -calculate_rec(depth-1, tempState, rules, -b, -alpha, !isMaximizingPlayer);

				if ( (score > alpha) && (score < beta) && (i > 1) )
					score = -calculate_rec(depth-1, tempState, rules, -beta, -alpha, !isMaximizingPlayer);

				alpha = Math.max (alpha, score);	
				if (alpha >= beta)
					return alpha;
				b = alpha + 1;
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		return alpha;
	}
}
