package it.unibo.ai.didattica.competition.tablut.searchingAlgorithms;

import java.util.HashMap;
import java.util.Set;

import it.unibo.ai.didattica.competition.tablut.domain.Action;
import it.unibo.ai.didattica.competition.tablut.domain.Game;
import it.unibo.ai.didattica.competition.tablut.domain.GameAshtonTablut;
import it.unibo.ai.didattica.competition.tablut.domain.State;

public class AlphaBetaTTAlgorithm extends SearchingAlgorithms {
	private Utils utils;
	
	private HashMap<Long, TT_Entry> transpositionTable;
	
	private enum TT_Flag {EXACT, LOWERBOUND, UPPERBOUND};
	private class TT_Entry
	{
		private int value;
		private TT_Flag flag;
		private int depth;
	}
	
	public AlphaBetaTTAlgorithm(Utils utils) {
		super();
		this.utils = utils;
		transpositionTable = new HashMap<Long, TT_Entry>();
		//transpositionTable.clear();
	}
	
	@Override
	public double calculate_rec(int depth, State state, Game rules, double alpha, double beta, boolean isMaximizingPlayer) {
		
		TT_Entry entry;
		double alphaOriginal = alpha;
		Long hash = (long) state.hashCode();
		State stateClone = state.clone();
		Long hashClone = (long) stateClone.hashCode();
		if ( hash == hashClone) {
			System.out.println("hash uguali");
		}
		
		entry = transpositionTable.get(hash);
		if(entry != null && entry.depth >= depth)
		{
			if(entry.flag == TT_Flag.EXACT) return entry.value;
			else if(entry.flag == TT_Flag.LOWERBOUND) alpha = Math.max(entry.value, alpha);
			else if(entry.flag == TT_Flag.UPPERBOUND) beta = Math.min(entry.value, beta);
			
			if(alpha >= beta) return entry.value;
		}
		
		
		if (depth == 0) {
			return state.getHeuristicValue();
		}
		double bestValue;
		if (isMaximizingPlayer) {
			bestValue = -9999;
			Set<Action> availableMoves = null;
			try {
				availableMoves = utils.getAvailableMoves(state, (GameAshtonTablut) rules, isMaximizingPlayer); // ATTENZIONE!! Cast non corretto... ma chissene

				for (Action action : availableMoves) {
					State tempState = state.clone();
					tempState = rules.checkMove(tempState, action);
					bestValue = Math.max (bestValue, calculate_rec(depth-1, tempState, rules, alpha, beta, !isMaximizingPlayer));	
					alpha = Math.max(alpha, bestValue);
					if (beta <= alpha) {
						return bestValue;
					}
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} else {
			bestValue = 9999;
			Set<Action> availableMoves = null;
			try {
				availableMoves = utils.getAvailableMoves(state, (GameAshtonTablut) rules, isMaximizingPlayer); // ATTENZIONE!! Cast non corretto... ma chissene

				for (Action action : availableMoves) {
					State tempState = state.clone();
					tempState = rules.checkMove(tempState, action);
					bestValue = Math.min (bestValue, calculate_rec(depth-1, tempState, rules, alpha, beta, !isMaximizingPlayer));	
					beta = Math.min(beta, bestValue);
					if (beta <= alpha) {
						return bestValue;
					}
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
        entry = new TT_Entry();
		
		entry.value = (int) bestValue;
		if(bestValue <= alphaOriginal)
			entry.flag = TT_Flag.UPPERBOUND;
		else if(bestValue >= beta) 
			entry.flag = TT_Flag.LOWERBOUND;
		else 
			entry.flag = TT_Flag.EXACT;
		entry.depth = depth;
		
		transpositionTable.put(hash, entry);
		
		return bestValue;
	}
	
}
