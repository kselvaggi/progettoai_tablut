package it.unibo.ai.didattica.competition.tablut.searchingAlgorithms;

import java.util.Set;

import it.unibo.ai.didattica.competition.tablut.domain.Action;
import it.unibo.ai.didattica.competition.tablut.domain.Game;
import it.unibo.ai.didattica.competition.tablut.domain.GameAshtonTablut;
import it.unibo.ai.didattica.competition.tablut.domain.State;

public abstract class SearchingAlgorithms {
	private Utils utils = new Utils();
	
	public Action calculate(int depth, State state, Game rules, boolean isMaximizingPlayer) {
		Set<Action> availableMoves = null;
		double bestValue = -9999;
		double alpha = -10000;
		double beta = 10000;
		Action bestMove = null;

		try {
			availableMoves = utils.getAvailableMoves(state, (GameAshtonTablut) rules, isMaximizingPlayer); // ATTENZIONE!! Cast non corretto... ma chissene

			for (Action action : availableMoves) {
				State tempState = state.clone();
				tempState = rules.checkMove(tempState, action);
				
				double currentValue = calculate_rec(depth, tempState, rules, alpha, beta, !isMaximizingPlayer);

				if (currentValue > bestValue) {
					bestValue = currentValue;
					bestMove = action;
				}
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return bestMove;
	}
	
	public abstract double calculate_rec(int depth, State state, Game rules, double alpha, double beta, boolean isMaximizingPlayer);

}
