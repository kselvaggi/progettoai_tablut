package it.unibo.ai.didattica.competition.tablut.searchingAlgorithms;

import java.util.Set;

import it.unibo.ai.didattica.competition.tablut.domain.Action;
import it.unibo.ai.didattica.competition.tablut.domain.Game;
import it.unibo.ai.didattica.competition.tablut.domain.GameAshtonTablut;
import it.unibo.ai.didattica.competition.tablut.domain.State;

public class NegaScoutAlgorithm extends SearchingAlgorithms{
	private Utils utils;

	public NegaScoutAlgorithm(Utils utils) {
		super();
		this.utils = utils;
	}

	@Override
	public double calculate_rec(int depth, State state, Game rules, double alpha, double beta, boolean isMaximizingPlayer) {
		if (depth == 0) {
			return state.getHeuristicValue();
		}
		
		double score;
		double a = alpha;
		double b = beta;

		Set<Action> availableMoves = null;
		try {
			availableMoves = utils.getAvailableMoves(state, (GameAshtonTablut) rules, isMaximizingPlayer);
			int i = 0;

			for (Action action : availableMoves) {
				i++;
				State tempState = state.clone();
				tempState = rules.checkMove(tempState, action);

				score = -calculate_rec(depth-1, tempState, rules, -b, -a, !isMaximizingPlayer);

				if ( (score > a) && (score < beta) && (i > 1) && (depth > 1) )
					a = -calculate_rec(depth-1, tempState, rules, -beta, -score, !isMaximizingPlayer);

				a = Math.max (a, score);	
				if (a >= beta)
					return a;
				b = a + 1;
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		return a;
	}
}
