package it.unibo.ai.didattica.competition.tablut.searchingAlgorithms;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import it.unibo.ai.didattica.competition.tablut.domain.Action;
import it.unibo.ai.didattica.competition.tablut.domain.GameAshtonTablut;
import it.unibo.ai.didattica.competition.tablut.domain.State;
import it.unibo.ai.didattica.competition.tablut.exceptions.ActionException;

public class Utils implements IUtils{

	public Set<Action> getAvailableMoves(State state, GameAshtonTablut rules, boolean isMaximizingPlayer) throws IOException, ActionException {
		// Controllo degli argomenti...

		Set<Action> result = new HashSet<>();
		List<int[]> pawns = new ArrayList<int[]>();
		int[] buf;
		for (int i = 0; i < state.getBoard().length; i++) {
			for (int j = 0; j < state.getBoard().length; j++) {
				if (isMaximizingPlayer) {
					if (state.getPawn(i, j).equalsPawn(State.Pawn.WHITE.toString()) 
							|| state.getPawn(i, j).equalsPawn(State.Pawn.KING.toString())) {
						buf = new int[2];
						buf[0] = i;
						buf[1] = j;
						pawns.add(buf);
					}
				} else {
					if (state.getPawn(i, j).equalsPawn(State.Pawn.BLACK.toString())) {
						buf = new int[2];
						buf[0] = i;
						buf[1] = j;
						pawns.add(buf);
					}
				}
			}
		}

		for (int[] pawnIndex : pawns) {
			boolean canMove = true;
			int startingRow = pawnIndex[0];
			int startingColumn = pawnIndex[1];

			// controlla mosse in alto
			for (int i=startingRow-1; i >= 0 && canMove ; i--) {
				Action a = new Action( state.getBox(startingRow, startingColumn), state.getBox(i, startingColumn), state.getTurn() );
				if (rules.isPossibleMove(state, a)) {
					result.add(a);
				} else {
					canMove = false;
				}
			}
			
			// controlla mosse in basso
			canMove = true;
			for (int i=startingRow+1; i < state.getBoard().length && canMove; i++) {
				Action a = new Action( state.getBox(startingRow, startingColumn), state.getBox(i, startingColumn), state.getTurn() );
				if (rules.isPossibleMove(state, a)) {
					result.add(a);
				} else {
					canMove = false;
				}
			}
			
			// controlla mosse a destra
			canMove = true;
			for (int j=startingColumn+1; j < state.getBoard().length && canMove; j++) {
				Action a = new Action( state.getBox(startingRow, startingColumn), state.getBox(startingRow, j), state.getTurn() );
				if (rules.isPossibleMove(state, a)) {
					result.add(a);
				} else {
					canMove = false;
				}
			}
			
			// controlla mosse a sinistra
			canMove = true;
			for (int j=startingColumn-1; j >= 0 && canMove; j--) {
				Action a = new Action( state.getBox(startingRow, startingColumn), state.getBox(startingRow, j), state.getTurn() );
				if (rules.isPossibleMove(state, a)) {
					result.add(a);
				} else {
					canMove = false;
				}
			}
		}
		
		return result;
	}
	
	public Action calculateBestMove(State state, GameAshtonTablut rules, boolean isMaximizingPlayer) {
		Set<Action> availableMoves = null;
		double bestValue = -9999;
		Action bestMove = null;
		try {
			availableMoves = getAvailableMoves(state, (GameAshtonTablut) rules, isMaximizingPlayer); // ATTENZIONE!! Cast non corretto... ma chissene

			for (Action action : availableMoves) {
				State tempState = state.clone();
				rules.checkMove(tempState, action);
				double currentValue = tempState.getHeuristicValue();

				if (currentValue>bestValue) {
					bestValue = currentValue;
					bestMove = action;
				}

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return bestMove;
	}
}
