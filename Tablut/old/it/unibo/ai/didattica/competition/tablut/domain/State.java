package it.unibo.ai.didattica.competition.tablut.domain;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import it.unibo.ai.didattica.competition.tablut.bitboard.BitboardState;


/**
 * Abstract class for a State of a game We have a representation of the board
 * and the turn
 * 
 * @author Andrea Piretti
 *
 */
public abstract class State {

	/**
	 * Turn represent the player that has to move or the end of the game(A win
	 * by a player or a draw)
	 * 
	 * @author A.Piretti
	 */
	public enum Turn {
		WHITE("W"), BLACK("B"), WHITEWIN("WW"), BLACKWIN("BW"), DRAW("D");
		private final String turn;

		private Turn(String s) {
			turn = s;
		}

		public boolean equalsTurn(String otherName) {
			return (otherName == null) ? false : turn.equals(otherName);
		}

		public String toString() {
			return turn;
		}
	}

	/**
	 * 
	 * Pawn represents the content of a box in the board
	 * 
	 * @author A.Piretti
	 *
	 */
	public enum Pawn {
		EMPTY("O"), WHITE("W"), BLACK("B"), THRONE("T"), KING("K");
		private final String pawn;

		private Pawn(String s) {
			pawn = s;
		}

		public boolean equalsPawn(String otherPawn) {
			return (otherPawn == null) ? false : pawn.equals(otherPawn);
		}

		public String toString() {
			return pawn;
		}

	}

	protected Pawn board[][];
	protected Turn turn;
	protected int turnNumber = 0;

	public State() {
		super();
	}

	public Pawn[][] getBoard() {
		return board;
	}

	public String boardString() {
		StringBuffer result = new StringBuffer();
		for (int i = 0; i < this.board.length; i++) {
			for (int j = 0; j < this.board.length; j++) {
				result.append(this.board[i][j].toString());
				if (j == 8) {
					result.append("\n");
				}
			}
		}
		return result.toString();
	}

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();

		// board
		result.append("");
		result.append(this.boardString());

		result.append("-");
		result.append("\n");

		// TURNO
		result.append(this.turn.toString());

		return result.toString();
	}
	
	public String toLinearString() {
		StringBuffer result = new StringBuffer();

		// board
		result.append("");
		result.append(this.boardString().replace("\n", ""));
		result.append(this.turn.toString());

		return result.toString();
	}

	/**
	 * this function tells the pawn inside a specific box on the board
	 * 
	 * @param row
	 *            represents the row of the specific box
	 * @param column
	 *            represents the column of the specific box
	 * @return is the pawn of the box
	 */
	public Pawn getPawn(int row, int column) {
		return this.board[row][column];
	}

	/**
	 * this function remove a specified pawn from the board
	 * 
	 * @param row
	 *            represents the row of the specific box
	 * @param column
	 *            represents the column of the specific box
	 * 
	 */
	public void removePawn(int row, int column) {
		this.board[row][column] = Pawn.EMPTY;
	}

	public void setBoard(Pawn[][] board) {
		this.board = board;
	}

	public Turn getTurn() {
		return turn;
	}

	public void setTurn(Turn turn) {
		this.turn = turn;
	}
	
	public int getTurnNumber() {
		return turnNumber;
	}

	public void setTurnNumber(int turnNumber) {
		this.turnNumber = turnNumber;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		State other = (State) obj;
		if (this.board == null) {
			if (other.board != null)
				return false;
		} else {
			if (other.board == null)
				return false;
			if (this.board.length != other.board.length)
				return false;
			if (this.board[0].length != other.board[0].length)
				return false;
			for (int i = 0; i < other.board.length; i++)
				for (int j = 0; j < other.board[i].length; j++)
					if (!this.board[i][j].equals(other.board[i][j]))
						return false;
		}
		if (this.turn != other.turn)
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
//		result = prime * result + this.board.toString().hashCode();
		for (int i=0; i<board.length; i++) {
			for (int j=0; j<board[i].length ; j++) {
				result = prime * result + this.board[i][j].ordinal();
			}
		}
		result = prime * result + this.turn.ordinal();
		return result;
	}
	
	public String getBox(int row, int column) {
		String ret;
		char col = (char) (column + 97);
		ret = col + "" + (row + 1);
		return ret;
	}

	public State clone() {
		Class<? extends State> stateclass = this.getClass();
		Constructor<? extends State> cons = null;
		State result = null;
		try {
			cons = stateclass.getConstructor(stateclass);
			result = cons.newInstance(new Object[0]);
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}

		Pawn oldboard[][] = this.getBoard();
		Pawn newboard[][] = result.getBoard();

		for (int i = 0; i < this.board.length; i++) {
			for (int j = 0; j < this.board[i].length; j++) {
				newboard[i][j] = oldboard[i][j];
			}
		}

		result.setBoard(newboard);
		result.setTurn(this.turn);
		return result;
	}
	
	public double getHeuristicValue() {
		/*		OLD		*/
//		double value = 0;
//		for (int i=0; i < this.board.length; i++) {
//			for (int j=0; j < this.board[i].length; j++) {								
//				value = value + getPieceValue(board[i][j], i, j);
//			}
//		}	
//		return value;
		
		
		/*		NEW		*/
		double value = 0;
		int[] numPieces = new int[2];

		numPieces = getNumPieces();
		GameAshtonTablut rules = new GameAshtonTablut(99, 0, "garbage", "fake", "fake");

		if (this.turn.equalsTurn("W")) {
			value = numPieces[0] - numPieces[1] + kingMovesToWinningPointsValue(rules);
		}
		else if (this.turn.equalsTurn("B")) {
			value = numPieces[1] - numPieces[0] - kingMovesToWinningPointsValue(rules) + blackAroundKing();
		}
		else if (this.turn.equalsTurn("WW"))
			value = 20000;
		else if (this.turn.equalsTurn("BW"))
			value = -20000;

		return value;
	}
	

	private double kingMovesToWinningPointsValue(Game rules) {
		double value = 0;
				
		List<int[]> winningPoints = getWinningPoints();
		int[] kingPosition = getKingPosition();

		Set<Action> availableKingMoves = getKingAvailableMoves(this, (GameAshtonTablut) rules, kingPosition);
		
		if (!availableKingMoves.isEmpty()) {
			
			//min number of moves to reach each of the 16 winning Points
			int[] distances = new int[16];
			
			//iterate through all the winning points, calculating the min number of moves to reach each one
			int winningPointIndex = 0;
			for (int[] winningPoint : winningPoints) {
				distances[winningPointIndex] = calcMinMovesToWinningPoint (this, (GameAshtonTablut) rules, winningPoint, 1, kingPosition);
				winningPointIndex++;
			}

			for (int i = 0; i < distances.length; i++) {
				switch (distances[i]) {
				case 1: value += 15; //distanza dalla vittoria = 1 --> valore molto alto!!
				break;
				
				case 2: value += 10;
				break;
				
				default: value += 0;
				break;
				}
			}
		}

		return value;
	}
	
	private int calcMinMovesToWinningPoint (State state, Game rules, int[] winningPoint, int moveTerminalCondition, int[] kingPosition) {
		if (moveTerminalCondition == 2 || ( (kingPosition[0] == winningPoint[0]) && (kingPosition[1] == winningPoint[1]) ) )
			return moveTerminalCondition;
		
		Set<Action> availableKingMoves = getKingAvailableMoves(state, (GameAshtonTablut) rules, kingPosition);
		
		//teniamo traccia del numero di passi per ogni mossa
		int [] moveCounts = new int[availableKingMoves.size()];
		
		//iteriamo lungo le possibili mosse correnti del re e vediamo quanto siamo vicini a un Winning Point.
		int moveIndex = 0;
		int[] newKingPos;

		try {
			for (Action a : availableKingMoves) {
				State tempState = this.clone();
				tempState = rules.checkMove(tempState, a);

				newKingPos = new int[2];
				newKingPos[0] = a.getRowTo();
				newKingPos[1] = a.getColumnTo();

				//qua ci sarebbe un if... Se questa mossa portasse il re a una distanza dalla vittoria minore di quella di partenza:
				moveCounts[moveIndex] = calcMinMovesToWinningPoint(tempState, (GameAshtonTablut) rules, winningPoint, moveTerminalCondition+1, newKingPos);
				moveIndex++;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		//trovo il numero minimo di mosse per raggiungere un winning point, altrimento ritorno 50 se irraggiungibile
		int min = 50;
		for (int i = 0; i < moveCounts.length; i++) {
			int current = moveCounts[i];
			if (current != 0 && current < min) {
				min = current;
			}
		}
		
		return min;
	}
	
	private List<int[]> getWinningPoints() {
		List<int[]> result = new ArrayList<>();
		int[] winningPoint;
		for (int i=0; i < this.board.length; i++) {
			for (int j=0; j < this.board[i].length; j++) {
				if (this.getBox(i, j).equalsIgnoreCase("a2") || this.getBox(i, j).equalsIgnoreCase("a3") ||
						this.getBox(i, j).equalsIgnoreCase("a7") || this.getBox(i, j).equalsIgnoreCase("a8") ||
						this.getBox(i, j).equalsIgnoreCase("b1") || this.getBox(i, j).equalsIgnoreCase("c1") ||
						this.getBox(i, j).equalsIgnoreCase("b9") || this.getBox(i, j).equalsIgnoreCase("c9") || 
						this.getBox(i, j).equalsIgnoreCase("g1") || this.getBox(i, j).equalsIgnoreCase("h1") ||
						this.getBox(i, j).equalsIgnoreCase("g9") || this.getBox(i, j).equalsIgnoreCase("h9") ||
						this.getBox(i, j).equalsIgnoreCase("i2") || this.getBox(i, j).equalsIgnoreCase("i3") ||
						this.getBox(i, j).equalsIgnoreCase("i7") || this.getBox(i, j).equalsIgnoreCase("i8")) { 

					winningPoint = new int[2];
					winningPoint[0] = i;
					winningPoint[1] = j;
					result.add(winningPoint);
				}
			}
		}
		return result;
	}
	
	
	private int[] getKingPosition() {
		int[] kingPosition = new int[2];

		for (int i = 0; i < this.board.length; i++) {
			for (int j = 0; j < this.board[i].length; j++) {
				if (this.getPawn(i, j).equalsPawn("K")) {					
					kingPosition[0] = i;
					kingPosition[1] = j;
				}
			}
		}
		return kingPosition;
	}
	
	private int[] getNumPieces() { //int[0] = num bianchi, int[1] = num neri
		int[] result = new int[2];
		for (int i=0; i < this.board.length; i++) {
			for (int j=0; j < this.board[i].length; j++) {
				if (this.board[i][j].equalsPawn("W") || this.board[i][j].equalsPawn("K")) {
					result[0] = result[0] + 1;
				}
				else if (this.board[i][j].equalsPawn("B")) {
					result[1] = result[1] + 1;
				}
			}
		}
		return result;
	}
	
	private Set<Action> getKingAvailableMoves (State state, GameAshtonTablut rules, int[] kingPosition) {
		Set<Action> result = new HashSet<>();

		int startingRow = kingPosition[0];
		int startingColumn = kingPosition[1];

		try {
			boolean canMove = true;

			// controlla mosse in alto
			for (int i=startingRow-1; i >= 0 && canMove ; i--) {
				Action a = new Action( state.getBox(startingRow, startingColumn), state.getBox(i, startingColumn), state.getTurn() );
				if (rules.isPossibleMove(state, a)) {
					result.add(a);
				} else {
					canMove = false;
				}
			}

			// controlla mosse in basso
			canMove = true;
			for (int i=startingRow+1; i < state.getBoard().length && canMove; i++) {
				Action a = new Action( state.getBox(startingRow, startingColumn), state.getBox(i, startingColumn), state.getTurn() );
				if (rules.isPossibleMove(state, a)) {
					result.add(a);
				} else {
					canMove = false;
				}
			}

			// controlla mosse a destra
			canMove = true;
			for (int j=startingColumn+1; j < state.getBoard().length && canMove; j++) {
				Action a = new Action( state.getBox(startingRow, startingColumn), state.getBox(startingRow, j), state.getTurn() );
				if (rules.isPossibleMove(state, a)) {
					result.add(a);
				} else {
					canMove = false;
				}
			}

			// controlla mosse a sinistra
			canMove = true;
			for (int j=startingColumn-1; j >= 0 && canMove; j--) {
				Action a = new Action( state.getBox(startingRow, startingColumn), state.getBox(startingRow, j), state.getTurn() );
				if (rules.isPossibleMove(state, a)) {
					result.add(a);
				} else {
					canMove = false;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return result;
	}
	
	
//	private double[][] pawnEvalWhite = {
//			{ 0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, 0.0 } ,
//			{ 0.0,  2.0,  2.0, 40.0,  0.0, 40.0,  2.0,  2.0, 0.0 } ,
//			{ 0.0,  2.0,  2.0, 10.0,  0.0, 10.0,  2.0,  2.0, 0.0 } ,
//			{ 0.0, 40.0, 10.0,  4.0,  2.0,  4.0, 10.0, 40.0, 0.0 } ,
//			{ 0.0,  0.0,  0.0,  2.0,  0.0,  2.0,  0.0,  0.0, 0.0 } ,
//			{ 0.0, 40.0, 10.0,  4.0,  2.0,  4.0, 10.0, 40.0, 0.0 } ,
//			{ 0.0,  2.0,  2.0, 10.0,  0.0, 10.0,  2.0,  2.0, 0.0 } ,
//			{ 0.0,  2.0,  2.0, 40.0,  0.0, 40.0,  0.0,  2.0, 0.0 } ,
//			{ 0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, 0.0 }
//	} ;
//
//	private double[][] pawnEvalBlack = {
//			{  0.0,  -10.0, -10.0,  -2.0,  -1.0,  -2.0, -10.0, -10.0,   0.0 } ,
//			{ -10.0, -15.0, -20.0, -10.0,  -1.0, -10.0, -20.0, -15.0, -10.0 } ,
//			{ -10.0, -20.0, -10.0,  -1.0,  -1.0,  -1.0, -10.0, -20.0, -10.0 } ,
//			{ -2.0,  -10.0,  -1.0, -20.0, -40.0, -20.0,  -1.0, -10.0,  -2.0 } ,
//			{ -1.0,   -1.0,  -1.0, -40.0,   0.0, -40.0,  -1.0,  -1.0,  -1.0 } ,
//			{ -2.0,  -10.0,  -1.0, -20.0, -40.0, -20.0,  -1.0, -10.0,  -2.0 } ,
//			{ -10.0, -20.0, -10.0,  -1.0,  -1.0,  -1.0, -10.0, -20.0, -10.0 } ,
//			{ -10.0, -15.0, -20.0,  -1.0,  -1.0, -10.0, -20.0, -15.0, -10.0 } ,
//			{  0.0,  -10.0, -10.0,  -2.0,  -1.0,  -2.0, -10.0, -10.0,   0.0 }
//	} ;
//
//	private double[][] pawnEvalKing = {
//			{   0.0, 900.0, 900.0,   0.0,   0.0,   0.0, 900.0, 900.0,   0.0 } ,
//			{ 900.0, 300.0, 300.0,  10.0,   0.0,  10.0, 300.0, 300.0, 900.0 } ,
//			{ 900.0, 300.0, 200.0, 150.0, 200.0, 150.0, 200.0, 300.0, 900.0 } ,
//			{   0.0,  10.0, 100.0,   0.0, 100.0,   0.0, 100.0,  10.0,   0.0 } ,
//			{   0.0,   0.0, 200.0, 100.0,   0.0, 100.0, 200.0,   0.0,   0.0 } ,
//			{   0.0,  10.0, 100.0,   0.0, 100.0,   0.0, 100.0,  10.0,   0.0 } ,
//			{ 900.0, 300.0, 200.0, 150.0, 200.0, 150.0, 200.0, 300.0, 900.0 } ,
//			{ 900.0, 300.0, 300.0,  10.0,   0.0,  10.0, 300.0, 300.0, 900.0 } ,
//			{   0.0, 900.0, 900.0,   0.0,   0.0,   0.0, 900.0, 900.0,   0.0 }
//	} ;


	private double blackAroundKing() {
		int rowKing = 0;
		int columnKing = 0;
		int[] rowColKing = new int[2];
		double numBlack = 0;

		rowColKing = getKingPosition();
		
		rowKing = rowColKing[0];
		columnKing = rowColKing[1];

		if (this.getBox(rowKing, columnKing).equals("e5")) { //priorita' bassa

			if (this.getPawn(rowKing+1, columnKing).equalsPawn("B"))
				numBlack++;

			if (this.getPawn(rowKing-1, columnKing).equalsPawn("B"))
				numBlack++;

			if (this.getPawn(rowKing, columnKing+1).equalsPawn("B"))
				numBlack++;

			if (this.getPawn(rowKing, columnKing-1).equalsPawn("B"))
				numBlack++;
		}

		else if ( (this.getBox(rowKing, columnKing).equals("e4")) ) { //priorita' media

			if (this.getPawn(rowKing-1, columnKing).equalsPawn("B")) 
				numBlack++;

			if (this.getPawn(rowKing, columnKing+1).equalsPawn("B"))
				numBlack++;

			if (this.getPawn(rowKing, columnKing-1).equalsPawn("B")) 
				numBlack++;
		}

		else if ( (this.getBox(rowKing, columnKing).equals("e6")) ) { //priorita' media

			if (this.getPawn(rowKing+1, columnKing).equalsPawn("B"))
				numBlack++;

			if (this.getPawn(rowKing, columnKing+1).equalsPawn("B")) 
				numBlack++;

			if (this.getPawn(rowKing, columnKing-1).equalsPawn("B"))
				numBlack++;
		}

		else if ( (this.getBox(rowKing, columnKing).equals("d5")) ) { //priorita' media

			if (this.getPawn(rowKing-1, columnKing).equalsPawn("B")) 
				numBlack++;

			if (this.getPawn(rowKing+1, columnKing).equalsPawn("B")) 
				numBlack++;

			if (this.getPawn(rowKing, columnKing-1).equalsPawn("B")) 
				numBlack++;
		}

		else if ( (this.getBox(rowKing, columnKing).equals("f5")) ) { //priorita' media

			if (this.getPawn(rowKing-1, columnKing).equalsPawn("B"))
				numBlack++;

			if (this.getPawn(rowKing+1, columnKing).equalsPawn("B")) 
				numBlack++;

			if (this.getPawn(rowKing, columnKing+1).equalsPawn("B"))
				numBlack++;
		}

		else { //PRIORITA' ALTA
			if (rowKing < 8 && this.getPawn(rowKing+1, columnKing).equalsPawn("B")) 
				numBlack++;
			if (rowKing > 0 && this.getPawn(rowKing-1, columnKing).equalsPawn("B"))
				numBlack++;
			
			if (columnKing < 8 && this.getPawn(rowKing, columnKing+1).equalsPawn("B")) 
				numBlack++;
			if (columnKing > 0 && this.getPawn(rowKing, columnKing-1).equalsPawn("B"))
				numBlack++;
		}

		return numBlack;
	}
	

	private double priorityKing(Pawn pawn) {
		int rowKing = 0;
		int columnKing = 0;
		int[] rowColKing = new int[2];

		rowColKing = getKingPosition();
		
		rowKing = rowColKing[0];
		columnKing = rowColKing[1];

		if (this.getBox(rowKing, columnKing).equals("e5"))  //priorita' bassa
			return 5;
		else if ( this.getBox(rowKing, columnKing).equals("e4") || this.getBox(rowKing, columnKing).equals("e6") ||
				this.getBox(rowKing, columnKing).equals("d5") || this.getBox(rowKing, columnKing).equals("f5") ) //priorita' media
			return 10;
		else //priorita' alta
			return 20;
	}

	
	
	/*		OLD		*/
	
//	private double getPieceValue(Pawn pawn, int i, int j) {
//		double value = 0;
//		if (pawn.toString() == "O") 
//			return 0;
//		if (pawn.equalsPawn("W")) { //Aggiunto il numero di turni per l'attuazione di diverse strategie in base alle fasi di gioco (iniziali o finali) 
//			if ( this.turnNumber < 20) {
//			value = 20 + pawnEvalWhite[i][j] - priorityKing(pawn) * blackAroundKing(pawn);
//			//idea alla base: se il bianco vede che il re e' circondato da uno/piu' neri ci va lui nel punto in cui vorrebbe anadare il nero e circonda lui il re!
//			} else {
//				value = 20 + pawnEvalWhite[i][j] - 2 * priorityKing(pawn) * blackAroundKing(pawn);
//			}
//		}
//		else if (pawn.equalsPawn("B")) {
//			if ( this.turnNumber < 20) {
//			value = -20 + pawnEvalBlack[i][j] - priorityKing(pawn) * blackAroundKing(pawn); //- 5 * blackAroundCorners(pawn);
//			} else {
//				value = -20 + pawnEvalBlack[i][j] - 2 * priorityKing(pawn) * blackAroundKing(pawn); //- 5 * blackAroundCorners(pawn);			
//			}
//		}
//		else if (pawn.equalsPawn("K")) {
//			if ( this.turnNumber < 20) {
//			value = 1000 + pawnEvalKing[i][j] + priorityKing(pawn) * blackAroundKing(pawn);
//			} else {
//				value = 1000 + pawnEvalKing[i][j] + 2 * priorityKing(pawn) * blackAroundKing(pawn);
//			}
//		}
//		else
//			return -1;
//		return value;
//	}
	
}