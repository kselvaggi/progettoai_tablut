package it.unibo.ai.didattica.competition.tablut.client;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import it.unibo.ai.didattica.competition.tablut.domain.*;
import it.unibo.ai.didattica.competition.tablut.domain.State.Turn;
import it.unibo.ai.didattica.competition.tablut.exceptions.ActionException;
import it.unibo.ai.didattica.competition.tablut.exceptions.BoardException;
import it.unibo.ai.didattica.competition.tablut.exceptions.CitadelException;
import it.unibo.ai.didattica.competition.tablut.exceptions.ClimbingCitadelException;
import it.unibo.ai.didattica.competition.tablut.exceptions.ClimbingException;
import it.unibo.ai.didattica.competition.tablut.exceptions.DiagonalException;
import it.unibo.ai.didattica.competition.tablut.exceptions.OccupitedException;
import it.unibo.ai.didattica.competition.tablut.exceptions.PawnException;
import it.unibo.ai.didattica.competition.tablut.exceptions.StopException;
import it.unibo.ai.didattica.competition.tablut.exceptions.ThroneException;
import it.unibo.ai.didattica.competition.tablut.searchingAlgorithms.MinMaxAlphaBetaAlgorithm;
import it.unibo.ai.didattica.competition.tablut.searchingAlgorithms.NegaMaxAlgorithm;
import it.unibo.ai.didattica.competition.tablut.searchingAlgorithms.NegaScoutAlgorithm;
import it.unibo.ai.didattica.competition.tablut.searchingAlgorithms.NegaScoutAlternativeAlgorithm;
import it.unibo.ai.didattica.competition.tablut.searchingAlgorithms.NegaScoutPVSAlgorithm;
import it.unibo.ai.didattica.competition.tablut.searchingAlgorithms.AlphaBetaTTAlgorithm;
import it.unibo.ai.didattica.competition.tablut.searchingAlgorithms.Utils;

/**
 * 
 * @author provaaaa
 *
 */
public class TablutProtoClient extends TablutOldClient {

	private int game;

	public TablutProtoClient(String player, String name, int gameChosen) throws UnknownHostException, IOException {
		super(player, name);
		game = gameChosen;
	}

	public TablutProtoClient(String player) throws UnknownHostException, IOException {
		this(player, "proto", 4);
	}

	public TablutProtoClient(String player, String name) throws UnknownHostException, IOException {
		this(player, name, 4);
	}

	public TablutProtoClient(String player, int gameChosen) throws UnknownHostException, IOException {
		this(player, "proto", gameChosen);
	}

	public static void main(String[] args) throws UnknownHostException, IOException, ClassNotFoundException {
		int gametype = 4;
		String role = "";
		String name = "proto";
		// TODO: change the behavior?
		if (args.length < 1) {
			System.out.println("You must specify which player you are (WHITE or BLACK)");
			System.exit(-1);
		} else {
			System.out.println(args[0]);
			role = (args[0]);
		}
		if (args.length == 2) {
			System.out.println(args[1]);
			gametype = Integer.parseInt(args[1]);
		}
		if (args.length == 3) {
			name = args[2];
		}
		System.out.println("Selected client: " + args[0]);

		TablutProtoClient client = new TablutProtoClient(role, name, gametype);
		client.run();
	}

	@Override
	public void run() {

		try {
			this.declareName();
		} catch (Exception e) {
			e.printStackTrace();
		}

		State state;

		Game rules = null;
		switch (this.game) {
		case 1:
			state = new StateTablut();
			rules = new GameTablut();
			break;
		case 2:
			state = new StateTablut();
			rules = new GameModernTablut();
			break;
		case 3:
			state = new StateBrandub();
			rules = new GameTablut();
			break;
		case 4:
			state = new StateTablut();
			state.setTurn(State.Turn.WHITE);
			rules = new GameAshtonTablut(99, 0, "garbage", "fake", "fake");
			System.out.println("Ashton Tablut game");
			break;
		default:
			System.out.println("Error in game selection");
			System.exit(4);
		}

		System.out.println("You are player " + this.getPlayer().toString() + "!");

		while (true) {
			try {
				this.read();
			} catch (ClassNotFoundException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				System.exit(1);
			}
			System.out.println("Current state:");
			state = this.getCurrentState();
			System.out.println(state.toString());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}

			if (this.getPlayer().equals(Turn.WHITE)) { // SONO BIANCO
				// � il mio turno
				if (this.getCurrentState().getTurn().equals(StateTablut.Turn.WHITE)) {			

					// Naive Implementation
					//Action bestMove = calculateBestMove(state, (GameAshtonTablut) rules);

					Utils utils = new Utils();
					int depth = 0;

					MinMaxAlphaBetaAlgorithm minMax = new MinMaxAlphaBetaAlgorithm(utils);
					Action bestMove = minMax.calculate(depth, state, rules, true);

					//NegaMaxAlgorithm negaMax = new NegaMaxAlgorithm(utils);
					//Action bestMove = negaMax.calculate(depth, state, rules, true);

					//NegaScoutAlgorithm negaScout = new NegaScoutAlgorithm(utils);
					//Action bestMove = negaScout.calculate(depth, state, rules, true);

					//NegaScoutAlternativeAlgorithm negaScoutAlternative = new NegaScoutAlternativeAlgorithm(utils);
					//Action bestMove = negaScoutAlternative.calculate(depth, state, rules, true);

					//NegaScoutPVSAlgorithm negaScoutPVS = new NegaScoutPVSAlgorithm(utils);
					//Action bestMove = negaScoutPVS.calculate(depth, state, rules, true);

					//AlphaBetaTTAlgorithm alphaBetaTT = new AlphaBetaTTAlgorithm(utils);
					//Action bestMove = alphaBetaTT.calculate(depth, state, rules, true);

					// Controllare se bestMove � null?
					try {
						state = rules.checkMove(state, bestMove);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					System.out.println("Mossa scelta: " + bestMove.toString());
					try {
						this.write(bestMove);
					} catch (ClassNotFoundException | IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}


				}
				// � il turno dell'avversario
				else if (state.getTurn().equals(StateTablut.Turn.BLACK)) {
					System.out.println("Waiting for your opponent move... ");
				}
				// ho vinto
				else if (state.getTurn().equals(StateTablut.Turn.WHITEWIN)) {
					System.out.println("YOU WIN!");
					System.exit(0);
				}
				// ho perso
				else if (state.getTurn().equals(StateTablut.Turn.BLACKWIN)) {
					System.out.println("YOU LOSE!");
					System.exit(0);
				}
				// pareggio
				else if (state.getTurn().equals(StateTablut.Turn.DRAW)) {
					System.out.println("DRAW!");
					System.exit(0);
				}

			} else { //SONO IL NERO

				// � il mio turno
				if (this.getCurrentState().getTurn().equals(StateTablut.Turn.BLACK)) {

					// Naive implementation
					//Action bestMove = calculateBestMove(state, (GameAshtonTablut) rules, false);

					Utils utils = new Utils();
					int depth = 2;

					NegaScoutAlgorithm negaScout = new NegaScoutAlgorithm(utils);
					Action bestMove = negaScout.calculate(depth, state, rules, true);

					// Controllare se bestMove � null?
					try {
						rules.checkMove(state, bestMove);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					System.out.println("Mossa scelta: " + bestMove.toString());
					try {
						this.write(bestMove);
					} catch (ClassNotFoundException | IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				else if (state.getTurn().equals(StateTablut.Turn.WHITE)) {
					System.out.println("Waiting for your opponent move... ");
				} else if (state.getTurn().equals(StateTablut.Turn.WHITEWIN)) {
					System.out.println("YOU LOSE!");
					System.exit(0);
				} else if (state.getTurn().equals(StateTablut.Turn.BLACKWIN)) {
					System.out.println("YOU WIN!");
					System.exit(0);
				} else if (state.getTurn().equals(StateTablut.Turn.DRAW)) {
					System.out.println("DRAW!");
					System.exit(0);
				}
			}
		}
	}
}
