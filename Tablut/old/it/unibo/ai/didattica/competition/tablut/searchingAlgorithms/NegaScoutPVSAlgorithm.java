package it.unibo.ai.didattica.competition.tablut.searchingAlgorithms;

import java.util.Set;

import it.unibo.ai.didattica.competition.tablut.domain.Action;
import it.unibo.ai.didattica.competition.tablut.domain.Game;
import it.unibo.ai.didattica.competition.tablut.domain.GameAshtonTablut;
import it.unibo.ai.didattica.competition.tablut.domain.State;

public class NegaScoutPVSAlgorithm extends SearchingAlgorithms{
	private Utils utils;
	
	public NegaScoutPVSAlgorithm(Utils utils) {
		super();
		this.utils = utils;
	}

	@Override
	public double calculate_rec(int depth, State state, Game rules, double alpha, double beta, boolean isMaximizingPlayer) {
		if (depth == 0) {
			return state.getHeuristicValue();
		}

		boolean bSearchPV = true;
		double score;

		Set<Action> availableMoves = null;
		try {
			availableMoves = utils.getAvailableMoves(state, (GameAshtonTablut) rules, isMaximizingPlayer);

			for (Action action : availableMoves) {
				State tempState = state.clone();
				tempState = rules.checkMove(tempState, action);
				if (bSearchPV) {
					score = -calculate_rec(depth-1, tempState, rules, -beta, -alpha, !isMaximizingPlayer);
				} else {
					score = -calculate_rec(depth-1, tempState, rules, -alpha-1, -alpha, !isMaximizingPlayer);
					if (score > alpha && score < beta)
						score = -calculate_rec(depth-1, tempState, rules, -beta, -alpha, !isMaximizingPlayer);
				}

				if (score >= beta)
					return beta;
				if (score > alpha) {
					alpha = score;
					bSearchPV = false;
				}
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		return alpha;
	}
}
