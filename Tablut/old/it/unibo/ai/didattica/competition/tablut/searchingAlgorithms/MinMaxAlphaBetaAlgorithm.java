package it.unibo.ai.didattica.competition.tablut.searchingAlgorithms;

import java.util.Set;

import it.unibo.ai.didattica.competition.tablut.domain.Action;
import it.unibo.ai.didattica.competition.tablut.domain.Game;
import it.unibo.ai.didattica.competition.tablut.domain.GameAshtonTablut;
import it.unibo.ai.didattica.competition.tablut.domain.State;

public class MinMaxAlphaBetaAlgorithm extends SearchingAlgorithms {
	private Utils utils;

	public MinMaxAlphaBetaAlgorithm(Utils utils) {
		super();
		this.utils = utils;
	}

	@Override
	public double calculate_rec(int depth, State state, Game rules, double alpha, double beta, boolean isMaximizingPlayer) {
		if (depth == 0) {
			return state.getHeuristicValue();
		}
		double bestValue;
		if (isMaximizingPlayer) {
			bestValue = -9999;
			Set<Action> availableMoves = null;
			try {
				availableMoves = utils.getAvailableMoves(state, (GameAshtonTablut) rules, isMaximizingPlayer); // ATTENZIONE!! Cast non corretto... ma chissene

				for (Action action : availableMoves) {
					State tempState = state.clone();
					tempState = rules.checkMove(tempState, action);
					bestValue = Math.max (bestValue, calculate_rec(depth-1, tempState, rules, alpha, beta, !isMaximizingPlayer));	
					alpha = Math.max(alpha, bestValue);
					if (beta <= alpha) {
						return bestValue;
					}
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} else {
			bestValue = 9999;
			Set<Action> availableMoves = null;
			try {
				availableMoves = utils.getAvailableMoves(state, (GameAshtonTablut) rules, isMaximizingPlayer); // ATTENZIONE!! Cast non corretto... ma chissene

				for (Action action : availableMoves) {
					State tempState = state.clone();
					tempState = rules.checkMove(tempState, action);
					bestValue = Math.min (bestValue, calculate_rec(depth-1, tempState, rules, alpha, beta, !isMaximizingPlayer));	
					beta = Math.min(beta, bestValue);
					if (beta <= alpha) {
						return bestValue;
					}
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return bestValue;
	}
}
