package it.unibo.ai.didattica.competition.tablut.searchingAlgorithms;

import java.io.IOException;
import java.util.Set;

import it.unibo.ai.didattica.competition.tablut.domain.Action;
import it.unibo.ai.didattica.competition.tablut.domain.GameAshtonTablut;
import it.unibo.ai.didattica.competition.tablut.domain.State;
import it.unibo.ai.didattica.competition.tablut.exceptions.ActionException;

public interface IUtils {
	Set<Action> getAvailableMoves(State state, GameAshtonTablut rules, boolean isMaximizingPlayer) throws IOException, ActionException;
	Action calculateBestMove(State state, GameAshtonTablut rules, boolean isMaximizingPlayer);
}
